#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import healpy as hp


PROJECTPATH = '/Users/hao-yiwu/work_zwicky/IM_opt/'
MAPPATH = PROJECTPATH + 'repo/cl/data/map_test.fits'

cls = np.sqrt(np.arange(2048))
nside = 2048
#lmax = 1024

maps = hp.synfast(cls=cls,nside=nside)
hp.mollview(maps)
hp.write_map(MAPPATH, maps)

#plt.show()

cl = hp.anafast(maps, lmax=1024)
ell = np.arange(len(cl))
plt.figure()
#plt.plot(ell, ell * (ell+1) * cl)
plt.plot(ell, cl)

plt.xlabel('ell')
plt.ylabel('ell(ell+1)cl')
plt.grid()
plt.show()
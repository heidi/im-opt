#!/usr/bin/env python
import numpy as np
import healpy as hp
import math
import cross
import makeinv
import output
import pdb
import plot


lmax = 2048 #int(input("What lmax ? "))
# if lmax % 2 != 0:
#     raise RuntimeError("lmax must be multiple of 2!")
Delta_bin = 32 #int(input("Which l-Delta_bin for the output spectra (ex: 32, 64, 128...)? "))
#if lmax % Delta_bin != 0:
#    raise RuntimeError("lmax must be multiple of Delta!")
freqs = ['']#["353"]#, "545", "857"]
in_map = 'map_test'#'healpy_z_5_6'#input("Name of input map (no freq nor .fits extension, ex: 'input_map' or 'CIBmap2048'): ")
name1 = 'cib'
auto_or_x = 'auto'#input("'auto' or 'cross' power spectrum? Ex: 'auto': ")
if auto_or_x == 'cross':
    in_map2 = input("Name of second input map, ex: 'input_map2' or 'kappa_map_2048': ")
    name2 = 'phi'
    beam = "sqrtbeamxwin_" # beam name to be used in the makeinv file
else:
    in_map2 = in_map
    name2 = name1
    beam = "win" 
in_mask = 'mask'#input("Name of input mask, default is: 'cross_mask': ")


# compute the Cls of the mask and the masked maps
cross.cross_maps(lmax, freqs, in_map, in_map2, in_mask, auto_or_x)

# compute the inverse matrix
makeinv.inv(lmax, freqs, Delta_bin, beam)

# deconvolve using the inverse matrix
output.master(lmax, freqs, Delta_bin, name1, name2)

# cross-check! Plot input and output spectra 
#plot.plot_cl(lmax, freqs, Delta_bin, name1, name2)

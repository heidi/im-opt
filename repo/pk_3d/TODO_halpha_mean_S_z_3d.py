#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
import params as pa

fsample = 0.01

data = np.loadtxt(pa.bolshoip_loc+'cosmo_volume_time.dat')
z = data[:,1]
dVdOmegadz = data[:,5]
from scipy.interpolate import interp1d
dVdOmegadz_interp = interp1d(z, dVdOmegadz)

def intensity_from_catalog():
    lam_rest_AA = 6563.
    na = len(pa.a_list)
    dInu_list = np.zeros(na)
    outfile = open(pa.data_master_loc + 'pk_3d/S_z.dat','w')
    outfile.write('#z, S_nu [Jy/sr] \n')
    for ia in range(na):
        a = pa.a_list[ia]
        z = max(0, 1./a - 1)

        data = np.loadtxt(pa.data_master_loc+'pk_3d/Ha_cat_a%0.5f_f%g.dat'%(a,fsample))
        lgLHa = data[:,4]

        chi = pa.cosmo.comoving_distance(z).value

        DH = 3000. / pa.h
        DA = chi/(1+z)
        DL = chi*(1+z)
        Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)

        nu = (3.e+8)/(lam_rest_AA*1.e-10)
        dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
        S_Ha = 10**lgLHa / (4*np.pi*DL**2) * (DA**2) * dchi_dnu #erg s^-1 Mpc Hz^-1
        sim_vol = (250. / pa.h )**3
        # DH = 3000./pa.h
        # Ez = np.sqrt(pa.OmegaM*(1+z)**3 + pa.OmegaDE)
        # zslice_vol = chi**2 * DH/Ez
        # print(z)
        #vol = dVdOmegadz_interp(z)
        #print('compare vol %e %e'%(vol, zslice_vol)) # NOT BAD!!!

        dInu_list[ia] = sum(S_Ha) / sim_vol/ fsample * pa.Mpc2cm**-2 * pa.cgs2Jy
        outfile.write('%g %g\n'%(z, dInu_list[ia]))
    outfile.close()



def plot_intensity():
    plt.figure()
    plt.subplot(111,yscale='log')

    data = np.loadtxt(pa.data_master_loc + 'pk_3d/S_z.dat')
    plt.plot(data[:,0], data[:,1], label='mock catalog')

    data = np.loadtxt(pa.data_master_loc + 'halpha/S_z_2d.dat')
    plt.plot(data[:,0], data[:,1], label='2d mock catalog')


    data = np.loadtxt('../data_Gong17/fig3a_dInudz_Ha_HopkinsBeacom.dat')
    plt.plot(data[:,0], data[:,1], label='Gong17 (Hopkins & Beacom)')
    plt.legend()
    #plt.xlim(0,5)
    plt.ylim(0.1,100)
    plt.xlabel(r"$z$")
    plt.ylabel(r"$S(z)\ [Jy\ sr^{-1}]$")
    plt.title(r"$H_\alpha$")
    plt.savefig('../../plots/S_z_Halpha.pdf')
    plt.show()

if __name__ == "__main__":
    intensity_from_catalog()
    plot_intensity()


#!/usr/bin/env python
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
pi = np.pi        

L = 250 # size of the box
n = 128  # number of grid
l_cell = L/(n*1.) # size of the grid 
nk = 50
kmin = 2*np.pi/L
kmax = 2*np.pi/l_cell
print('kmin, kmax', kmin, kmax)

# data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'
# Mpc2cm = 3.086e+24
# cgs2Jy = 1.e+23

import sys
sys.path.append('../')
import params as pa

fsample = 0.01

data = np.loadtxt(pa.bolshoip_loc+'cosmo_volume_time.dat')
z = data[:,1]
dVdOmegadz = data[:,5]
from scipy.interpolate import interp1d
dVdOmegadz_interp = interp1d(z, dVdOmegadz)

a = 0.16194
z = 1./a - 1
sim_vol = (250 / pa.h )**3
vol = dVdOmegadz_interp(z)

class PowerSpectrum3D(object):
    def __init__(self,lgMmin=10):
        self.lgMmin = lgMmin

    def make_Ha_intensity_grid(self):
        fname = pa.data_master_loc+'pk_3d/Ha_cat_a%0.5f_f%g.dat'%(a,fsample)
        ifile = open(fname, 'r')
        header = ifile.readline().split()

        from astropy.cosmology import FlatLambdaCDM
        cosmo = FlatLambdaCDM(H0=100*pa.h, Om0=pa.OmegaM)
        chi = cosmo.comoving_distance(z)
        DL = chi.value*(1+z)



        fx = np.zeros([n,n,n])
        for line in ifile:
            halo = line.split()
            px = np.double(halo[0])
            py = np.double(halo[1])
            pz = np.double(halo[2])
            Mvir = 10**np.double(halo[3])
            L_Ha = 10**np.double(halo[4])
            nu = (3.e+8)/(6563.*1.e-10)
            Snu_Ha = L_Ha / (4*np.pi*DL**2) / nu * (pa.Mpc2cm**-2) * pa.cgs2Jy 

            #for ip in range(len(x)):
            i = min(int(np.floor(px/l_cell)),n-1)
            j = min(int(np.floor(py/l_cell)),n-1)
            k = min(int(np.floor(pz/l_cell)),n-1)

            #print(px, py, pz, i, j, k)
            #print(np.log10(Mvir[ip]))
            #if np.log10(Mvir) > self.lgMmin:
            fx[i,j,k] += Snu_Ha

        fx_flat = fx.flatten() / sim_vol * vol / fsample # Jy/sr for each cell
        np.savetxt(pa.data_master_loc+'/pk_3d/Ha_grid_M%g.dat'%(self.lgMmin), fx_flat)

        # check density grid
        #data = np.loadtxt('../output/den_3d.dat')
        #fx_new = data.reshape((n,n,n))
        #print(fx_new - fx)


    def fourier_transform(self):
        data = np.loadtxt(pa.data_master_loc+'/pk_3d/Ha_grid_M%g.dat'%(self.lgMmin))
        mean_den = np.mean(data)
        fx = (data - mean_den)/mean_den #data - np.mean(data)#
        fx = fx.reshape((n,n,n))

        # create the grid based on Python convention
        k = np.zeros([n,n,n])
        for i1 in range(n):
            for i2 in range(n):
                for i3 in range(n):
                    if i1 < n/2: k1 = i1*2*pi/L
                    else: k1 = (i1-n)*2*pi/L
                    if i2 < n/2: k2 = i2*2*pi/L
                    else: k2 = (i2-n)*2*pi/L
                    if i3 < n/2: k3 = i3*2*pi/L
                    else: k3 = (i3-n)*2*pi/L
                    k[i1,i2,i3] = np.sqrt(k1**2+k2**2+k3**2)

        Fk = fft.fftn(fx)*(L/n)**3 # Fourier coefficients
        Fk = np.absolute(Fk)**2
        print(Fk)
        print('shape of Fk', len(Fk))

        # summing over k-modes
        k = k.flatten()
        Fk = Fk.flatten()


        lgk_list = np.linspace(np.log10(kmin), np.log10(kmax), nk+1)
        Pk_list = np.zeros(nk)
        lgPk_list = np.zeros(nk)
        dPk_list = np.zeros(nk)
        for ik in range(nk):
            b = (np.log10(k) > lgk_list[ik])*(np.log10(k) < lgk_list[ik+1])
            Pk_list[ik] = np.mean(Fk[b])/(L**3) # NOTE! don't forget L^3
            lgPk_list[ik] = np.mean(np.log10(Fk[b]))

        outfile = open(pa.data_master_loc+'pk_3d/Ha_pk.dat','w')
        outfile.write('#k [h/Mpc], Pk [Mpc^-3 h^3] \n')
        for i in range(nk):
            outfile.write('%g %g\n'%(10**lgk_list[i], Pk_list[i]))
        outfile.close()

        #print(10**lgk_list)
        #print(Pk_list)
        #plt.loglog(10**lgk_list[0:-1], Pk_list)


    def plot_pk(self):
        plt.subplot(111,xscale='log',yscale='log')
        data = np.loadtxt(pa.data_master_loc+'pk_3d/Ha_pk.dat')

        k = data[:,0]
        Pk = data[:,1]
        plt.subplot(111,xscale='log',yscale='log')
        plt.plot(k, Pk*k**3/(2*np.pi**2), label='mock catalog')

        # Gong 17
        data = np.loadtxt('../data_Gong17/fig5a_pk_Ha_z4.8.dat')
        plt.plot(data[:,0], data[:,1], label='Gong17 model')


        plt.title(r"$\rm H_\alpha, z=%.2f$"%(z))
        plt.legend()
        plt.xlabel(r"$\rm k (h/Mpc)$")
        plt.ylabel(r"$\rm k^3 P(k)/(2\pi^2) (Jy/sr)^2$")
        plt.savefig('../../plots/pk_Ha_3d_sanity.pdf')



if __name__ == "__main__":
    import timeit
    start = timeit.default_timer()

    ps3 = PowerSpectrum3D()
    
    #ps3.make_Ha_intensity_grid()
    ps3.fourier_transform()
    ps3.plot_pk()

    plt.show()

    stop = timeit.default_timer()
    print('took', stop - start, 'seconds')


#!/usr/bin/env python
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
pi = np.pi

import sys
sys.path.append('../')
import params as pa

# No h in this calculation!
L = 250./pa.h # size of the box
ngrid = 32#128  # number of grid
l_cell = L/(ngrid*1.) # size of the grid 
nk = 50
kmin = 2*pi/L
kmax = 2*pi/l_cell
#print('kmin, kmax', kmin, kmax)

fsample = 0.01

a = 0.16700
z = 1./a - 1


cell_vol = l_cell**3

class PowerSpectrum3D(object):
    def __init__(self):
        pass 

    def intensity_grid(self):
        fname = pa.data_master_loc+'pk_3d_cat/Ha_cat_a%0.5f_f%g.dat'%(a,fsample)
        ifile = open(fname, 'r')
        header = ifile.readline().split()

        chi = pa.cosmo.comoving_distance(z).value
        DH = 3000. / pa.h
        DA = chi/(1+z)
        DL = chi*(1+z)
        Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)

        sum_S = 0
        fx_halpha = np.zeros([ngrid,ngrid,ngrid])
        fx_halo = np.zeros([ngrid,ngrid,ngrid])
        for line in ifile:
            halo = line.split()
            px = np.double(halo[0])/pa.h
            py = np.double(halo[1])/pa.h
            pz = np.double(halo[2])/pa.h
            Mvir = 10**np.double(halo[3])
            L_Ha = 10**np.double(halo[4])
            

            #print(px, py, pz, Mvir, L_Ha)
            nu = (3.e+8)/(6563.*1.e-10)
            dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
            S_Ha = L_Ha / (4*pi*DL**2) * (DA**2) * dchi_dnu
            i = min(int(np.floor(px/l_cell)),ngrid-1)
            j = min(int(np.floor(py/l_cell)),ngrid-1)
            k = min(int(np.floor(pz/l_cell)),ngrid-1)
            #print(px, py, pz, i, j, k)
            #exit()
            #print(np.log10(Mvir), np.log10(L_Ha))
            #plt.scatter(np.log10(Mvir), np.log10(L_Ha))
            if Mvir > 1.e+11 and Mvir < 2e+11:
                fx_halpha[i,j,k] += S_Ha
                fx_halo[i,j,k] += 1#Mvir
                sum_S += S_Ha
        ifile.close()

        fx_halpha = fx_halpha/np.mean(fx_halpha.flatten()) - 1
        fx_halo = fx_halo/np.mean(fx_halo.flatten()) - 1

        '''
        islice = 1
        plt.figure(figsize=(14,7))
        plt.subplot(121)
        plt.imshow(fx_halpha[islice,:,:])
        plt.subplot(122)
        plt.imshow(fx_halo[islice,:,:])
        plt.show()
        '''
        
        for dummy in [0, 1]:
            if dummy == 0: fx = fx_halpha; la='halpha'
            if dummy == 1: fx = fx_halo  ; la='halo'

            # create the grid based on Python convention
            k = np.zeros([ngrid,ngrid,ngrid])
            for i1 in range(ngrid):
                for i2 in range(ngrid):
                    for i3 in range(ngrid):
                        if i1 < ngrid/2: k1 = i1*2*pi/L
                        else: k1 = (i1-ngrid)*2*pi/L
                        if i2 < ngrid/2: k2 = i2*2*pi/L
                        else: k2 = (i2-ngrid)*2*pi/L
                        if i3 < ngrid/2: k3 = i3*2*pi/L
                        else: k3 = (i3-ngrid)*2*pi/L
                        k[i1,i2,i3] = np.sqrt(k1**2+k2**2+k3**2)

            Fk = fft.fftn(fx)*(L/ngrid)**3 # Fourier coefficients
            Fk = np.absolute(Fk)**2
            #print(Fk)
            #print('shape of Fk', len(Fk))

            # summing over k-modes
            k = k.flatten()
            Fk = Fk.flatten()

            lgk_list = np.linspace(np.log10(kmin), np.log10(kmax), nk+1)
            Pk_list = np.zeros(nk)
            lgPk_list = np.zeros(nk)
            dPk_list = np.zeros(nk)
            for ik in range(nk):
                b = (np.log10(k) > lgk_list[ik])*(np.log10(k) < lgk_list[ik+1])
                Pk_list[ik] = np.mean(Fk[b])/(L**3) # NOTE! don't forget L^3
                lgPk_list[ik] = np.mean(np.log10(Fk[b]))

            plt.loglog(10**lgk_list[0:-1], Pk_list, label=la)
        plt.legend()
        plt.show()

    '''
            outfile = open(pa.data_master_loc+'pk_3d/%s_pk.dat'%(obj),'w')
            outfile.write('#k [Mpc, no h], Pk\n')
            for i in range(nk):
                outfile.write('%g %g\n'%(10**lgk_list[i], Pk_list[i]))
            outfile.close()
        #print(10**lgk_list)
        #print(Pk_list)
        #plt.loglog(10**lgk_list[0:-1], Pk_list)


    def plot_pk(self):
        plt.figure(figsize=(14,7))
        plt.subplot(121,xscale='log',yscale='log')
        data = np.loadtxt(pa.data_master_loc+'pk_3d/halpha_pk.dat')
        k = data[:,0]
        Pk = data[:,1]
        plt.plot(k, Pk, label=r"$\rm H_\alpha$")

        data = np.loadtxt(pa.data_master_loc+'pk_3d/halo_pk.dat')
        k = data[:,0]
        Pk = data[:,1]
        plt.plot(k, Pk, label='halos')
        
        # growth rate
        data = np.loadtxt('/home/hywu/work/CIB_bathtub_v2/repo/data/data_interp/Growth_z.dat')
        from scipy.interpolate import interp1d
        growth = interp1d(data[:,0], data[:,1])
        # liner PS
        data = np.loadtxt('/home/hywu/work/CIB_bathtub_v2/repo/data/data_interp/Planck14_matterpower.dat')
        k = data[:,0]
        z = 5
        Pk = data[:,1]*(growth(z)**2)
        b = (k>0.01)*(k < 2)     
        plt.plot(k[b], Pk[b], label='linear, z=%g'%(z))
        
        plt.legend()
        plt.xlabel(r"$\rm k (1/Mpc)$")
        plt.ylabel(r"$\rm P(k)$")

        plt.subplot(122,xscale='log',yscale='log')
        data = np.loadtxt(pa.data_master_loc+'pk_3d/halpha_pk.dat')
        k = data[:,0]
        Pk = data[:,1]
        plt.plot(k/pa.h, Pk*k**3/(2*pi**2), label='mock catalog')

        #shot_ana = 81.4924153682
        #plt.plot(k*pa.h, shot_ana*k**3/(2*pi**2), label='anayltic')


        # Gong 17
        data = np.loadtxt('../data_Gong17/fig5a_pk_Ha_z4.8.dat')
        plt.plot(data[:,0], data[:,1], label='Gong17 model')

        plt.title(r"$\rm H_\alpha, z=%.2f$"%(z))
        plt.legend()
        plt.xlabel(r"$\rm k (h/Mpc)$")
        plt.ylabel(r"$\rm k^3 P(k)/(2\pi^2) (Jy/sr)^2$")
        plt.savefig('../../plots/pk_Ha_3d_sanity.pdf')
        plt.show()
'''
if __name__ == "__main__":
    import timeit
    start = timeit.default_timer()

    ps3 = PowerSpectrum3D()
    ps3.intensity_grid()
    #ps3.plot_grid()
    #ps3.make_Ha_intensity_grid()
    #ps3.fourier_transform(obj='halpha')
    #ps3.fourier_transform(obj='halo')
    #ps3.plot_pk()

    stop = timeit.default_timer()
    print('took', stop - start, 'seconds')


#!/usr/bin/env python
#from numpy import fft
import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
import params as pa

# copied from pk_3d_mastercode_demo.py
# no h in this file!!!!

ngrid = 256
Lbox = 250./pa.h
l_cell = Lbox/(ngrid*1.) # size of the grid 
cell_vol = l_cell**3

a = 0.16700
fsample = 1
z = 1./a - 1.
name = 'halpha_a%.5f'%(a)
cat_name = pa.data_master_loc+'pk_3d_cat/%s.dat'%(name)

# write the file
def write_pk_input_file():
    fname = pa.data_master_loc+'halpha_cat/Ha_cat_a%0.5f_f%g.dat'%(a,fsample)
    ifile = open(fname, 'r')
    header = ifile.readline().split()

    chi = pa.cosmo.comoving_distance(z).value # no h for S_halpha
    DH = 3000. / pa.h # no h for S_halpha
    DA = chi/(1+z)
    DL = chi*(1+z)
    Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)

    outfile = open(cat_name, 'w')
    for line in ifile:
        halo = line.split()
        px = np.double(halo[0])/pa.h # Mpc/h for coordinates
        py = np.double(halo[1])/pa.h
        pz = np.double(halo[2])/pa.h
        # Mvir = 10**np.double(halo[3])
        L_halpha = 10**np.double(halo[4])
        
        # #print(px, py, pz, Mvir, L_Ha)
        nu = (3.e+8)/(6563.*1.e-10)
        dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
        S_halpha = L_halpha / (4*np.pi*DL**2) * (DA**2) * dchi_dnu / cell_vol * pa.Mpc2cm**-2 * pa.cgs2Jy

        outfile.write('%g %g %g %g \n'%(px,py,pz,S_halpha))
    outfile.close()


from pk_3d_mastercode import PowerSpectrum3D
def calc_pk():
    ifname = cat_name
    ofname = pa.data_master_loc+'pk_3d/%s'%(name)
    ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=True, save_grid=False, divide_by_mean=False)#/pa.h
    ps3.make_halo_density_grid()
    ps3.fourier_transform()

'''
def plot_pk():
    plt.subplot(111, xscale='log', yscale='log')
    data = np.loadtxt(pa.data_master_loc+'pk_3d/%s_pk.dat'%(name))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k/pa.h, (k**3/(2*np.pi**2))*pk, label=name)

    # Gong 17
    data = np.loadtxt('../data_Gong17/fig5a_pk_Ha_z4.8.dat')
    plt.plot(data[:,0], data[:,1], label='Gong17 model')

    plt.title(r"$\rm H_\alpha, z=%.2f$"%(z))
    plt.legend()
    plt.xlabel(r"$\rm k (h/Mpc)$")
    plt.ylabel(r"$\rm k^3 P(k)/(2\pi^2) (Jy/sr)^2$")
    
    data = np.loadtxt(pa.data_master_loc+'pk_3d/halo_pk_a%.5f_f%g_M0_100.dat'%(a,fsample))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k, (k**3/(2*np.pi**2))*pk, label='original')

    plt.title('a=%g, f=%g'%(a, fsample))
    
    plt.legend()
    plt.savefig('../../plots/pk_3d/halpha_pk.pdf')
    plt.show()
'''
if __name__ == "__main__":
    write_pk_input_file()
    calc_pk()
    #plot_pk()

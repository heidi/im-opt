#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

'''
# half box: x < 0.5*L
# with Mpc/h
# agrees with previous results!! see halo_pk_3d_plot.py
'''

a = 0.16700
fsample = 1
#z = 1./a - 1.
name = 'halo_halfbox_a%.5f_f%g'%(a,fsample)
cat_name = pa.data_master_loc+'pk_3d_cat/%s.dat'%(name)
win_name = pa.data_master_loc+'pk_3d_cat/%s_win.dat'%(name)

ngrid = 128
Lbox = 250.
l_cell = Lbox/(ngrid*1.)

# write the file
def write_pk_input_file():
    fname = pa.data_master_loc+'halpha_cat/Ha_cat_a%0.5f_f%g.dat'%(a,fsample)
    ifile = open(fname, 'r')
    header = ifile.readline().split()

    outfile = open(cat_name, 'w')
    for line in ifile:
        halo = line.split()
        px = np.double(halo[0])#/pa.h
        py = np.double(halo[1])#/pa.h
        pz = np.double(halo[2])#/pa.h
        Mvir = 10**np.double(halo[3])
        if px < 0.5*Lbox:#./pa.h: # NOTE!!!!
            outfile.write('%g %g %g %g \n'%(px,py,pz,1))
    outfile.close()

px_min = 0
px_max = 0.5*Lbox
py_min = 0
py_max = Lbox
pz_min = 0
pz_max = Lbox
# write the window function!
n = ngrid
def write_window():
    window = np.zeros([n,n,n])
    for i in range(n):
        for j in range(n):
            for k in range(n):
                if i*l_cell >= px_min and (i+1)*l_cell <= px_max and \
                j*l_cell >= py_min and (j+1)*l_cell <= py_max and \
                k*l_cell >= pz_min and (k+1)*l_cell <= pz_max:
                    window[i,j,k] = 1
                    #print('w', window[i,j,k])
    np.savetxt(win_name, window.flatten())

from pk_3d_mastercode import PowerSpectrum3D
def calc_pk():
    ifname = cat_name
    ofname = pa.data_master_loc+'pk_3d/%s'%(name)
    ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=False, save_grid=False, divide_by_mean=True, use_window=True, win_name=win_name)
    ps3.make_halo_density_grid()
    ps3.fourier_transform()

if __name__ == "__main__":
    write_pk_input_file()
    write_window()
    calc_pk()


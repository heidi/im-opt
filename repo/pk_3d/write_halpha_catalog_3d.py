#!/usr/bin/env python
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from astropy.table import Table
from astropy.io import fits
import numpy.random as nr

sys.path.append('/home/hywu/work/CIB_opt/repo/model')
from sim import Sim
sim = Sim(name='BolshoiP')
fsample = 1#0.01

import sys
sys.path.append('../')
import params as pa
data_master_loc = pa.data_master_loc
bolshoip_loc = pa.bolshoip_loc

class LineCatalogs3D(object):
    def __init__(self, a = 0.16700):
        self.a = a
        self.redshift = 1./a-1

    def interp_Ha(self):
        data = np.loadtxt(data_master_loc+'halpha/Lalpha_z_lgM.dat')
        z_list = np.unique(data[:,0])
        lgM_list = np.unique(data[:,1])
        nz = len(z_list)
        nlgM = len(lgM_list)
        lgLHa_list = np.log10(data[:,2]).reshape(nz,nlgM)
        from scipy.interpolate import RectBivariateSpline
        self.lgLa_interp = RectBivariateSpline(z_list, lgM_list, lgLHa_list)

    def write_Ha_catalogs_3d(self):
        self.interp_Ha()
        # using the original Bolshoi-Planck
        fname = bolshoip_loc+'hlist_%.5f.list'%(self.a)
        ifile = open(fname, 'r')
        for dummy in range(100):
            header = ifile.readline().split()
            #print header[0]
            if sim.name=='Bolshoi': breakphrase = '#Vmax@Mpeak:' # for Bolshoi
            if sim.name=='BolshoiP': breakphrase = '#Tidal_Force_Tdyn:' # for BolshoiP 
            if header[0] == breakphrase: break

        outfile = open(data_master_loc+'halpha_cat/Ha_cat_a%0.5f_f%g.dat'%(self.a,fsample),'w')
        outfile.write('#px[Mpc/h], py, pz, lgMvir[Msun, no h], lgL_Ha[erg/s] \n')
        for line in ifile:
            if nr.rand() < fsample:
                halo = line.split()
                #for dummy in range(100):
                #halo = ifile.readline().split()
                h = sim.hubble
                ids = int(halo[1])
                Mvir = np.double(halo[10])/h
                lgM = np.log10(Mvir)
                px = np.double(halo[17])
                py = np.double(halo[18])
                pz = np.double(halo[19])
                lgLHa = self.lgLa_interp(self.redshift, lgM)
                #print('lgLHa', lgLHa, lgM)
                outfile.write('%g %g %g %g %g \n'%(px, py, pz, lgM, lgLHa))
        outfile.close()

    def plot_LM(self):
        data = np.loadtxt(data_master_loc+'pk_3d_cat/Ha_cat_a%0.5f_f%g.dat'%(self.a,fsample))
        lgM = data[:,3]
        lgL = data[:,4]
        plt.scatter(lgM, lgL)
        plt.savefig('../../plots/pk_3d/LM.pdf')
        plt.show()

if __name__ == "__main__":
    
    a_list = np.loadtxt(bolshoip_loc+'a_list.dat')[::2,2]
    z_list = np.loadtxt(bolshoip_loc+'a_list.dat')[::2,0]
    print(z_list)
    for a in [0.16700]:#a_list:
        lc = LineCatalogs3D(a=a)
        lc.write_Ha_catalogs_3d()
    '''
    lc = LineCatalogs3D()
    lc.plot_LM()
    '''

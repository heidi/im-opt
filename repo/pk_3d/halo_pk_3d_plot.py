#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa
pi = np.pi
loc = pa.data_master_loc + 'pk_3d/'

a = 0.16700
fsample = 1

plt.subplot(111,xscale='log',yscale='log')


eps = 1e-2
# halo_pk_3d_developement.py
fname = loc+'halo_pk_a0.16700_f1_M0_100.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b], k[b]**3/(2*pi**2)*pk[b], label='development')

# halo_pk_3d.py
fname = loc+'halo_a0.16700_f1_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]*(1+eps), k[b]**3/(2*pi**2)*pk[b], label='original')

# halo_pk_3d_noh.py
fname = loc+'halo_noh_a0.16700_f1_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+2*eps), k[b]**3/(2.*pi**2)*pk[b], label='no h')

# halpha_pk_3d_development.py
fname = loc+'halo_dev_a0.16700_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+2*eps), k[b]**3/(2.*pi**2)*pk[b], label='halpha dev no h')



#### with window ####
# halo_pk_3d_halfbox.py
fname = loc+'halo_halfbox_a0.16700_f1_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]*(1+3*eps), k[b]**3/(2.*pi**2)*pk[b], label='half box')

#### from lightcone ###
loc = pa.data_master_loc + 'pk_lightcone/'
# ../halo_pk_lightcone_ydir.py
fname = loc+'halo_ydir_z_5_5.3_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]*(1+3*eps), k[b]**3/(2.*pi**2)*pk[b], label='lightcone ydir')

# ../halo_pk_lightcone_zdir.py # no h
fname = loc+'halo_zdir_z_5_5.3_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+3*eps), k[b]**3/(2.*pi**2)*pk[b], label='lightcone zdir no h')

plt.legend(loc=2)
plt.xlabel(r"$\rm k [h/Mpc]$")
plt.ylabel(r"$\rm k^3 P(k)/(2\pi^2) $")
z = 1./a - 1.
plt.title(r"$\rm halos,\ z=%.2f$"%(z))
plt.savefig('../../plots/pk_3d/halo_pk_a%.5f_f%g.pdf'%(a,fsample))

plt.show()


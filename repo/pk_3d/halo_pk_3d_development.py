#!/usr/bin/env python
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

'''
superceeded by pk_3d_mastercode.py and halo_pk_3d.py
'''

pi = np.pi

L = 250 # size of the box
n = 128  # number of grid
l_cell = L/(n*1.) # size of the grid 
nk = 50
kmin = 2*np.pi/L
kmax = 2*np.pi/l_cell
print('kmin, kmax', kmin, kmax)



#0.01
a = 0.16700

class PowerSpectrum3D(object):
    def __init__(self, lgMmin=0, lgMmax=100, fsample=1):
        self.lgMmin = lgMmin
        self.lgMmax = lgMmax
        self.fsample = fsample
        print('lgM', self.lgMmin, self.lgMmax)

    def make_halo_density_grid(self):

        #data = np.loadtxt(data_master_loc+'pk_3d/Ha_cat_a%0.5f.dat'%(a))
        # x = data[:,0]
        # y = data[:,1]
        # z = data[:,2]
        # Mvir = 10**data[:,3]
        # L_Ha = 10**data[:,4]
        fname = pa.data_master_loc+'halpha_cat/Ha_cat_a%0.5f_f%g.dat'%(a,self.fsample)
        ifile = open(fname, 'r')
        header = ifile.readline().split()

        nh = 0
        fx = np.zeros([n,n,n])
        for line in ifile:
            halo = line.split()
            px = np.double(halo[0])
            py = np.double(halo[1])
            pz = np.double(halo[2])
            Mvir = 10**np.double(halo[3])
            L_Ha = 10**np.double(halo[4])


            #for ip in range(len(x)):
            i = min(int(np.floor(px/l_cell)),n-1)
            j = min(int(np.floor(py/l_cell)),n-1)
            k = min(int(np.floor(pz/l_cell)),n-1)

            #print(px, py, pz, i, j, k)
            #print(np.log10(Mvir[ip]))
            if True:#self.lgMmin < np.log10(Mvir) < self.lgMmax:
                fx[i,j,k] += 1
                nh += 1

        fx_flat = fx.flatten()
        print('np.mean(fx_flat)', np.mean(fx_flat))
        #print('nh', nh)

        np.savetxt(pa.data_master_loc+'/pk_3d_cat/halo_grid_a%0.5f_f%g_M%g_%g.dat'%(a,self.fsample,self.lgMmin,self.lgMmax), fx_flat)
        #print('mean_den', np.mean(fx_flat))

        # check density grid
        #data = np.loadtxt('../output/den_3d.dat')
        #fx_new = data.reshape((n,n,n))
        #print(fx_new - fx)


    def fourier_transform(self):
        data = np.loadtxt(pa.data_master_loc+'/pk_3d_cat/halo_grid_a%0.5f_f%g_M%g_%g.dat'%(a, self.fsample, self.lgMmin, self.lgMmax))
        mean_den = np.mean(data)
        print('mean_den', mean_den)
        fx = (data - mean_den)/mean_den
        fx = fx.reshape((n,n,n))
        print('self.fx[0,0,0]', fx[0,0,0])
        # create the grid based on Python convention
        k = np.zeros([n,n,n])
        for i1 in range(n):
            for i2 in range(n):
                for i3 in range(n):
                    if i1 < n/2: k1 = i1*2*pi/L
                    else: k1 = (i1-n)*2*pi/L
                    if i2 < n/2: k2 = i2*2*pi/L
                    else: k2 = (i2-n)*2*pi/L
                    if i3 < n/2: k3 = i3*2*pi/L
                    else: k3 = (i3-n)*2*pi/L
                    k[i1,i2,i3] = np.sqrt(k1**2+k2**2+k3**2)

        Fk = fft.fftn(fx)*(L/n)**3 # Fourier coefficients
        Fk = np.absolute(Fk)**2
        #print(Fk)
        #print('shape of Fk', len(Fk))

        # summing over k-modes
        k = k.flatten()
        Fk = Fk.flatten()

        b = (k > 0)
        k = k[b]
        Fk = Fk[b]

        lgk_list = np.linspace(np.log10(kmin), np.log10(kmax), nk+1)
        Pk_list = np.zeros(nk)
        lgPk_list = np.zeros(nk)
        dPk_list = np.zeros(nk)
        for ik in range(nk):
            b = (np.log10(k) > lgk_list[ik])*(np.log10(k) < lgk_list[ik+1])
            Pk_list[ik] = np.mean(Fk[b])/L**3 # NOTE! don't forget L^3
            lgPk_list[ik] = np.mean(np.log10(Fk[b]))

        outfile = open(pa.data_master_loc+'pk_3d/halo_pk_a%0.5f_f%g_M%g_%g.dat'%(a,self.fsample,self.lgMmin,self.lgMmax),'w')
        outfile.write('#k [h/Mpc], Pk [Mpc^-3 h^3] \n')
        for i in range(nk):
            outfile.write('%g %g\n'%(10**(0.5*(lgk_list[i]+lgk_list[i+1])), Pk_list[i]))
        outfile.close()



    def calc_halo_bias(self):
        data = np.loadtxt(pa.data_master_loc+'pk_3d/halo_pk_a%0.5f_f%g_M%g_%g.dat'%(a,self.fsample,self.lgMmin,self.lgMmax))
        k = data[:,0]
        Pk = data[:,1]
        plt.subplot(111,xscale='log',yscale='log')
        plt.plot(k, Pk, label='f=%g, %g < M< %g'%(self.fsample,self.lgMmin,self.lgMmax))#label='halo, lgM > %g'%(self.lgMmin))
        from scipy.interpolate import interp1d
        Pk_sim = interp1d(k, Pk)

        # growth rate
        data = np.loadtxt('/home/hywu/work/CIB_bathtub_v2/repo/data/data_interp/Growth_z.dat')
        growth = interp1d(data[:,0], data[:,1])

        # liner PS
        data = np.loadtxt('/home/hywu/work/CIB_bathtub_v2/repo/data/data_interp/Planck14_matterpower.dat')
        k = data[:,0]
        z = 5
        Pk = data[:,1]*(growth(z)**2)
        b = (k>0.01)*(k < 10)     
        plt.plot(k[b], Pk[b], ls='--', c='gray')#, label='linear, z=%g'%(z))

        # b2 = (k > 0.1)*(k < 0.7)
        # bh = np.sqrt(np.median(Pk_sim(k[b2])/Pk[b2]))
        #plt.title(r"$\rm halo\ bias\ = %.2f$"%(bh))
        plt.legend()
        plt.xlabel(r"$\rm k [h/Mpc]$")
        plt.ylabel(r"$\rm P(k) [(h/Mpc)^3]$")
        #

        #plt.xlim(1.e-2, 10)
        #plt.ylim(100,1.e+5)
    

if __name__ == "__main__":
    ps3 = PowerSpectrum3D(lgMmin=0, lgMmax=100, fsample=1)#0.01)
    ps3.make_halo_density_grid()
    ps3.fourier_transform()
    ps3.calc_halo_bias()
    plt.show()

    '''
    for fsample in [1, 0.01]:
        for lgMmin in [10, 11]:
            lgMmax = lgMmin + 1
            ps3 = PowerSpectrum3D(lgMmin=lgMmin, lgMmax=lgMmax, fsample=fsample)
            #ps3.make_halo_density_grid()
            #ps3.fourier_transform()
            ps3.calc_halo_bias()
    plt.savefig('../../plots/pk_3d/halo_pk.pdf')#_f%g.pdf'%(fsample))
    plt.show()
    '''


#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

'''
coped from halo_pk_3d_mastercode_demo.py
no h!
agrees with previous results!! see halo_pk_3d_plot.py
'''

a = 0.16700
fsample = 1

ngrid = 128
Lbox = 250./pa.h

z = 1./a - 1.
name = 'halo_noh_a%.5f_f%g'%(a,fsample)
cat_name = pa.data_master_loc+'pk_3d_cat/%s.dat'%(name)

def write_pk_input_file():
    fname = pa.data_master_loc+'halpha_cat/Ha_cat_a%0.5f_f%g.dat'%(a,fsample)
    ifile = open(fname, 'r')
    header = ifile.readline().split()

    outfile = open(cat_name, 'w')
    for line in ifile:
        halo = line.split()
        px = np.double(halo[0])/pa.h
        py = np.double(halo[1])/pa.h
        pz = np.double(halo[2])/pa.h
        outfile.write('%g %g %g %g \n'%(px,py,pz,1))
    outfile.close()


from pk_3d_mastercode import PowerSpectrum3D
def calc_pk():
    ifname = cat_name
    ofname = pa.data_master_loc+'pk_3d/%s'%(name)
    ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=False, save_grid=False, divide_by_mean=True)
    ps3.make_halo_density_grid()
    ps3.fourier_transform()


if __name__ == "__main__":
    write_pk_input_file()
    calc_pk()

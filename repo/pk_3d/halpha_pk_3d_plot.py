#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa
pi = np.pi
loc = pa.data_master_loc + 'pk_3d/'

a = 0.16700
fsample = 1

plt.subplot(111,xscale='log',yscale='log')

# Gong 17
data = np.loadtxt('../data_Gong17/fig5a_pk_Ha_z4.8.dat')
plt.plot(data[:,0], data[:,1], label='Gong17')


eps = 1e-2

# halpha_pk_3d_developement.py # no h
fname = loc+'halpha_dev_a0.16700_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h, k[b]**3/(2*pi**2)*pk[b], label='development')

# halpha_pk_3d.py # no h
fname = loc+'halpha_a0.16700_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+2*eps), k[b]**3/(2*pi**2)*pk[b], label='original')

### from lightcone ###
loc = pa.data_master_loc + 'pk_lightcone/'
# ../pk_lightcone/halo_pk_lightcone_zdir.py # no h
fname = loc+'halpha_zdir_z_5_5.3_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+3*eps), k[b]**3/(2.*pi**2)*pk[b], label='lightcone zdir no h')

### from cube ###
loc = pa.data_master_loc + 'pk_cube/'
# ../pk_lightcone/halo_pk_lightcone_zdir.py # no h
fname = loc+'halpha_z_5_5.3_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+3*eps), k[b]**3/(2.*pi**2)*pk[b], label='cube, 5<z<5.3')

fname = loc+'halpha_z_5_6_pk.dat'
data = np.loadtxt(fname)
k = data[:,0]
pk = data[:,1]
b = (pk > 0)
plt.plot(k[b]/pa.h*(1+3*eps), k[b]**3/(2.*pi**2)*pk[b], label='cube, 5<z<6')






plt.legend(loc=2)
plt.xlabel(r"$\rm k [h/Mpc]$")
plt.ylabel(r"$\rm k^3 P(k)/(2\pi^2) [(Jy/sr)^2]$")
z = 1./a - 1.
plt.title(r"$\rm H_\alpha,\ z=%.2f$"%(z))
plt.savefig('../../plots/pk_3d/halpha_pk_a%.5f.pdf'%(a))

plt.show()


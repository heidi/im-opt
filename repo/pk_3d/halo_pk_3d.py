#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

'''
# agreed with halo_pk_3d_development.py
# using Mpc/h
'''

a = 0.16700
fsample = 1#0.01

ngrid = 128
Lbox = 250.

z = 1./a - 1.
name = 'halo_a%.5f_f%g'%(a,fsample)
cat_name = pa.data_master_loc+'pk_3d_cat/%s.dat'%(name)

# write the file
def write_pk_input_file():
    fname = pa.data_master_loc+'halpha_cat/Ha_cat_a%0.5f_f%g.dat'%(a,fsample)
    ifile = open(fname, 'r')
    header = ifile.readline().split()

    # chi = pa.cosmo.comoving_distance(z).value
    # DH = 3000. / pa.h
    # DA = chi/(1+z)
    # DL = chi*(1+z)
    # Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)

    outfile = open(cat_name, 'w')
    for line in ifile:
        halo = line.split()
        px = np.double(halo[0])#/pa.h
        py = np.double(halo[1])#/pa.h
        pz = np.double(halo[2])#/pa.h
        # Mvir = 10**np.double(halo[3])
        # L_halpha = 10**np.double(halo[4])
        
        # #print(px, py, pz, Mvir, L_Ha)
        # nu = (3.e+8)/(6563.*1.e-10)
        # dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
        # S_halpha = L_halpha / (4*np.pi*DL**2) * (DA**2) * dchi_dnu

        outfile.write('%g %g %g %g \n'%(px,py,pz,1))
    outfile.close()


from pk_3d_mastercode import PowerSpectrum3D
def calc_pk():
    ifname = cat_name
    ofname = pa.data_master_loc+'pk_3d/%s'%(name)
    ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=False, save_grid=False, divide_by_mean=True)#/pa.h
    ps3.make_halo_density_grid()
    ps3.fourier_transform()

'''
# superceeded by halo_pk_3d_plot.py
def plot_pk():
    plt.subplot(111, xscale='log', yscale='log')
    data = np.loadtxt(pa.data_master_loc+'pk_3d/%s_pk.dat'%(name))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k, (k**3/(2*np.pi**2))*pk, label='demo')

    data = np.loadtxt(pa.data_master_loc+'pk_3d/halo_pk_a%.5f_f%g_M0_100.dat'%(a,fsample))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k, (k**3/(2*np.pi**2))*pk, label='original')

    plt.title('a=%g, f=%g'%(a, fsample))
    plt.legend()
    plt.show()
'''

if __name__ == "__main__":
    write_pk_input_file()
    calc_pk()
    #plot_pk()

#!/usr/bin/env python
import numpy as np
#import matplotlib.pyplot as plt
import astropy
#import numpy.random as nr


data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

data_loc = data_master_loc+'data/Behroozi_SFH_v2/'
a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')


#print('len(a_list)', len(a_list))
#print('len(m_list)', len(m_list))

#wave = np.loadtxt(data_master_loc+'sed_m_z/wavelength.dat')
wave = np.loadtxt('../../output/wavelength.dat')

for ia in range(len(a_list)):
    a = a_list[ia]
    z = 1./a - 1
    if z > -1:#>= 5.5 and z <= 7.5:
        data = np.loadtxt(data_master_loc+'sed_m_z/sed_lib_%i.dat'%(ia))

        for iM in range(len(m_list)):
            spec = data[iM,1:]
            np.savetxt(data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,iM),spec)
    #plt.subplot(111, xscale='log', yscale='log')
    #print('len(spec)', len(spec))
    #plt.plot(wave, spec)
    #plt.show()
    #return spec

#get_spectrum(lgM=14, z=1.2)


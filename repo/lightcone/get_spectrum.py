#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import astropy
#import matplotlib.pyplot as plt
#import numpy.random as nr
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=67.8, Om0=0.307)

data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

data_loc = data_master_loc+'data/Behroozi_SFH_v2/'
a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')

#print('len(a_list)', len(a_list))
#print('len(m_list)', len(m_list))

wave = np.loadtxt(data_master_loc+'sed_m_z/wavelength.dat')
#wave = np.loadtxt('../../output/wavelength.dat')

LsunMpc2Hz_to_Jansky = 4.e+7

def get_spectrum(lgM, z):
    a = 1./(1.+z)
    b = (abs(m_list - lgM) == min(abs(m_list - lgM)))
    iM = np.arange(len(m_list))[b][0]
    b = (abs(a_list - a) == min(abs(a_list - a)))
    ia = np.arange(len(a_list))[b][0]
    #print(iM, ia)

    
    # plt.figure()
    # data = np.loadtxt(data_master_loc+'sed_m_z/sed_lib_%i.dat'%(ia))
    # spec = data[iM,1:]
    # print('len(spec)', len(spec))
    # b = (wave > 1000)&(wave < 1.e+4)
    # plt.plot(wave[b], spec[b])
    
    #plt.figure()
    spec = np.loadtxt(data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,iM))
    #print('len(spec)', len(spec))
    b = (wave > 1000)&(wave < 1.e+4)
    #plt.plot(wave[b], spec[b]/max(spec[b]))
    
    chi = cosmo.comoving_distance(z=z).value
    spec = spec/(4*np.pi*chi*chi*(1+z))*LsunMpc2Hz_to_Jansky

    #plt.figure()
    #plt.plot(wave[b], spec[b]/max(spec[b]))    
    #plt.show()

    return spec

if __name__ == "__main__":

    # just looking at the spectra
    plt.figure(figsize=(14,14))
    z_list = [0.01, 3, 6, 9]
    for iz in range(4):
        z = z_list[iz]
        ax = plt.subplot(2,2,iz+1,yscale='log')
        #z = 6
        wave_obs = wave * (1+z)
        b = (wave_obs > 5000)&(wave_obs < 50000)

        lgM_list = range(14,8,-1)
        if z > 4: lgM_list = range(13,8,-1)
        if z > 7: lgM_list = range(12,8,-1)
        for lgM in lgM_list:
            spec = get_spectrum(lgM=lgM, z=z)        
            
            
            plt.plot(wave_obs[b]*1.e-4, spec[b], label=r"$log_{10}M=%i$"%(lgM))
        plt.title(r"$\rm z=%g$"%(z))
        #ax.set_yticklabels(fontsize='small')
        plt.tick_params(labelsize='small')

        plt.xlabel(r"$\lambda_{obs} [\mu m]$")
        plt.ylabel(r"$\rm S_\nu [L_\odot/Hz]$")
        plt.legend()
    plt.savefig('../../plots/spectra_example.pdf')
    plt.show()
    

    # testing the interpolation scheme...
    #import matplotlib.pyplot as plt
    z = 5#0.005
    lgM = 9
    min_lam = 5.e+3
    max_lam = 5.e+4
    spec = get_spectrum(lgM=lgM, z=z)
    wave_obs = wave*(1+z)
    b = (wave_obs > min_lam)&(wave_obs < max_lam)
    print('len(wave_obs[b])', len(wave_obs[b]))
    plt.figure()
    plt.plot(wave_obs[b], spec[b])

    wave_output = np.linspace(min_lam, max_lam, 5000)
    from scipy.interpolate import interp1d
    b = (wave_obs >= 0.9*min_lam)&(wave_obs <= 1.1*max_lam)
    spec_interp = interp1d(wave_obs[b], spec[b])
    #print('len(wave_obs[b])', len(wave_obs[b]))
    #spec_interp = interp1d(wave_obs, spec)
    spec_output = spec_interp(wave_output)
    plt.figure()
    plt.plot(wave_output, spec_output, c='r')

    plt.show()



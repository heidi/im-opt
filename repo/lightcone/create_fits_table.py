#!/usr/bin/env python
# example from Daniel
from astropy.io import fits
def create_FITStable(ras, decs, zs, ms, specs):
    """
    ras, decs 1D array
    spectra with shape (ras.shape, n_channels)
    """

    c1 = fits.Column(name='ra', format='D', array=ras)
    c2 = fits.Column(name='dec', format='D', array=decs)
    c3 = fits.Column(name='z', format='D', array=zs)
    c4 = fits.Column(name='mh', format='E', array=ms)

    c5 = fits.Column(
        name='spectra',
        format='{}E'.format(int(specs.shape[1])),
        array=specs)

    coldefs = fits.ColDefs([c1, c2, c3, c4, c5])
    tbhdu = fits.BinTableHDU.from_columns(coldefs)

    return tbhdu


'''
catalogue = create_FITStable(
        ras=locations['ras'],
        decs=locations['decs'],
        specs=specs)
catalogue.writeto(outdir + 'catalogue.fits', clobber=True)


'''


# def generate_catalogue(
#         box, gal_priors, locations):
#     # generate spectra with the HI profile code
#     specs = spectra.generate_spectra(
#         gal_priors,
#         ra=locations['ras'],
#         dec=locations['decs'],
#         velocity_grid=box['velocity_grid'].value,
#         v_rad=locations['velos'])
#    return specs, catalogue




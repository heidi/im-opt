#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import astropy

data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

'''
def read_ascii():
    data = np.loadtxt(data_master_loc+'output_catalog/catalog_z6.dat')
    #line number 21685
    # z, ra, dec, Mh
    z = data[:,0]
    ra = data[:,1]
    dec = data[:,2]
    Mh = data[:,3]
    #plt.hist(np.log10(Mh))
    #plt.show()
    data = np.loadtxt(data_master_loc+'output_catalog/spec_z6.dat')
    # wavelength: 1000 log bins between 1000 and 1.e+7 AA
    min_lam = 1000
    max_lam = 1.e+7
    wave = 10**np.linspace(np.log10(min_lam), np.log10(max_lam), 1000)
    for ih in [0, 3, 9]:
        spec = data[ih,:]
        plt.loglog(wave, spec)
    plt.show()
'''

def read_lightcone(zmin):
    zmax = zmin + 1
    from astropy.table import Table
    t = Table.read(data_master_loc+'output_catalog/spectra_z_%i_%i.fits'%(zmin,zmax), hdu=0)
    print(t.info)
    ra_list = t['ra']
    dec_list = t['dec']
    z_list = t['z']
    spec = t['spectra']
    print(np.shape(spec))
    min_lam = 1000
    max_lam = 1.e+4#1.e+7
    nw = 100
    wave = np.linspace(min_lam, max_lam, nw)
    #plt.scatter(ra_list, dec_list)
    #plt.hist(z_list)
    # plt.figure()
    for i in [0]:#range(0,-1,1000):
        plt.loglog(wave, spec[i,:])
    
    # mh_list = t['MHALO'].data[0]

#for zmin in range(10):
#    read_lightcone(zmin=zmin)
#    plt.show()




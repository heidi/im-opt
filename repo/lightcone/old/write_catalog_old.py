#!/usr/bin/env python
import timeit
start = timeit.default_timer()

import numpy as np
#import matplotlib.pyplot as plt
import astropy
#import numpy.random as nr

data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

# ngal = 3
# nwave = 10
# x = np.arange(ngal)
# y = nr.rand(ngal, nwave)
# print(y)
'''
import astropy
from astropy.io import fits
hdulist = fits.open('../../data/Mock_cat_Bethermin2017.fits')
# overview of HDU's
hdulist.info()
# primaryHDU
hdulist[0].header
hdulist[0].data
'''

z_wanted = 6

from astropy.table import Table
t = Table.read(data_master_loc+'data/Mock_cat_Bethermin2017.fits', hdu=2)
#print(t.info)
ra_list = t['RA'].data[0]
dec_list = t['DEC'].data[0]
z_list = t['REDSHIFT'].data[0]
mh_list = t['MHALO'].data[0]

b = (z_list > z_wanted)*(z_list <= z_wanted+1)
ra_list = ra_list[b][0:10]
dec_list = dec_list[b][0:10]
z_list = z_list[b][0:10]
mh_list = mh_list[b][0:10]
print('len(mh_list)', len(mh_list))


from get_spectrum import *

wave = np.loadtxt(data_master_loc+'sed_m_z/wavelength.dat')
#print(wave)

#print('wavelength', min(wave)*20, max(wave))
min_lam = 1000
max_lam = 1.e+7
wave_output = np.arange(1000, 1.e+4, 1000)
#10**np.linspace(np.log10(min_lam), np.log10(max_lam), 1000)

# plt.plot(wave_obs, spec)
# plt.plot(wave_output, spec_output)
# plt.show()
# exit()

outfile1 = open(data_master_loc+'output_catalog/catalog_z%i_test.dat'%(z_wanted),'w')
outfile1.write('# z, ra, dec, Mh\n')

outfile2 = open(data_master_loc+'output_catalog/spec_z%i_test.dat'%(z_wanted),'w')
outfile2.write('# wavelength: 1000 log bins between %e and %e AA \n'%(min_lam, max_lam))

for ih in range(len(mh_list)):
    outfile1.write('%g %g %g %g \n'%(z_list[ih], ra_list[ih], dec_list[ih], mh_list[ih]))

    wave_obs = wave * (1+z_list[ih])
    spec = get_spectrum(lgM=np.log10(mh_list[ih]), z=z_list[ih])
    from scipy.interpolate import interp1d
    spec_interp = interp1d(wave_obs, spec)
    spec_output = spec_interp(wave_output)
    for i in range(len(spec_output)):
        outfile2.write('%g '%(spec_output[i]))
    outfile2.write('\n')

outfile1.close()
outfile2.close()

stop = timeit.default_timer()
print('took', stop - start, 'seconds')


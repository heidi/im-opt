#!/usr/bin/env python
import timeit
start = timeit.default_timer()
import sys
import numpy as np
#import astropy
#import numpy.random as nr

data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

zmin = int(sys.argv[1])
zmax = zmin + 1

# from astropy.table import Table
# t = Table.read(data_master_loc+'data/Mock_cat_Bethermin2017.fits', hdu=2)
# #print(t.info)
# ra_list = t['RA'].data[0]
# dec_list = t['DEC'].data[0]
# z_list = t['REDSHIFT'].data[0]
# mh_list = t['MHALO'].data[0]

lightcone_name = np.loadtxt('/panfs/ds08/hopkins/hywu/work/lightcone/Bethermin_v4_AbMatch/lightcone/BolshoiP_Bethermin_Mstar_z_%i_%i.dat'%(zmin,zmax))
#ra[0], dec[1], z[2], M200[3,Msun], Mstar[4,Msun]
ra_list = data[:,0]
dec_list = data[:,1]
z_list = data[:,2]
mh_list = data[:,3]
'''
b = (z_list > zmin)*(z_list <= zmax)
ra_list = ra_list[b]#[0:10]
dec_list = dec_list[b]#[0:10]
z_list = z_list[b]#[0:10]
mh_list = mh_list[b]#[0:10]
'''
nh = len(mh_list)
print('len(mh_list)', nh)

from get_spectrum import *

wave = np.loadtxt(data_master_loc+'sed_m_z/wavelength.dat')
#print(wave)

#print('wavelength', min(wave)*20, max(wave))

Ha_lam = 6563
min_lam = 6563*(1+zmin)#5.e+3
max_lam = 6563*(1+zmax)#5.e+4#1.e+7
nw = 1000
wave_output = np.linspace(min_lam, max_lam, nw)

#10**np.linspace(np.log10(min_lam), np.log10(max_lam), 1000)

# import matplotlib.pyplot as plt
# plt.plot(wave_obs, spec)
# plt.plot(wave_output, spec_output)
# plt.show()
# exit()

# outfile1 = open(data_master_loc+'output_catalog/catalog_z%i.dat'%(z_wanted),'w')
# outfile1.write('# z, ra, dec, Mh\n')

# outfile2 = open(data_master_loc+'output_catalog/spec_z%i.dat'%(z_wanted),'w')
# outfile2.write('# wavelength: 1000 log bins between %e and %e AA \n'%(min_lam, max_lam))

spec_output_masterlist = np.zeros([nh, nw])

for ih in range(len(mh_list)):
    #outfile1.write('%g %g %g %g \n'%(z_list[ih], ra_list[ih], dec_list[ih], mh_list[ih]))
    wave_obs = wave * (1+z_list[ih])
    spec = get_spectrum(lgM=np.log10(mh_list[ih]), z=z_list[ih])
    from scipy.interpolate import interp1d
    b = (wave_obs >= 0.9*min_lam)&(wave_obs <= 1.1*max_lam)
    spec_interp = interp1d(wave_obs[b], spec[b])
    #print('len(wave_obs[b])', len(wave_obs[b]))
    #spec_interp = interp1d(wave_obs, spec)
    spec_output = spec_interp(wave_output)

    spec_output_masterlist[ih,:] = spec_output

    #for i in range(len(spec_output)):
    #    outfile2.write('%g '%(spec_output[i]))
    #outfile2.write('\n')

# outfile1.close()
# outfile2.close()

from create_fits_table import create_FITStable
catalogue = create_FITStable(
        ras=ra_list,
        decs=dec_list,
        zs=z_list,
        ms=mh_list,
        specs=spec_output_masterlist)

catalogue.writeto(data_master_loc+'output_catalog/spectra_z_%i_%i.fits'%(zmin,zmax), clobber=True)



stop = timeit.default_timer()
print('took', stop - start, 'seconds')


#!/usr/bin/env python
import numpy as np
#import matplotlib.pyplot as plt

from scipy.integrate import quad
from scipy.interpolate import interp1d

import IM_opt.repo.params as pa 
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')


class LineLuminosity(object):
    def __init__(self, line_name='Halpha'):
        self.line_name = line_name

        data_loc = pa.data_master_loc+'data/Behroozi_SFH_v2/'
        self.a_sed_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
        self.m_sed_list = np.loadtxt(data_loc+'m_list.dat')

        if line_name == 'Halpha': lam_line_rest_um = 0.6563
        if line_name == 'Hbeta': lam_line_rest_um = 0.4861

        # working in rest frame 
        wave0 = np.loadtxt(pa.data_master_loc+'sed_m_z/wavelength.dat')*1.e-4 # AA to um
        wave_min = lam_line_rest_um-0.001
        wave_max = lam_line_rest_um+0.001
        self.b_wavelength = (wave0 > wave_min)&(wave0 < wave_max)
        wave = wave0[self.b_wavelength]
        freq = (3.e+8)/(wave*1.e-6)
        self.freq = freq[::-1]

    def get_line_lum(self, lgM, z):
        a = 1./(1.+z)
        b0 = (abs(self.m_sed_list - lgM) == min(abs(self.m_sed_list - lgM)))
        iM = np.arange(len(self.m_sed_list))[b0][0]
        b0 = (abs(self.a_sed_list - a) == min(abs(self.a_sed_list - a)))
        ia = np.arange(len(self.a_sed_list))[b0][0]

        spec0 = np.loadtxt(pa.data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,iM))
        spec = spec0[self.b_wavelength]
        spec = spec - min(spec)
        spec = spec[::-1]
        spec_interp = interp1d(self.freq,spec)
        L_line = quad(spec_interp, min(self.freq), max(self.freq))[0]*pa.Lsun2ergs
        return L_line

    def save_Lalpha_z_lgM(self):
        outfile = open(pa.data_master_loc+'lines_for_spherex/%s_z_lgM.dat'%(self.line_name),'w')
        outfile.write('#z, lgM[Msun], log10(luminoisty)[erg/s] \n') 
        for a in self.a_sed_list:
            for m in self.m_sed_list:
                z = 1./a-1
                lgM = m
                lum = self.get_line_lum(lgM=lgM, z=z)
                outfile.write('%g %g %g\n'%(z, lgM, np.log10(lum)))
        outfile.close()


if __name__ == "__main__":
    ll = LineLuminosity(line_name='Hbeta')
    ll.save_Lalpha_z_lgM()
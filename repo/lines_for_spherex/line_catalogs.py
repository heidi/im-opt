#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
#from astropy.table import Table
from astropy.io import fits
from scipy.interpolate import RectBivariateSpline

import IM_opt.repo.params as pa

class LineCatalogs(object):
    def __init__(self, zmin=0):
        self.zmin = zmin
        self.zmax = zmin + 1

    def interp_line(self, line_name='Halpha'):
        data = np.loadtxt(pa.data_master_loc+'lines_for_spherex/%s_z_lgM.dat'%(line_name))
        z_list = np.unique(data[:,0])
        lgM_list = np.unique(data[:,1])
        nz = len(z_list)
        nlgM = len(lgM_list)
        lgL_list = data[:,2].reshape(nz,nlgM)
        return RectBivariateSpline(z_list, lgM_list, lgL_list)

    def write_line_cat(self):
        lgHalpha_interp = self.interp_line(line_name='Halpha')
        lgHbeta_interp = self.interp_line(line_name='Hbeta')
        
        data = np.loadtxt('/panfs/ds08/hopkins/hywu/work/lightcone/Bethermin_v4_AbMatch/lightcone/BolshoiP_Bethermin_Mstar_z_%i_%i.dat'%(self.zmin,self.zmax))
        #ra[0], dec[1], z[2], M200[3,Msun], Mstar[4,Msun]
        ra = data[:,0]
        dec = data[:,1]
        z = data[:,2]
        lgM = np.log10(data[:,3]) # WHICH MASS????

        
        # ra = np.linspace(0,1.39, 10)
        # dec = np.linspace(0,1.39, 10)
        # z = np.linspace(0.01, 1, 10)
        # lgM = np.linspace(9, 14, 10)
        nh = len(z)
        lgHalpha_list = np.zeros(nh)
        lgHbeta_list = np.zeros(nh)
        for i in range(nh):
            lgHalpha_list[i] = lgHalpha_interp(z[i], lgM[i])
            lgHbeta_list[i] = lgHbeta_interp(z[i], lgM[i])
        c0 = fits.Column(name='ra', format='D', array=ra)
        c1 = fits.Column(name='dec', format='D', array=dec)
        c2 = fits.Column(name='z', format='D', array=z)
        c3 = fits.Column(name='lgM', format='D', array=lgM)
        c4 = fits.Column(name='lgHalpha', format='D', array=lgHalpha_list)
        c5 = fits.Column(name='lgHbeta', format='D', array=lgHbeta_list)
        coldefs = fits.ColDefs([c0, c1, c2, c3, c4, c5])
        tbhdu = fits.BinTableHDU.from_columns(coldefs)
        tbhdu.writeto(pa.data_master_loc+'lines_for_spherex/lines_z_%i_%i.fits'%(self.zmin, self.zmax), clobber=True)


if __name__ == "__main__":
    zmin = int(sys.argv[1])
    for zmin in [zmin]:
         wlcf = LineCatalogs(zmin=zmin)
         wlcf.write_line_cat()
#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import IM_opt.repo.params as pa 
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')

# modified from ../pk_cube/write_cube_old.py
class MakeCubeLines(object):
    def __init__(self, line_name='Halpha'):
        self.line_name = line_name
        if line_name == 'Halpha': self.lam_line_rest_um = 0.6563
        if line_name == 'Hbeta': self.lam_line_rest_um = 0.4861

        data = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/spherex_lambda_bins.dat')
        self.lam_id = data[:,0]
        self.lam_min_list = data[:,1]
        self.lam_max_list = data[:,2]

    def write_cube(self):

        l_survey = 1.4 # deg
        n_ra = 812 #int(l_ra/l_ra)
        l_ra = l_survey/n_ra #6.2 arcsecond, spherex
        n_dec = n_ra
        l_dec = l_ra

        n_lam = 96


        data_cube = np.zeros((n_ra, n_dec, n_lam))
        for zmin in range(10):
            zmax = zmin + 1
            fname = pa.data_master_loc+'lines_for_spherex/lines_z_%i_%i.fits'%(zmin,zmax)
            from astropy.io import fits
            d = fits.getdata(fname)
            z = d['z']
            ra = d['ra']
            dec = d['dec']
            lgM = d['lgM']
            
            #if line_name == 'Halpha': lgL = d['lgHalpha']
            #if line_name == 'Hbeta': lgL = d['lgHbeta']
            lgL_line = d['lg'+self.line_name]

            chi = pa.cosmo.comoving_distance(z).value
            L_line = 10**lgL_line
            DH = 3000. / pa.h # no h
            DA = chi/(1+z)
            DL = chi*(1+z)
            Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)
            nu = (3.e+8)/(self.lam_line_rest_um*1.e-6)
            dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
            S_line = L_line / (4*np.pi*DL**2) * (DA**2) * dchi_dnu
            # erg/s Mpc/Hz 1/sr
            # note! divide by cell vol later
            
            for igal in range(len(z)):
                lam_obs = self.lam_line_rest_um * (1+z[igal])
                b = (lam_obs > self.lam_min_list)&(lam_obs < self.lam_max_list)
                if len(self.lam_id[b]) > 0:
                    k = int(self.lam_id[b][0]+1.e-3)
                    i = min(n_ra-1, int(ra[igal]/l_ra))
                    j = min(n_ra-1, int(dec[igal]/l_dec))
                    data_cube[i,j,k] += S_line[igal]
            
        for ilam in range(n_lam):
            zmin_line = self.lam_min_list[ilam]/self.lam_line_rest_um - 1 
            zmax_line = self.lam_max_list[ilam]/self.lam_line_rest_um - 1 

            voxel_vol = (sim.VC_interp(zmax_line)-sim.VC_interp(zmin_line))*params.fsky/(n_ra**2)
            np.savetxt(pa.data_master_loc+'lines_for_spherex/%s_lam_%i.dat'%(self.line_name, ilam), data_cube[:,:,ilam] / voxel_vol * pa.Mpc2cm**-2 * pa.cgs2Jy, fmt ='%g')
        # erg s-1 Mpc-2 Hz-1 to Jy

    def plot_cube(self):
        for ilam in range(0,96,10):
            plt.figure()
            data = np.loadtxt(pa.data_master_loc+'lines_for_spherex/%s_lam_%i.dat'%(self.line_name, ilam))
            p1 = plt.imshow(np.arctan(data))
            #p1 = plt.imshow(data)
            print('mean', np.mean(data),'Jy/sr')
            #plt.colorbar()
            p1.axes.set_xticklabels([])
            p1.axes.set_yticklabels([])
            if self.line_name == 'Halpha': name = 'alpha'
            if self.line_name == 'Hbeta': name = 'beta'
                

            plt.title(r"$H\%s,\ band\ %i,\ %.2f - %.2f \mu m,$"%(name,ilam, self.lam_min_list[ilam], self.lam_max_list[ilam])+'\n'+r"$\rm mean=%g \ Jy/sr$"%(np.mean(data)))
            

            plt.savefig('../../plots/lines_for_spherex/%s_lam_%i.png'%(self.line_name, ilam))
        

    # def plot_slice():
    #     for ilam in [70]:#range(70, 81):
    #         plt.figure()
    #         data = np.loadtxt(pa.data_master_loc+'pk_cube/halpha_lam_%i.dat'%(ilam))
    #         p1 = plt.imshow(np.arctan(data))
    #         print('mean', np.mean(data))
    #         #plt.colorbar()
    #         p1.axes.set_xticklabels([])
    #         p1.axes.set_yticklabels([])
    #         plt.title(r"$H\alpha,\ band\ %i,\ %.2f - %.2f \mu m$"%(ilam, lam_min_list[ilam], lam_max_list[ilam]))
    #         plt.savefig(pa.data_master_loc+'pk_cube_images/halpha_lam_%i.png'%(ilam))


    # def plot_all_slices():
    #     data = np.zeros((n_ra, n_ra))
    #     for ilam in range(70,81):#range(n_lam):
    #         data += np.loadtxt(pa.data_master_loc+'cii_cube/band_%i.dat'%(ilam))
    #     plt.figure()
    #     p1 = plt.imshow(np.arctan(data))
    #     print('mean', np.mean(data))
    #     p1.axes.set_xticklabels([])
    #     p1.axes.set_yticklabels([])
    #     plt.show()



if __name__ == "__main__":
    mcl = MakeCubeLines(line_name='Halpha')
    #mcl.write_cube()
    mcl.plot_cube()

    mcl = MakeCubeLines(line_name='Hbeta')
    #mcl.write_cube()
    mcl.plot_cube()

    #plt.show()

#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import IM_opt.repo.params as pa 
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')


# modified from ../pk_cube/write_cube_old.py
class PlotCubeLines(object):
    def __init__(self, line_name='Halpha'):
        self.line_name = line_name
        if line_name == 'Halpha': self.lam_line_rest_um = 0.6563
        if line_name == 'Hbeta': self.lam_line_rest_um = 0.4861

    # def plot_cube(self):

    #     np.savetxt(pa.data_master_loc+'lines_for_spherex/%s_lam_%i.dat'%(self.line_name, ilam))

    def plot_cube(self):
        for ilam in [70]:#range(70, 81):
            plt.figure()
            data = np.loadtxt(pa.data_master_loc+'lines_for_spherex/%s_lam_%i.dat'%(self.line_name, ilam))
            p1 = plt.imshow(np.arctan(data))
            print('mean', np.mean(data))
            #plt.colorbar()
            p1.axes.set_xticklabels([])
            p1.axes.set_yticklabels([])
            #plt.title(r"$H\alpha,\ band\ %i,\ %.2f - %.2f \mu m$"%(ilam, lam_min_list[ilam], lam_max_list[ilam]))
            #plt.savefig(pa.data_master_loc+'pk_cube_images/halpha_lam_%i.png'%(ilam))
        plt.show()

    # def plot_all_slices():
    #     data = np.zeros((n_ra, n_ra))
    #     for ilam in range(70,81):#range(n_lam):
    #         data += np.loadtxt(pa.data_master_loc+'cii_cube/band_%i.dat'%(ilam))
    #     plt.figure()
    #     p1 = plt.imshow(np.arctan(data))
    #     print('mean', np.mean(data))
    #     p1.axes.set_xticklabels([])
    #     p1.axes.set_yticklabels([])
    #     plt.show()



if __name__ == "__main__":
    mcl = PlotCubeLines(line_name='Hbeta')
    mcl.plot_cube()



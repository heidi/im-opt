#!/usr/bin/env python
import numpy as np

data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'
data_loc = data_master_loc
bolshoip_loc = '/panfs/ds08/hopkins/hywu/work/Catalogs/Bolshoi_Planck/'


Mpc2cm = 3.086e+24
cgs2Jy = 1.e+23
LsunMpc2Hz_to_Jansky = 4.e+7
Lsun2ergs = 3.839e+33

from astropy.cosmology import FlatLambdaCDM
h = 0.678
H0 = 100. * h
OmegaM = 0.307
OmegaDE = 1-OmegaM
cosmo = FlatLambdaCDM(H0=H0, Om0=OmegaM)

a_list = np.loadtxt(bolshoip_loc+'a_list.dat')[::2,2][1:]
z_list = np.loadtxt(bolshoip_loc+'a_list.dat')[::2,0][1:]



Kennicutt_halpha = 10**41.27 # KennicuttEvans2012

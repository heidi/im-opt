#!/usr/bin/env python
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from astropy.table import Table
from astropy.io import fits

import sys
sys.path.append('../')
import params as pa

area = (1.4/180.*np.pi)**2

# copied from halpha/
class LineCatalogBetherminSFR(object):
    def __init__(self):
        pass

    def write_halpha_catalog(self):
        t = Table.read(pa.data_master_loc+'data/Mock_cat_Bethermin2017.fits', hdu=2)
        #print(t.info)
        ra_list = t['RA'].data[0]
        dec_list = t['DEC'].data[0]
        z_list = t['REDSHIFT'].data[0]
        mh_list = t['MHALO'].data[0]
        sfr_list = t['SFR'].data[0]
        halpha_list = sfr_list * pa.Kennicutt_halpha
        #print(max(halpha_list), min(halpha_list))
        #print(max(sfr_list), min(sfr_list))
        for zmin in range(10):
            zmax = zmin + 1
            b = (z_list > zmin)&(z_list < zmax)&(sfr_list > 0)
            ra = ra_list[b]
            dec = dec_list[b]
            z = z_list[b]
            mh = mh_list[b]
            sfr = sfr_list[b]
            halpha = halpha_list[b]

            c0 = fits.Column(name='ra', format='D', array=ra)
            c1 = fits.Column(name='dec', format='D', array=dec)
            c2 = fits.Column(name='z', format='D', array=z)
            c3 = fits.Column(name='lgM', format='D', array=np.log10(mh))
            c4 = fits.Column(name='lgSFR', format='D', array=np.log10(sfr))
            c5 = fits.Column(name='lgLHa', format='D', array=np.log10(halpha))
            coldefs = fits.ColDefs([c0, c1, c2, c3, c4, c5])
            tbhdu = fits.BinTableHDU.from_columns(coldefs)
            tbhdu.writeto(pa.data_master_loc+'bethermin_sfr/halpha_z_%i_%i.fits'%(zmin, zmax), clobber=True)

    def halpha_LF(self):
        data = np.loadtxt(pa.bolshoip_loc+'cosmo_volume_time.dat')
        z = data[:,1]
        dVdOmegadz = data[:,5]
        dVdOmega = data[:,6]
        from scipy.interpolate import interp1d
        dVdOmegadz_interp = interp1d(z, dVdOmegadz)
        dVdOmega_interp = interp1d(z, dVdOmega)

        nlines = 12
        cmap = matplotlib.cm.get_cmap('afmhot')
        line_colors = cmap(np.linspace(0,1,nlines))
        plt.plot()
        plt.subplot(111,xscale='log',yscale='log')
        for zmin in range(10):
            co = line_colors[zmin]
            zmax = zmin + 1
            t = Table.read(pa.data_master_loc+'bethermin_sfr/halpha_z_%i_%i.fits'%(zmin, zmax))
            z = t['z']
            lgM = t['lgM']
            lgLHa = t['lgLHa']
            vol = dVdOmegadz_interp(zmin+0.5) * area
            # volume from lightcone code
            # sys.path.append('/home/hywu/work/CIB_opt/repo/model')
            # from sim import Sim
            # self.sim = Sim(name='BolshoiP')
            # sys.path.append('/panfs/ds08/hopkins/hywu/work/lightcone/repo/coordinate')
            # from params import Params
            # self.params = Params()
            # vol = (self.sim.VC_interp(zmax)-self.sim.VC_interp(zmin))*self.params.fsky

            #print(zmin, zmax, 'vol**(1/3)', vol**(1./3.))
            ##################################################
            # Luminosity Function
            print('len(lgLHa)=', len(lgLHa))
            Qlist = lgLHa
            nbins = 20
            lowest = 40
            highest = 45
            binsize = (highest-lowest)/(nbins*1.)
            x_list = np.zeros(nbins); y_list = np.zeros(nbins)
            #outfile = open(self.val_loc+'HMF_z_%g_%g.dat'%(zmin, zmax),'w')
            #outfile.write('#Mmin, Mmax, dn/dlnM \n')
            for jj in range(nbins):
                lower = lowest + binsize*jj
                upper = lowest + binsize*(jj+1)
                hh = len(Qlist[(Qlist>=lower)*(Qlist<upper)])
                #scatter((lowest+(jj+0.5)*binsize), hh)
                x_list[jj] = (lowest+(jj+0.5)*binsize)
                y_list[jj] = hh/binsize/vol
                #outfile.write('%.6e %.6e %.6e\n'%(exp(lower), exp(upper), hh/binsize/vol))
            #outfile.close() 
            
            plt.plot(10**x_list, y_list, c=co, label=r"$%g<z<%g$"%(zmin,zmax))
        plt.legend()
        plt.xlabel(r"$L_{H\alpha}\ [erg\ s^{-1}]$")
        plt.ylabel(r"$\Phi = dn/dlog_{10}L [dex^{-1} Mpc^{-3} ]$")
        plt.savefig('../../plots/bethermin_sfr/halpha_LF.pdf')
        plt.show()

    def rho_sfr(self):
        nz = 20
        dInu_dz_list = np.zeros(nz)
        dz = 0.5
        zmin_list = dz*np.arange(nz)
        outfile = open(pa.data_master_loc+'/bethermin_sfr/rho_SFR_z.dat','w')
        for iz in range(nz):
            zmin = zmin_list[iz]
            zmax = zmin + dz
            #print('z range', zmin, zmax)
            fname = pa.data_master_loc+'bethermin_sfr/halpha_z_%i_%i.fits'%(np.floor(zmin),np.floor(zmin)+1)
            data = fits.getdata(fname)
            #print(data)
            z = data['z']
            b = (z > zmin)*(z < zmax)
            z = z[b]
            sfr = 10**data['lgSFR'][b]

            zmid = 0.5*(zmin+zmax)
            chi = pa.cosmo.comoving_distance(zmid).value
            DH = (3.e+5)/pa.H0
            Ez = np.sqrt(pa.OmegaM*(1+zmid)**3 + pa.OmegaDE)
            vol = chi**2 * DH/Ez * dz * area
            rho_SFR = sum(sfr) / vol
            #print(rho_SFR)
            outfile.write('%g %g \n'%(zmid, rho_SFR))
        outfile.close()

        plt.plot()
        plt.subplot(111, yscale='log')
        data = np.loadtxt(pa.data_master_loc+'/bethermin_sfr/rho_SFR_z.dat')
        plt.plot(data[:,0], data[:,1], label='mock - bethermin sfr')

        #def plot_HopkinsBeacom():
        z = np.linspace(0,5,100)
        a = 0.0118
        b = 0.08
        c = 3.3
        d = 5.2
        sfrd = (a + b* z)/(1 + (z/c)**d)
        plt.plot(z, sfrd, label='Hopkins & Beacom')

        plt.xlabel(r"$z$")
        plt.ylabel(r"$\rm\rho_{SFR}$")
        plt.legend()
        #plt.title(r"$H_\alpha$")
        plt.xlim(0,5)
        plt.ylim(0.01, 1)
        plt.savefig('../../plots/bethermin_sfr/rho_SFR.pdf')
        plt.show()

if __name__ == "__main__":
    lcb = LineCatalogBetherminSFR()
    #lcb.write_halpha_catalog()
    #lcb.halpha_LF()
    lcb.rho_sfr()


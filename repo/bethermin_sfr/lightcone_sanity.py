#!/usr/bin/env python
# it is insane!

>>> from astropy.io import fits
>>> fname = 'Mock_cat_Bethermin2017.fits'
>>> hdulist = fits.open(fname)
>>> hdulist.info()
Filename: Mock_cat_Bethermin2017.fits
No.    Name         Type      Cards   Dimensions   Format
0    PRIMARY     PrimaryHDU       4   ()              
1                BinTableHDU     23   1R x 3C      [5584998D, 5584998D, 5584998D]   
2                BinTableHDU     73   1R x 28C     [1489628D, 1489628D, 1489628D, 1489628D, 1489628D, 1489628I, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E]   
3                BinTableHDU     73   1R x 28C     [1489628D, 1489628D, 1489628D, 1489628D, 1489628D, 1489628I, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E]   
4                BinTableHDU     73   1R x 28C     [1489628D, 1489628D, 1489628D, 1489628D, 1489628D, 1489628I, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E]   
5                BinTableHDU     73   1R x 28C     [1489628D, 1489628D, 1489628D, 1489628D, 1489628D, 1489628I, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E, 1489628E]   



'''
#import sys
import numpy as np
#import matplotlib
import matplotlib.pyplot as plt
from astropy.table import Table
#from astropy.io import fits

import sys
sys.path.append('../')
import params as pa

area = (1.4/180.*np.pi)**2

t = Table.read(pa.data_master_loc+'data/Mock_cat_Bethermin2017.fits', hdu=2)
#print(t.info)
ra_list = t['RA'].data[0]
dec_list = t['DEC'].data[0]
z_list = t['REDSHIFT'].data[0]
mh_list = t['MHALO'].data[0]
sfr_list = t['SFR'].data[0]
print('len(z_list)', len(z_list))


plt.hist(z_list)
plt.show()
'''
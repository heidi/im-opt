#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits
import sys
sys.path.append('../')
import params as pa

data_loc = pa.data_master_loc#'/panfs/ds08/hopkins/hywu/work/IM_opt/'
area = (1.4*np.pi/180.)**2


def total_intensity_from_map():
    fname = pa.data_master_loc+'cubes/map_z_5_6.fits'
    data = fits.getdata(fname)
    Inu = sum(data.flatten())
    intensity = Inu / area
    print(intensity) # NOT WORKING!!!!


# total_intensity_from_map()
# exit()


def total_intensity_from_catalog():
    data = np.loadtxt(pa.bolshoip_loc+'cosmo_volume_time.dat')
    z = data[:,1]
    dVdOmegadz = data[:,5]
    dVdOmega = data[:,6]
    from scipy.interpolate import interp1d
    dVdOmegadz_interp = interp1d(z, dVdOmegadz)
    dVdOmega_interp = interp1d(z, dVdOmega)


    nz = 10 #20
    S_z_list = np.zeros(nz)
    dz = 1
    zmin_list = dz*np.arange(nz)
    outfile = open(pa.data_master_loc+'halpha/S_z_2d.dat','w')
    for iz in range(nz):
        zmin = zmin_list[iz]
        zmax = zmin + dz
        print('z range', zmin, zmax)
        fname = pa.data_master_loc+'/output_catalog/Ha_z_%i_%i.fits'%(np.floor(zmin),np.floor(zmin)+1)
        data = fits.getdata(fname)
        #print(data)
        z = data['z']
        b = (z > zmin)*(z < zmax)
        z = z[b]
        lgLHa = data['lgLHa'][b]
        #from astropy.cosmology import FlatLambdaCDM
        #cosmo = FlatLambdaCDM(H0=67.8, Om0=0.307)
        chi = pa.cosmo.comoving_distance(z).value

        DH = 3000. / pa.h
        DA = chi/(1+z)
        DL = chi*(1+z)
        Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)

        lam_rest_AA = 6563.
        nu = (3.e+8)/(lam_rest_AA*1.e-10)
        dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
        S_Ha = 10**lgLHa / (4*np.pi*DL**2) * (DA**2) * dchi_dnu #erg s^-1 Mpc Hz^-1

        zmid = 0.5*(zmin+zmax)
        vol = dVdOmegadz_interp(zmid)
        zslice_vol = vol * area * dz

        S_z_list[iz] = sum(S_Ha) / zslice_vol * pa.Mpc2cm**-2 * pa.cgs2Jy
        print('z', zmin, zmax, 'dInudz', S_z_list[iz])
        outfile.write('%g %g \n'%(0.5*(zmin+zmax), S_z_list[iz]))
    outfile.close()

#exit()

# see ../pk_3d/halpha_S_z_3d.py
def plot_total_intensity_from_catalog():
    data = np.loadtxt(pa.data_master_loc+'halpha/S_z_2d.dat')
    z = data[:10,0]
    dInudz = data[:10,1]
    plt.figure()
    plt.subplot(111,yscale='log')
    plt.plot(z, dInudz, label='2d mock catalog')

    data = np.loadtxt('../data_Gong17/fig3a_dInudz_Ha_HopkinsBeacom.dat')
    plt.plot(data[:,0], data[:,1], label='Gong17, Hopkins & Beacom')
    plt.legend()
    plt.xlabel(r"$z$")
    plt.ylabel(r"$S_\nu\ [Jy\ sr^{-1}]$")
    plt.title(r"$H\alpha$")
    plt.savefig('../../plots/S_z_Halpha.pdf')
    plt.show()


if __name__ == "__main__":
    #total_intensity_from_catalog()
    plot_total_intensity_from_catalog()

#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits

import sys
sys.path.append('../')
import params as pa

#data_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'
area = (1.4*np.pi/180.)**2


# from astropy.cosmology import FlatLambdaCDM
# H0 = 67.8
# OmegaM = 0.307
# OmegaDE = 1-OmegaM
# cosmo = FlatLambdaCDM(H0=H0, Om0=OmegaM)

def rho_SFR_from_catalog():
    nz = 20
    dInu_dz_list = np.zeros(nz)
    dz = 0.5
    zmin_list = dz*np.arange(nz)
    outfile = open(pa.data_master_loc+'halpha/rho_SFR_z.dat','w')
    for iz in range(nz):
        zmin = zmin_list[iz]
        zmax = zmin + dz
        print('z range', zmin, zmax)
        fname = pa.data_master_loc+'/output_catalog/Ha_z_%i_%i.fits'%(np.floor(zmin),np.floor(zmin)+1)
        data = fits.getdata(fname)
        #print(data)
        z = data['z']
        b = (z > zmin)*(z < zmax)
        z = z[b]
        sfr = 10**data['lgSFR'][b]

        zmid = 0.5*(zmin+zmax)
        chi = pa.cosmo.comoving_distance(zmid).value
        DH = (3.e+5)/pa.H0
        Ez = np.sqrt(pa.OmegaM*(1+zmid)**3 + pa.OmegaDE)
        vol = chi**2 * DH/Ez * dz * area
        rho_SFR = sum(sfr) / vol
        print(rho_SFR)
        outfile.write('%g %g \n'%(zmid, rho_SFR))
    outfile.close()




def plot_HopkinsBeacom():
    z = np.linspace(0,10,100)
    a = 0.0118
    b = 0.08
    c = 3.3
    d = 5.2
    sfrd = (a + b* z)/(1 + (z/c)**d)
    plt.plot(z, sfrd, label='Hopkins & Beacom')

def plot_rho_sfr():
    data = np.loadtxt(pa.data_master_loc+'halpha/rho_SFR_z.dat')
    plt.subplot(111,yscale='log')
    plt.plot(data[:,0], data[:,1], label='mock')
    #plot_MadauDickinson()
    plot_HopkinsBeacom()
    plt.xlabel(r"$z$")
    plt.ylabel(r"$\rm\rho_{SFR} [M_\odot/yr/Mpc^3]$")
    plt.legend()
    #plt.title(r"$H_\alpha$")
    #plt.xlim(0,5)
    #plt.ylim(0.01, 1)
    plt.savefig('../../plots/rho_SFR.pdf')


if __name__ == "__main__":
    #rho_SFR_from_catalog()
    plot_rho_sfr()
    plt.show()





'''
def plot_MadauDickinson():
    data_loc = '/home/hywu/work/data/MadauDickinson14/'
    # IR!
    data = np.loadtxt(data_loc+'Table1_IR.dat')
    ## zmin, zmax, nouse, err_up, mean lg10 rho_SFR, erro_down
    zmin = data[:,0]
    zmax = data[:,1]
    zmid = 0.5*(zmin+zmax)
    dz = 0.5*(zmax-zmin)
    lgrho = data[:,4]
    dlgrho_lo = abs(data[:,5]) # lower
    dlgrho_hi = data[:,3]
    (_, caps, _) = plt.errorbar(zmid, lgrho, xerr=dz, yerr=[dlgrho_lo, dlgrho_hi], label=r"$\rm Madau\ & \ Dickinson\ 14\  IR$",c='r',marker='o',mec='r',ls='', elinewidth=2)
    for cap in caps:
        cap.set_linewidth(2)
        cap.set_markeredgewidth(2)
    # UV !!!!
    data = np.loadtxt(data_loc+'Table1_UV.dat')
    ## zmin, zmax, nouse, err_up, mean lg10 rho_SFR, erro_down
    zmin = data[:,0]
    zmax = data[:,1]
    zmid = 0.5*(zmin+zmax)
    dz = 0.5*(zmax-zmin)
    lgrho = data[:,4]
    dlgrho_lo = abs(data[:,5]) # lower
    dlgrho_hi = data[:,3]
    (_, caps, _) = plt.errorbar(zmid, lgrho, xerr=dz, yerr=[dlgrho_lo, dlgrho_hi], label=r"$\rm Madau\ & \ Dickinson\ 14\  UV$",c='g',marker='s',mec='g',ls='', elinewidth=2)
    for cap in caps:
        cap.set_linewidth(2)
        cap.set_markeredgewidth(2)
'''

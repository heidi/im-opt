#!/usr/bin/env python
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from astropy.table import Table
from astropy.io import fits

# NOTE! superceeded by ../lines_for_spherex/line_catalogs.py


data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

class LineCatalogs(object):
    def __init__(self, zmin=6):
        self.zmin = zmin
        self.zmax = zmin + 1

    def interp_Ha(self):
        data = np.loadtxt(data_master_loc+'halpha/Lalpha_z_lgM.dat')
        z_list = np.unique(data[:,0])
        lgM_list = np.unique(data[:,1])
        nz = len(z_list)
        nlgM = len(lgM_list)
        lgLHa_list = np.log10(data[:,2]).reshape(nz,nlgM)
        from scipy.interpolate import RectBivariateSpline
        self.lgLa_interp = RectBivariateSpline(z_list, lgM_list, lgLHa_list)

    def interp_SFR(self):
        data = np.loadtxt(data_master_loc+'halpha/SFR_z_lgM.dat')
        z_list = np.unique(data[:,0])
        lgM_list = np.unique(data[:,1])
        nz = len(z_list)
        nlgM = len(lgM_list)
        lgSFR_list = data[:,2].reshape(nz,nlgM)
        from scipy.interpolate import RectBivariateSpline
        self.lgSFR_interp = RectBivariateSpline(z_list, lgM_list, lgSFR_list)

    def write_Ha_cat(self):
        self.interp_Ha()
        self.interp_SFR()
        # t = Table.read(data_master_loc+'output_catalog/spectra_z_%i_%i.fits'%(self.zmin, self.zmax))
        # #ra, dec, z, mh, spectra [1000] 
        # ra = t['ra']
        # dec = t['dec']
        # z = t['z']
        # lgM = np.log10(t['mh'])
        data = np.loadtxt('/panfs/ds08/hopkins/hywu/work/lightcone/Bethermin_v4_AbMatch/lightcone/BolshoiP_Bethermin_Mstar_z_%i_%i.dat'%(self.zmin,self.zmax))
        #ra[0], dec[1], z[2], M200[3,Msun], Mstar[4,Msun]
        ra = data[:,0]
        dec = data[:,1]
        z = data[:,2]
        lgM = np.log10(data[:,3]) # WHICH MASS????
        nh = len(z)
        lgSFR_list = np.zeros(nh)
        lgLHa_list = np.zeros(nh)
        for i in range(nh):
            lgSFR_list[i] = self.lgSFR_interp(z[i], lgM[i])
            lgLHa_list[i] = self.lgLa_interp(z[i], lgM[i])
        #print(lgLHa_list)
        c0 = fits.Column(name='ra', format='D', array=ra)
        c1 = fits.Column(name='dec', format='D', array=dec)
        c2 = fits.Column(name='z', format='D', array=z)
        c3 = fits.Column(name='lgM', format='D', array=lgM)
        c4 = fits.Column(name='lgSFR', format='D', array=lgSFR_list)
        c5 = fits.Column(name='lgLHa', format='D', array=lgLHa_list)
        coldefs = fits.ColDefs([c0, c1, c2, c3, c4, c5])
        tbhdu = fits.BinTableHDU.from_columns(coldefs)
        tbhdu.writeto(data_master_loc+'output_catalog/Ha_z_%i_%i.fits'%(self.zmin, self.zmax), clobber=True)

    def plot_Kennicutt(self):
        t = Table.read(data_master_loc+'output_catalog/Ha_z_%i_%i.fits'%(self.zmin, self.zmax))
        lgSFR = t['lgSFR']
        lgLHa = t['lgLHa']
        plt.scatter(lgLHa, lgSFR)
        K_LHa = 10**41.27 # KennicuttEvans2012
        x = np.arange(36,44)
        plt.plot(x, x-np.log10(K_LHa), label=r"$\rm Kennicutt$")
        plt.xlabel(r"$log_{10} SFR [M_\odot\ yr^{-1}]$")
        plt.ylabel(r"$log_{10} L_{H_\alpha} [erg\ s^{-1}]$")
        plt.legend()
        #plt.show()

    def plot_Ha_LM(self, co='b'):
        t = Table.read(data_master_loc+'output_catalog/Ha_z_%i_%i.fits'%(self.zmin, self.zmax))
        z = t['z']
        lgM = t['lgM']
        lgLHa = t['lgLHa']

        Xlist = lgM
        Ylist = lgLHa
        nbins = 20
        lowest = 9
        highest = 15
        binsize = (highest-lowest)/(nbins*1.)
        x_list = np.zeros(nbins)
        y_list = np.zeros(nbins)
        dy_list = np.zeros(nbins)
        #outfile = open(self.data_loc+'validation/SMHM_z_%g_%g.dat'%(zmin,zmax),'w')
        #outfile.write('#lgM200_min, lgM200_max, mean(lgMstar), median(lgMstar), std(lgMstar) \n')
        for jj in np.arange(nbins):
            lower = lowest + binsize*jj
            upper = lowest + binsize*(jj+1)
            bb = (Xlist > lower)*(Xlist < upper)
            x_list[jj] = (lowest+(jj+0.5)*binsize)
            if len(Ylist[bb]) > 0:
                y_list[jj] = np.mean(Ylist[bb])
                dy_list[jj] = np.std(Ylist[bb])
        b = (y_list > 0)
        plt.errorbar(x_list[b], y_list[b], dy_list[b], c=co, label=r"$%g<z<%g$"%(self.zmin,self.zmax))
        plt.legend()
        plt.xlabel(r"$M_{halo}\ [M_\odot]$")
        plt.ylabel(r"$L_{H\alpha}\ [erg\ s^{-1}]$")


    def plot_Ha_LF(self, co='b'):
        t = Table.read(data_master_loc+'output_catalog/Ha_z_%i_%i.fits'%(self.zmin, self.zmax))
        z = t['z']
        lgM = t['lgM']
        lgLHa = t['lgLHa']

        # volume from lightcone code
        sys.path.append('/home/hywu/work/CIB_opt/repo/model')
        from sim import Sim
        self.sim = Sim(name='BolshoiP')
        sys.path.append('/panfs/ds08/hopkins/hywu/work/lightcone/repo/coordinate')
        from params import Params
        self.params = Params()
        vol = (self.sim.VC_interp(self.zmax)-self.sim.VC_interp(self.zmin))*self.params.fsky
        #print(self.zmin, self.zmax, 'vol**(1/3)', vol**(1./3.))
        ##################################################
        # Luminosity Function
        print('len(lgLHa)=', len(lgLHa))
        Qlist = lgLHa
        nbins = 20
        lowest = 40
        highest = 45
        binsize = (highest-lowest)/(nbins*1.)
        x_list = np.zeros(nbins); y_list = np.zeros(nbins)
        #outfile = open(self.val_loc+'HMF_z_%g_%g.dat'%(zmin, zmax),'w')
        #outfile.write('#Mmin, Mmax, dn/dlnM \n')
        for jj in range(nbins):
            lower = lowest + binsize*jj
            upper = lowest + binsize*(jj+1)
            hh = len(Qlist[(Qlist>=lower)*(Qlist<upper)])
            #scatter((lowest+(jj+0.5)*binsize), hh)
            x_list[jj] = (lowest+(jj+0.5)*binsize)
            y_list[jj] = hh/binsize/vol
            #outfile.write('%.6e %.6e %.6e\n'%(exp(lower), exp(upper), hh/binsize/vol))
        #outfile.close() 
        plt.subplot(111,xscale='log',yscale='log')
        plt.plot(10**x_list, y_list, c=co, label=r"$%g<z<%g$"%(self.zmin,self.zmax))
        plt.legend()
        plt.xlabel(r"$L_{H\alpha}\ [erg\ s^{-1}]$")
        plt.ylabel(r"$\Phi = dn/dlog_{10}L [dex^{-1} Mpc^{-3} ]$")
        



if __name__ == "__main__":

    
    ## write catalogs
    for zmin in range(10):
         wlcf = LineCatalogs(zmin=zmin)
         wlcf.write_Ha_cat()
    
    exit()
    '''
    nlines = 12
    cmap = matplotlib.cm.get_cmap('afmhot')
    line_colors = cmap(np.linspace(0,1,nlines))

    
    # plotting LHa - SFR
    plt.figure()
    for zmin in [3]:#range(10):#range(1,8):
        wlcf = LineCatalogs(zmin=zmin)
        wlcf.plot_Kennicutt()#co=line_colors[zmin])
    plt.show()
    #plt.savefig('../../plots/Ha_LM.pdf')
    

    
    # plotting LM
    plt.figure()
    for zmin in range(10):#range(1,8):
        wlcf = LineCatalogs(zmin=zmin)
        wlcf.plot_Ha_LM(co=line_colors[zmin])
    plt.savefig('../../plots/Ha_LM.pdf')

    # plotting LF
    plt.figure()
    for zmin in range(1,8):
        wlcf = LineCatalogs(zmin=zmin)
        wlcf.plot_Ha_LF(co=line_colors[zmin])
    plt.savefig('../../plots/Ha_LF.pdf')
    plt.show()
    '''

exit()

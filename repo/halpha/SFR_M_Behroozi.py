#!/usr/bin/env python
#import sys
import numpy as np
#import matplotlib
import matplotlib.pyplot as plt
#from astropy.table import Table
#from astropy.io import fits

data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'
data_loc = data_master_loc+'data/Behroozi_SFH_v2/'
a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')

class SFR_M_Behroozi(object):
    def __init__(self):
        pass

    def get_lgSFR(self, lgM, z):
        a = 1./(1.+z)
        b0 = (abs(m_list - lgM) == min(abs(m_list - lgM)))
        m_present = m_list[b0][0]
        b0 = (abs(a_list - a) == min(abs(a_list - a)))
        a_present = a_list[b0][0]
        data = np.loadtxt(data_loc+'sfh_a%.6f.dat'%(a_present))
        b = (data[:,1] == m_present)
        lgSFR = data[b,2][0]
        return lgSFR

    def plot_SFR_M(self, z_wanted = 1):
        a_wanted = 1./(1+z_wanted)
        b = (abs(a_list-a_wanted) == min(abs(a_list-a_wanted)))
        a_present = a_list[b]
        print('a_presnet=', a_present)

        data = np.loadtxt(data_loc+'sfh_a%.6f.dat'%(a_present))
        ng = len(m_list)
        lgSFR_list = np.zeros(ng)
        for i in range(ng):
            m_present = m_list[i]
            b = (data[:,1] == m_present)
            lgSFR_list[i] = data[b,2][0] # the first SFR is the present SFR
        plt.plot(m_list, lgSFR_list, label=r"$z=%g$"%(z_wanted))
        plt.legend()


    def save_SFR_z_lgM(self):
        outfile = open(data_master_loc+'lines/SFR_z_lgM.dat','w')
        outfile.write('#z, lgM[Msun], lgSFR[Msun/yr] \n')
        for a_present in a_list:
            z = 1./a_present-1
            data = np.loadtxt(data_loc+'sfh_a%.6f.dat'%(a_present))
            for m_present in m_list:
                b = (data[:,1] == m_present)
                SFR = data[b,2][0]
                outfile.write('%g %g %g\n'%(z, m_present, SFR))
        outfile.close()



if __name__ == "__main__":
    sfrm = SFR_M_Behroozi()
    print(sfrm.get_lgSFR(lgM=12, z=2))

    #sfrm = SFR_M_Behroozi()
    #sfrm.save_SFR_z_lgM()
    
    # for z in np.arange(0.5,9.5,1):
    #     sfrm.plot_SFR_M(z_wanted=z)
    # plt.show()
    
    
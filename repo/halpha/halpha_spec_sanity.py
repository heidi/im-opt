#!/usr/bin/env python
#import cygrid as cg
import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits

zmin = 7

zmax = zmin + 1
data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'

fname = data_master_loc+'output_catalog/spectra_z_%i_%i.fits'%(zmin,zmax)

d = fits.getdata(fname)
nrow, nw = np.shape(d['spectra'])

Ha_lam = 6563
min_lam = 6563*(1+zmin)
max_lam = 6563*(1+zmax)
nw = 1000
wave = np.linspace(min_lam, max_lam, nw)
#zmin = 6
#zmax = 6.5
plt.subplot(111, yscale='log')
#wave = np.linspace(5e+3, 5.e+4, 5000)
for i in range(0, nrow, int(nrow/10)):
    spec = d['spectra'][i,:]

    #print(np.shape(spec))
    #b = (wave > 4e+4)
    plt.plot(wave, spec)

y = np.linspace(0,0.0002,10)
plt.plot(y*0+min_lam, y, ls='--', c='gray')
plt.plot(y*0+max_lam, y, ls='--', c='gray')
plt.title(r"$H_\alpha,\ %g < z< %g$"%(zmin,zmax))
plt.xlabel(r"$\lambda [\AA]$")
plt.ylabel(r"$L_\lambda [L_\odot/Hz]$")
plt.savefig('../../plots/Halpha_spec_sanity.pdf')
plt.show()
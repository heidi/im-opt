#!/usr/bin/env python
import cygrid as cg
import numpy as np

from astropy.io import fits

PROJECTPATH = '/Users/hao-yiwu/work_zwicky/IM_opt_local/'

lam_Ha = 6563
nlam = 1000

side_deg = 1.4


'''
# calculate ngrid based on the resolution I want
beam_deg = 20./3600. #deg # want: 6"
pixel_deg = beam_deg/2./np.sqrt(2)
ngrid = int(np.ceil(side_deg/pixel_deg))
print('ngrid=', ngrid)
'''
ngrid = 256
beam_deg = 20./3600. #TODO?

def create_header(zmin, zmax):
    lam_min = lam_Ha * (1+zmin)
    lam_max = lam_Ha * (1+zmax)
    
    header = fits.Header()

    header['BUNIT'] = 'Jy/beam'

    header['NAXIS'] = 3

    header['NAXIS1'] = ngrid
    header['NAXIS2'] = ngrid
    header['NAXIS3'] = nlam#has to match data

    header['CDELT1'] = side_deg / (ngrid*1.)
    header['CDELT2'] = side_deg / (ngrid*1.)
    header['CDELT3'] = (lam_max - lam_min)/(1.*nlam)#has to match data

    header['CRPIX1'] = 0
    header['CRPIX2'] = 0
    header['CRPIX3'] = 0

    header['CUNIT1'] = 'deg'
    header['CUNIT2'] = 'deg'
    header['CUNIT3'] = 'Angstrom'

    header['CRVAL1'] = 0
    header['CRVAL2'] = 0
    header['CRVAL3'] = lam_min#40000 # starting from 4mm
    header['LATPOLE'] = 90.

    header['CTYPE1'] = 'RA---SFL'
    header['CTYPE2'] = 'DEC--SFL'
    header['CTYPE3'] = 'WAVE-F2W'

    #header['BMAJ'] = 3. * 1.4 / 256  # need to match kernelsize_fwhm
    #header['BMIN'] = 1.

    return header


def grid_catalogue(header, ra, dec, spectra):
    # set up the gridder and kernel
    gridder = cg.WcsGrid(header)
    # set kernel
    kernelsize_fwhm = beam_deg #6 arcsec #3. * 1.4 / 256 # in degree
    kernelsize_sigma = kernelsize_fwhm / 2.355  # can leave this line unchanged
    sphere_radius = 3. * kernelsize_sigma  # can leave this line unchanged
    gridder.set_kernel(
        'gauss1d',
        (kernelsize_sigma,),
        sphere_radius,
        kernelsize_sigma / 2.)

    # grid
    gridder.grid(ra, dec, spectra)

    datacube = gridder.get_datacube()

    return datacube


def main():
    zmin_list = [5]#range(10)
    for zmin in zmin_list:
        zmax = zmin + 1
        INPATH = PROJECTPATH + 'output/catalog/spectra_z_%i_%i.fits'%(zmin, zmax)
        OUTPATH = PROJECTPATH + 'output/cubes/cubes_z_%i_%i.fits'%(zmin, zmax)
        d = fits.getdata(INPATH)
        header = create_header(zmin, zmax)

        z = d['z']
        ra = d['ra']
        dec = d['dec']
        spec = d['spectra']
        print(np.shape(z))
        print(np.shape(spec))
        #print(np.shape(spec))
        #cube = grid_catalogue(header, d['ra'], d['dec'], d['spectra'])
        cube = grid_catalogue(header, ra, dec, spec)
        fits.writeto(OUTPATH, cube, header, clobber=True)


if __name__ == '__main__':
    main()

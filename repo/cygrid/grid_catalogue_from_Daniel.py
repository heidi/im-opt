import cygrid as cg
import numpy as np

from astropy.io import fits

PROJECTPATH = '/users/dlenz/projects/im-sims/'

INPATH = PROJECTPATH + 'data/catalogues/spectra_z_6_7.fits'
OUTPATH = PROJECTPATH + 'data/cubes/firstcube.fits'


def create_header():

    header = fits.Header()
    
    header['BUNIT'] = 'Jy/beam'

    header['NAXIS'] = 3

    header['NAXIS1'] = 256
    header['NAXIS2'] = 256
    header['NAXIS3'] = 100

    header['CDELT1'] = 1.4 / 256
    header['CDELT2'] = 1.4 / 256
    header['CDELT3'] = 0.01

    header['CRPIX1'] = 0
    header['CRPIX2'] = 0
    header['CRPIX3'] = 0

    header['CUNIT1'] = 'deg'
    header['CUNIT2'] = 'deg'
    header['CUNIT3'] = 'redshift'

    header['CRVAL1'] = 0
    header['CRVAL2'] = 0
    header['CRVAL3'] = 6
    header['LATPOLE'] = 90.

    header['CTYPE1'] = 'RA---SFL'
    header['CTYPE2'] = 'DEC--SFL'
    header['CTYPE3'] = 'REDSHIFT'

    return header


def grid_catalogue(header, ra, dec, spectra):
    # set up the gridder and kernel
    gridder = cg.WcsGrid(header)

    # set kernel in degrees
    kernelsize_fwhm = 3. * 1.4 / 256
    kernelsize_sigma = kernelsize_fwhm / 2.355  # can leave this line unchanged
    sphere_radius = 3. * kernelsize_sigma  # can leave this line unchanged

    gridder.set_kernel(
        'gauss1d',
        (kernelsize_sigma,),
        sphere_radius,
        kernelsize_sigma / 2.)

    # grid
    gridder.grid(ra, dec, spectra)

    datacube = gridder.get_datacube()

    return datacube


def main():
    d = fits.getdata(INPATH)

    header = create_header()

    cube = grid_catalogue(header, d['ra'], d['dec'], d['spectra'])

    fits.writeto(OUTPATH, cube, header, clobber=True)


if __name__ == '__main__':
    main()

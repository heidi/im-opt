#!/usr/bin/env python
import cygrid as cg
import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits
import healpy as hp


import catalog_to_grid as c2g
PROJECTPATH = c2g.PROJECTPATH
ngrid = c2g.ngrid
nlam = c2g.nlam

def cube_to_map(zmin,zmax):
    OUTPATH = PROJECTPATH + 'output/cubes/map_z_%i_%i.fits'%(zmin, zmax)
    INPATH = PROJECTPATH + 'output/cubes/cubes_z_%i_%i.fits'%(zmin, zmax)
    
    d = fits.getdata(INPATH)
    d = np.nan_to_num(d)
    print('np.shape(d)',np.shape(d))
    #nlam = np.shape(d)[0]
    #ngrid = np.shape(d)[1]
    map_output = np.zeros([ngrid, ngrid])
    for i in range(nlam):
        map_output += d[i]

    header = c2g.create_header(zmin, zmax)
    #cube = grid_catalogue(header, d['ra'], d['dec'], d['spectra'])
    fits.writeto(OUTPATH, map_output, header, clobber=True)
    plt.imshow(np.log10(map_output))
    plt.show()


def map_to_healpy(zmin,zmax):
    OUTPATH = PROJECTPATH + 'repo/cl/data/healpy_z_%i_%i.fits'%(zmin, zmax)
    MASKPATH = PROJECTPATH + 'repo/cl/data/mask.fits'

    INPATH = PROJECTPATH + 'output/cubes/map_z_%i_%i.fits'%(zmin, zmax)
    d = fits.getdata(INPATH)
    #print(np.shape(d))
    #ngrid = np.shape(d)
    grid_rad = (c2g.side_deg/ngrid)*np.pi/180.

    nside = 2048
    npix = hp.nside2npix(nside)
    intensity_healpy = np.zeros(npix)
    mask_healpy = np.zeros(npix)

    ra = 0.5*np.pi+np.arange(ngrid)*grid_rad
    dec = np.arange(ngrid)*grid_rad

    for i_ra in range(ngrid):
        for i_dec in range(ngrid):
            ipix = hp.ang2pix(nside, ra[i_ra], dec[i_dec])
            #print(ipix, d[i_ra, i_dec])
            intensity_healpy[ipix] += d[i_ra, i_dec]
            mask_healpy[ipix] = 1
    #fits.writeto(OUTPATH, intensity_healpy, clobber=True)
    #fits.writeto(MASKPATH, mask_healpy, clobber=True)
    hp.write_map(OUTPATH, intensity_healpy)
    hp.write_map(MASKPATH, mask_healpy)
    hp.mollview(intensity_healpy)
    plt.show()
    
def check_map(zmin,zmax):
    INPATH = PROJECTPATH + 'output/cubes/healpy_z_%i_%i.fits'%(zmin, zmax)
    d = fits.getdata(INPATH)
    print(np.shape(d))
    print(np.shape(d[d>0]))
    plt.hist(d[d>0], range=(0.,0.01))
    plt.show()

if __name__ == '__main__':
    cube_to_map(zmin=5,zmax=6)
    #map_to_healpy(zmin=5,zmax=6)
    #check_map(zmin=5,zmax=6)
#!/usr/bin/env python
import cygrid as cg
import numpy as np

from astropy.io import fits

PROJECTPATH = '/Users/hao-yiwu/work_zwicky/IM_opt/'



from catalog_to_grid import create_header

def main():
    OUTPATH = PROJECTPATH + 'output/cubes/cubes_all_z.fits'
    zmin_list = range(10)

    for zmin in zmin_list:
        zmax = zmin + 1
        INPATH = PROJECTPATH + 'output/cubes/cubes_z_%i_%i.fits'%(zmin, zmax)
        
        d = fits.getdata(INPATH)
        d = np.nan_to_num(d)
        #print(d)

        if zmin == min(zmin_list):
            d_master = d
        else:
            d_master += d
        
        header = create_header()

        #cube = grid_catalogue(header, d['ra'], d['dec'], d['spectra'])
        fits.writeto(OUTPATH, d_master, header, clobber=True)


if __name__ == '__main__':
    main()

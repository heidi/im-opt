#!/usr/bin/env python
#import cygrid as cg
import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits

PROJECTPATH = '/Users/hao-yiwu/work_zwicky/IM_opt/'

zmin = 5
zmax = zmin + 1
#INPATH = PROJECTPATH + 'output/catalog/spectra_z_5_6.fits'
OUTPATH = PROJECTPATH + 'output/cubes/cubes_z_%i_%i.fits'%(zmin, zmax)

from astropy.io import fits
hdulist = fits.open(OUTPATH)


# overview of HDU's
hdulist.info()

# primaryHDU
hdulist[0].header
datacube = hdulist[0].data
print(np.shape(datacube))

for i in [0]:#range(100):
    plt.figure()
    plt.imshow(np.log(datacube[i,:,:]))
    plt.colorbar()
    plt.savefig(PROJECTPATH +'output/cubes/images/%i.png'%(i))

plt.show()
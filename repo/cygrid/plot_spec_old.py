#!/usr/bin/env python
#import cygrid as cg
import numpy as np
import matplotlib.pyplot as plt

from astropy.io import fits

zmin = 6
zmax = 7
PROJECTPATH = '/Users/hao-yiwu/work_zwicky/IM_opt/'

INPATH = PROJECTPATH + 'output/catalog/spectra_z_%i_%i.fits'%(zmin, zmax)
OUTPATH = PROJECTPATH + 'output/cubes/cubes_z_%i_%i.fits'%(zmin, zmax)
d = fits.getdata(INPATH)
#header = create_header()
print(np.shape(d['spectra']))


zmin = 6
zmax = 6.5
plt.subplot(111, yscale='log')
wave = np.linspace(5e+3, 5.e+4, 5000)
for i in range(0,21685,1000):
    spec = d['spectra'][i,:]

    #print(np.shape(spec))
    b = (wave > 4e+4)
    plt.plot(wave[b], spec[b])

lam_Ha = 6563
y = np.linspace(0,0.0002,10)
plt.plot(y*0+lam_Ha*(1+zmin), y, ls='--', c='gray')
plt.plot(y*0+lam_Ha*(1+zmax), y, ls='--', c='gray')
plt.title(r"$H_\alpha,\ %g < z< %g$"%(zmin,zmax))
plt.savefig('../../plots')
plt.show()
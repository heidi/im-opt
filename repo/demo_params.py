#!/usr/bin/env python
#import numpy as np
#import matplotlib.pyplot as plt

# there is a "params.py" in every repo.  here is how I call them

#import sys
#sys.path.append('/home/hywu/work/') #put this path in ~.bashrc
import IM_opt.repo.params as pa
print(pa.h)

import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)

from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')
print('sim.OmegaM', sim.OmegaM)
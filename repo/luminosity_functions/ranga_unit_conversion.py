#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa

# sigh.. why should I be doing this...
Lsun2cgs = 3.839e+33

for z in [5, 7, 10]:
    data = np.loadtxt(pa.data_loc+'luminosity_functions/ranga/cdim_preds_z%i.txt'%(z), skiprows=4)

    # HIGH 
    L_Lsun = data[:,0]
    L = L_Lsun*Lsun2cgs
    dNdL = data[:,2]
    dNdlgL = np.log(10) * L_Lsun * dNdL

    outfile = open(pa.data_loc+'luminosity_functions/ranga/LF_Ha_cgs_z%i_high.out'%(z), 'w')
    outfile.write('# converting to cgs by Heidi \n')
    outfile.write('# erg/s, 1/Mpc^3/dex \n')
    for i, L in enumerate(L):
        outfile.write('%e \t %g \n'%(L, dNdlgL[i]))
    outfile.close()

    # LOW
    L_Lsun = data[:,3]
    L = L_Lsun*Lsun2cgs
    dNdL = data[:,5]
    dNdlgL = np.log(10) * L_Lsun * dNdL

    outfile = open(pa.data_loc+'luminosity_functions/ranga/LF_Ha_cgs_z%i_low.out'%(z), 'w')
    outfile.write('# converting to cgs by Heidi \n')
    outfile.write('# erg/s, 1/Mpc^3/dex \n')
    for i, L in enumerate(L):
        outfile.write('%e \t %g \n'%(L, dNdlgL[i]))
    outfile.close()


# plt.plot(L, dNdlgL)
# plt.xscale('log')
# plt.yscale('log')
# plt.xlim(1e39, 1e44)
# plt.ylim(1e-5, 10)

# plt.show()

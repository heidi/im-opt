#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa

data = np.loadtxt(pa.data_loc+'luminosity_functions/finkelstein/finkelstein_half.txt')
L = 10**data[:,0]

for z in [5, 7, 10]:
    #data = np.loadtxt(pa.data_loc+'luminosity_functions/ranga/cdim_preds_z%i.txt'%(z), skiprows=4)
    if z == 5: Phi = data[:,2]
    if z == 7: Phi = data[:,4]
    if z == 10: Phi = data[:,7]

    outfile = open(pa.data_loc+'luminosity_functions/finkelstein/LF_Ha_z%i.out'%(z), 'w')
    outfile.write('# erg/s, 1/Mpc^3/dex \n')
    for i in range(len(L)):
        outfile.write('%e \t %g \n'%(L[i], Phi[i]))
    outfile.close()

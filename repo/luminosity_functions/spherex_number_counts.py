#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa 

from scipy.interpolate import interp1d
from astropy import units as u
from astropy.cosmology import Planck15 as cosmo
co_list = ['b','g','r','c','m','y','k']
lam_rest = 0.65628 # um!
plt_loc = '../../plots/luminosity_functions/'

# note! with time dependence
# import sys
# sys.path.append('CDIM_APROBE/')
# from cdim_sbsens_fn_heidi import calculate_cdim_sbsems

#out_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/luminosity_functions/sensitivity_zemcov/'
#calculate_cdim_sbsems(Apert=1.0, R=500, t_int = 200, out_loc=out_loc)

def plot_sensitivity():
    # calculated from spherex_sensitivity.py
    for name in ['deep']:#, 'allsky']:
        lam, flux_th = np.loadtxt('spherex/line_sensitivity_%s.dat'%(name), unpack=True)
        plt.plot(lam, flux_th, c='k')#, ls='--', label='latest '+name)

    '''
    A = 0.83
    R = 300
    for t in [768, 852, 6240, 6780, 7500]:
        data = np.loadtxt(pa.data_loc+'luminosity_functions/sensitivity_zemcov/cdim_sbsens_out_A%g_R%i_t%i.txt'%(A, R, t), delimiter=',')
        lam = data[:,0]
        flux_th = data[:,3]
        b = (lam < 10)
        plt.plot(lam[b], flux_th[b], label='old %g min'%(t/60.))

    plt.xlabel(r'$\lambda [\mu m]$')
    plt.ylabel(r'1 $\sigma$ Line Sensitivity'+'\n'+r'$\rm [10^{-18}erg/s/cm^2]$')
    plt.legend()
    plt.savefig('../../plots/luminosity_functions/sensitivity_old_new.pdf')
    '''
    plt.show()


#plot_sensitivity()
#exit()

lam_min_list = [0.75, 1.11, 1.64, 2.42, 3.68, 4.45 ]
lam_max_list = [1.11, 1.64, 2.42, 3.68, 4.45, 5.01 ]


def calc_nobj(iband=2, name='deep'):
    plt.figure(0, figsize=(21,14))
    plt.subplot(2,3,iband+1)
    lam_min = lam_min_list[iband]
    lam_max = lam_max_list[iband]
    zlo = lam_min/lam_rest - 1 
    zhi = lam_max/lam_rest - 1 
    zmid = 0.5*(zlo+zhi)
    print('band', iband+1,'z range', zlo, zhi, 'zmid', zmid)
    #exit()
    if name=='deep': survey_area_sq_deg=200.; c='b'
    if name=='allsky': survey_area_sq_deg=30000.; c='g'
    #data = np.loadtxt('20171215/cdim_sbsens_out_R300_%s.txt'%(name), delimiter=',')
    #lam = data[:,0]
    #flux_th = data[:,3]
    lam, flux_th = np.loadtxt('spherex/line_sensitivity_%s.dat'%(name), unpack=True)
    flux_th_interp = interp1d(lam, flux_th)
    lam_obs = lam_rest * (1+zmid)
    #print('flux th at ' , lam_obs, 'um is', flux_th_interp(lam_obs))
    flux_th = flux_th_interp(lam_obs) * (1.e-18)


    chi = cosmo.comoving_distance(zmid).value
    Mpc2cm = 3.086e+24
    d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

    lam_obs = lam_rest * (1+zmid)

    fsky = survey_area_sq_deg/41253.
    dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky
    #print('cosmo.comoving_volume(zhi)', cosmo.comoving_volume(zhi))
    #print('dV', dV)
    #ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
    #ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)


    #for fname in [ranga_lo, ranga_hi]:
    zmid = int(zmid)
    fname = 'heidi/halpha_z_%i.dat'%(zmid)
    data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
    L = data[:,0]
    flux = L / (4*np.pi*d_L*d_L)
    n = data[:,1]
    b1 = (flux > flux_th)
    n_obj = np.trapz(n[b1], x = np.log10(flux[b1])) * dV

    b = (n > 1.e-6)
    plt.plot(flux[b], n[b], c='k')#, c=c, ls=ls)
    plt.axvline(x=flux_th, c=c, label=name+r', A=%i $\rm deg^2$, N = %.2e'%(survey_area_sq_deg, n_obj))

    #plt.savefig(plt_loc+'')
    
    '''
    # other peoples' model
    if name == 'deep':
        heidi = 'heidi/halpha_z_%g.dat'%(zmid)
        marta = 'marta/LF_Ha_z%g_Marta_middle.out'%(zmid)
        madau = 'marta/LF_Ha_z%g_Madau.out'%(zmid)
        finkelstein = 'finkelstein/LF_Ha_z%g.out'%(zmid)
        ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
        ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
        for fname in [heidi, marta, madau, finkelstein, ranga_hi, ranga_lo]: # 
            # if fname == heidi: la = 'Wu'; c='b'; ls='-'; alpha=0.5
            # if fname == marta: la = 'Silva'; c='g'; ls='-'; alpha=0.5
            # if fname == madau: la = 'Madau(from Silva)'; c='g'; ls='--'; alpha=0.5
            # if fname == ranga_hi: la = 'Chary(High)'; c='r'; ls='-'; alpha=0.5
            # if fname == ranga_lo: la = 'Chary(Low)'; c='r'; ls='--'
            # if fname == finkelstein: la = 'Finkelstein'; c='k'; ls='-'
            if fname == heidi: la = 'model 1'; c='b'; ls='-'; alpha=0.5
            if fname == marta: la = 'model 2'; c='g'; ls='-'; alpha=0.5
            if fname == madau: la = 'model 3'; c='g'; ls='--'; alpha=0.5
            if fname == ranga_hi: la = 'model 5 (High)'; c='r'; ls='-'; alpha=0.5
            if fname == ranga_lo: la = 'model 5 (Low)'; c='r'; ls='--'
            if fname == finkelstein: la = 'model 4'; c='k'; ls='-'



            data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
            L = data[:,0]
            #Phi = data[:,1]
            flux = L / (4*np.pi*d_L*d_L)
            n = data[:,1]
            b = (n > 1.e-6)&(n < 1e-2)
            if fname == heidi: b = (flux > 1e-18)
            plt.plot(flux[b], n[b], label=la, c=c, ls=ls, alpha=alpha)
    '''

    plt.xscale('log')
    plt.yscale('log')
    plt.legend(loc=3)
    plt.title(r'$%g<\lambda<%g,%.2f<z<%.2f$'%(lam_min, lam_max, zlo, zhi))
    plt.xlabel(r'Line flux $\rm [erg/s/cm^2]$')
    plt.ylabel(r'$\rm \Phi \ [Mpc^{-3}dex^{-1}]$')


    return n_obj #int(nobj_hi), int(nobj_lo)

'''
def plot_hist_for_Asantha():

    z, obj_hi, obj_lo = np.loadtxt(pa.data_loc+'luminosity_functions/number_stawman.dat',unpack=True)
    width = 0.35

    plt.figure(figsize=(10,7))
    ax = plt.subplot(111)
    rects1 = plt.bar(np.arange(0,6,2), np.log10(obj_hi[0::2]), width)#, color='b', label='High')
    rects2 = plt.bar(np.arange(0,6,2)+width, np.log10(obj_lo[0::2]), width)#, color='orange', label='Low')

    rects3 = plt.bar(np.arange(1,7,2), np.log10(obj_hi[1::2]), width, color='#1f77b4',alpha=0.5)
    rects4 = plt.bar(np.arange(1,7,2)+width, np.log10(obj_lo[1::2]), width, color='#ff7f0e',alpha=0.5)


    ax.set_xticks(range(6))
    ax.set_xticklabels(['$4<z<6$'+'\n'+'Deep', '$4<z<6$'+'\n'+'Wide', 
        '$6<z<8$'+'\n'+'Deep', '$6<z<8$'+'\n'+'Wide', 
        '$8<z<10$'+'\n'+'Deep', '$8<z<10$'+'\n'+'Wide'], fontsize='medium')

    ax.legend()
    x = np.arange(0,8)
    la = []
    for i in range(len(x)):
        la.append(r'$10^%i$'%(x[i]))
    ax.set_yticks(x)
    ax.set_yticklabels(la)

    ax.set_ylabel('Number counts')
    plt.savefig(plt_loc+'number_stawman_bar.pdf')

    #rects1 = ax.bar(z[0::2], obj_lo[0::2], width, color='b')
    #rects1 = ax.bar(z[1::2]+width, obj_lo[1::2], width, color='b')


    #ax.set_yscale('log')
    

plot_hist_for_Asantha()

exit()
'''

if __name__ == "__main__":
    #plot_sensitivity()
    for iband in range(6):
        out = calc_nobj(iband, name='deep')
        print('deep', out)
        out = calc_nobj(iband, name='allsky')
        print('allsky', out)
    plt.savefig('../../plots/luminosity_functions/spherex_number_counts.pdf')
    #plt.show()
    '''
    plt.figure(figsize=(21,7))
    zmid_list = [5, 7, 10]
    outfile = open(pa.data_loc+'luminosity_functions/number_stawman.dat','w')
    for i, zmid in enumerate(zmid_list):
        plt.subplot(1,3,i+1)    
        for name in ['deep', 'wide']:
            out = calc_nobj(zmid=zmid, name=name)
            print(name, 'zmid=',zmid)
            print('{0:,} {1:,}'.format(out[0],out[1]))
            print('######')
            outfile.write('%g %g %g \n'%(zmid, out[0], out[1]))
        plt.savefig(plt_loc+'/flux_func.png')
        plt.savefig(plt_loc+'/flux_func.pdf')
    outfile.close()

    plt.show()
    '''
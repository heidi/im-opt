#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa 

from scipy.interpolate import interp1d
from astropy import units as u
from astropy.cosmology import Planck15 as cosmo
co_list = ['b','g','r','c','m','y','k']
lam_rest = 0.65628 # um!
fsky = 300./41253.  # 300 sq deg
plt_loc = '../../plots/luminosity_functions/'

# fix R, plot various A on a figure
# run CDIM_APROBE/cdim_sbsens_fn_heidi.py first

A_list = [0.8, 1, 1.2]
t = 250

def plot_sensitivity():
    plt.figure(figsize=(21,7))
    R_list = [300, 500, 1000]
    for iR, R in enumerate(R_list):
        plt.subplot(1,3,iR+1)
        plt.title('R=%i'%(R))
        flux_th_output = np.zeros(len(A_list))
        for iA, A in enumerate(A_list):
            data = np.loadtxt(pa.data_loc+'luminosity_functions/sensitivity_zemcov/cdim_sbsens_out_A%g_R%i_t%i.txt'%(A, R, t), delimiter=',')
            lam = data[:,0]
            flux_th = data[:,3]
            b = (lam < 10)
            plt.plot(lam[b], flux_th[b], label='A=%g'%(A))

        z_list = np.arange(4,11,2)
        for iz, z in enumerate(z_list):
            plt.axvline(x=lam_rest*(1+z), label='z=%i'%(z), ls=':', c=co_list[iz])
        #plt.yscale('log')
        plt.title('R=%i'%(R))
        plt.legend()
        plt.xlabel(r'$\lambda [\mu m]$')
        plt.ylabel(r'1 $\sigma$ Line Sensitivity'+'\n'+r'$\rm [10^{-18}erg/s/cm^2]$')
    plt.savefig(plt_loc+'line_sensitivity_A_R.pdf')
    plt.savefig(plt_loc+'line_sensitivity_A_R.png')

def get_sensitivity(R, z):
    flux_th_output = np.zeros(len(A_list))
    for iA, A in enumerate(A_list):
        data = np.loadtxt(pa.data_loc+'luminosity_functions/sensitivity_zemcov/cdim_sbsens_out_A%g_R%i_t%i.txt'%(A, R, t), delimiter=',')
        lam = data[:,0]
        flux_th = data[:,3]
        flux_th_interp = interp1d(lam, flux_th)
        lam_obs = lam_rest * (1+z)
        print('flux th at ' , lam_obs, 'um is', flux_th_interp(lam_obs))
        flux_th_output[iA] = flux_th_interp(lam_obs) * (1.e-18)
    return flux_th_output


def calc_nobj(R):
    #for fname in [heidi, marta, madau, ranga_hi, ranga_lo, finkelstein]:
    # take Ranga's luminosity functions, convert to flux
    zmid_list = [5,7,10]
    plt.figure(figsize=(21,7))
    for iz, zmid in enumerate(zmid_list):
        plt.subplot(1,3,iz+1)
        dz = 1
        zlo = zmid - dz
        zhi = zmid + dz
        chi = cosmo.comoving_distance(zmid).value
        Mpc2cm = 3.086e+24
        d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

        lam_obs = lam_rest * (1+zmid)

        dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky


        ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
        ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)

        for fname in [ranga_lo, ranga_hi]:
            if fname == ranga_hi: la = 'Chary(High)'; c='r'; ls='-'
            if fname == ranga_lo: la = 'Chary(Low)'; c='r'; ls='--'
            data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
            L = data[:,0]
            flux = L / (4*np.pi*d_L*d_L)
            n = data[:,1]
            b = (n > 1.e-6)
            flux_th_list = get_sensitivity(R=R, z=zmid)
            plt.plot(flux[b], n[b], c='k', ls=ls)
            for iflux, flux_th in enumerate(flux_th_list):
                plt.xscale('log')
                plt.yscale('log')
                b1 = (flux > flux_th)
                n_obj = np.trapz(n[b1], x = np.log10(L[b1])) * dV
                print('number in 300 sq deg', n_obj)
                plt.axvline(x=flux_th, c=co_list[iflux], label=la+', A=%g, N=%i'%(A_list[iflux], n_obj))
        plt.legend()
        plt.title(r'$%g<z<%g$, R=%i, 300 sq deg'%(zlo,zhi,R))
        plt.xlabel(r'Line flux $\rm [erg/s/cm^2]$')
        plt.ylabel(r'$\rm \Phi \ [Mpc^{-3}dex^{-1}]$')

    plt.savefig(plt_loc+'nobj_R%i.pdf'%(R))
    plt.savefig(plt_loc+'nobj_R%i.png'%(R))

# integrate above the threshold and get counts/Mpc^3

# multiply by the volume in dz = 0.2 bins


if __name__ == "__main__":
    plot_sensitivity()
    #print(get_sensitivity())
    for R in [300,500,1000]:
        calc_nobj(R=R)
    plt.show()
    


#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.repo.params as pa 

from scipy.interpolate import interp1d
from astropy import units as u
from astropy.cosmology import Planck15 as cosmo
co_list = ['b','g','r','c','m','y','k']
lam_rest = 0.65628 # um!
plt_loc = '../../plots/luminosity_functions/'

"""
Sep 2018, using the new sensitivity
modified from number_strawman.py
Asantha want z > 6 only 
"""


# first take a look at the latest sensitivity
def plot_sensitivity():
    # previous sensitivity
    for name in ['deep', 'wide']:
        data = np.loadtxt('20171215/cdim_sbsens_out_R300_%s.txt'%(name), delimiter=',')
        lam = data[:,0]
        flux_th = data[:,3]
        plt.plot(lam, flux_th, c='k', ls='--', label='20171215 '+name)

    # this work
    for name in ['deep', 'medium', 'wide']:
        data = np.loadtxt('SD2_20180911/cdim_sbsens_SD2_%s_R300.txt'%(name), delimiter=',')
        ## wavelength [um],
        ## surface brightnes per R (1sigma) [nW/m^2/sr],
        ## surface brightness per R (1-sigma) [erg/s/cm^2/sr],
        ## surface brightness per R (1-sigma) [erg/s/cm^2/arcsec^2],
        ## point source sensitivity (5sigma) [uJy],
        ## point source sensitivity (5 sigma) [Mab]

        lam = data[:,0] # lambda is um
        freq_hz = 3.e+8/(lam*1e-6)

        R = 300. # R = lambda/dlambda = nu / dnu
        dfreq_hz = freq_hz / R
        flux_th = data[:,4] * (1.e-6) * (1.e-23) * dfreq_hz * (1.e+18)
        # data[:,4] is in uJy
        # 1e-6: make it Jy
        # 1e-23: make it erg/s/Hz/cm^2
        # dfreq: get rid of 1./Hz
        # 1e+18: make it (1e-18) erg/s/cm^2
        #print(flux_th)
        plt.plot(lam, flux_th, label='SD2 '+name+' 5sigma')

    plt.xlabel(r'$\lambda [\mu m]$')
    plt.ylabel(r'1 $\sigma$ Line Sensitivity'+'\n'+r'$\rm [10^{-18}erg/s/cm^2]$')
    plt.legend()
    plt.savefig('../../plots/luminosity_functions/sensitivity_SD2.pdf')
    plt.show()



def calc_nobj(zmid=10, dz=1, name='deep'):
    ### calculating the number of object ###
    if name=='deep': survey_area_sq_deg=15.6
    if name=='medium': survey_area_sq_deg=31.1
    if name=='wide': survey_area_sq_deg=311
    data = np.loadtxt('SD2_20180911/cdim_sbsens_SD2_%s_R300.txt'%(name), delimiter=',')
    lam = data[:,0] # lambda is um
    freq_hz = 3.e+8/(lam*1e-6)
    R = 300. # R = lambda/dlambda = nu / dnu
    dfreq_hz = freq_hz / R
    # divide 5-sigma value by 5 to get 1-sigma
    nsigma = 3.5
    flux_th = data[:,4] * (1.e-6) * (1.e-23) * dfreq_hz * (1.e+18) / 5.* nsigma
    flux_th_interp = interp1d(lam, flux_th)
    lam_obs = lam_rest * (1.+zmid)
    print('flux th at ' , lam_obs, 'um is', flux_th_interp(lam_obs))
    flux_th = flux_th_interp(lam_obs) * (1.e-18)

    zlo = zmid - dz
    zhi = zmid + dz
    chi = cosmo.comoving_distance(zmid).value
    Mpc2cm = 3.086e+24
    d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

    lam_obs = lam_rest * (1+zmid)

    fsky = survey_area_sq_deg/41253.
    dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky
    print('cosmo.comoving_volume(zhi)', cosmo.comoving_volume(zhi))
    print('dV', dV)
    ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
    ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)


    for fname in [ranga_lo, ranga_hi]:
        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        flux = L / (4*np.pi*d_L*d_L)
        n = data[:,1]
        b1 = (flux > flux_th)
        n_obj = np.trapz(n[b1], x = np.log10(flux[b1])) * dV
        #print(fname, n_obj)
        if fname == ranga_hi: nobj_hi = n_obj; c='C0'; la='optimistic'
        if fname == ranga_lo: nobj_lo = n_obj; c='C1'; la='pessimistic'
        b = (n > 1.e-6)&(flux > 1e-19)
        if name=='deep': 
            plt.plot(flux[b], n[b], c=c, label=la, lw=2)
            
    if fname == ranga_hi:
        if name=='deep': c='C2'
        if name=='medium': c='C3'
        if name=='wide': c='C4'
        plt.axvline(x=flux_th, c=c, ls='--', label=name)
        plt.legend(loc=3)

    # other peoples' model
    if False:#name == 'deep':
        heidi = 'heidi/halpha_z_%g.dat'%(zmid)
        marta = 'marta/LF_Ha_z%g_Marta_middle.out'%(zmid)
        madau = 'marta/LF_Ha_z%g_Madau.out'%(zmid)
        finkelstein = 'finkelstein/LF_Ha_z%g.out'%(zmid)
        ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
        ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
        for fname in [heidi, marta, madau, finkelstein, ranga_hi, ranga_lo]: # 
            # if fname == heidi: la = 'Wu'; c='b'; ls='-'; alpha=0.5
            # if fname == marta: la = 'Silva'; c='g'; ls='-'; alpha=0.5
            # if fname == madau: la = 'Madau(from Silva)'; c='g'; ls='--'; alpha=0.5
            # if fname == ranga_hi: la = 'Chary(High)'; c='r'; ls='-'; alpha=0.5
            # if fname == ranga_lo: la = 'Chary(Low)'; c='r'; ls='--'
            # if fname == finkelstein: la = 'Finkelstein'; c='k'; ls='-'
            if fname == heidi: la = 'model 1'; c='b'; ls='-'; alpha=0.5
            if fname == marta: la = 'model 2'; c='g'; ls='-'; alpha=0.5
            if fname == madau: la = 'model 3'; c='g'; ls='--'; alpha=0.5
            if fname == ranga_hi: la = 'model 5 (High)'; c='r'; ls='-'; alpha=0.5
            if fname == ranga_lo: la = 'model 5 (Low)'; c='r'; ls='--'
            if fname == finkelstein: la = 'model 4'; c='k'; ls='-'

            data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
            L = data[:,0]
            #Phi = data[:,1]
            flux = L / (4*np.pi*d_L*d_L)
            n = data[:,1]
            b = (n > 1.e-6)&(n < 1e-2)
            if fname == heidi: b = (flux > 1e-18)
            plt.plot(flux[b], n[b], label=la, c=c, ls=ls, alpha=alpha)
    plt.xscale('log')
    plt.yscale('log')
    
    plt.title(r'$%g<z<%g$'%(zlo,zhi))
    plt.xlabel(r'Line flux $\rm [erg/s/cm^2]$')
    plt.ylabel(r'$\rm \Phi \ [Mpc^{-3}dex^{-1}]$')
    return int(nobj_hi), int(nobj_lo)


def plot_LF_sensitivity():
    plt.figure(figsize=(21,7))
    zmid_list = [5, 7, 10]
    outfile = open('SD2_20180911/number_SD2.dat','w')
    for i, zmid in enumerate(zmid_list):
        plt.subplot(1,3,i+1)
        outfile.write('#zmid, number_hi, number_low \n')
        for name in ['deep', 'medium', 'wide']:
            out = calc_nobj(zmid=zmid, name=name)
            print(name, 'zmid=',zmid)
            print('{0:,} {1:,}'.format(out[0],out[1]))
            print('######')
            outfile.write('#%s\n'%(name))
            outfile.write('%g %g %g \n'%(zmid, out[0], out[1]))
        plt.savefig(plt_loc+'/flux_func_SD2.png')
        plt.savefig(plt_loc+'/flux_func_SD2.pdf')
    outfile.close()



def plot_hist_for_Asantha_SD2():
    z, obj_hi, obj_lo = np.loadtxt('SD2_20180911/number_SD2.dat',unpack=True)
    width = 0.35

    plt.figure(figsize=(10,7))
    ax = plt.subplot(111)
    # deep
    rects1 = plt.bar(np.arange(0,9,3), np.log10(obj_hi[0::3]), width, color='C0') # number_high
    rects2 = plt.bar(np.arange(0,9,3)+width, np.log10(obj_lo[0::3]), width, color='C1') # number low
    # medium
    rects3 = plt.bar(np.arange(1,10,3), np.log10(obj_hi[1::3]), width, color='C0',alpha=0.7)
    rects4 = plt.bar(np.arange(1,10,3)+width, np.log10(obj_lo[1::3]), width, color='C1', alpha=0.7)
    # wide
    rects3 = plt.bar(np.arange(2,11,3), np.log10(obj_hi[2::3]), width, color='C0',alpha=0.3)
    rects4 = plt.bar(np.arange(2,11,3)+width, np.log10(obj_lo[2::3]), width, color='C1', alpha=0.3)


    ax.set_xticks(range(9))
    ax.set_xticklabels([
        '$4<z<6$'+'\n'+'Deep',  '$4<z<6$'+'\n'+'Medium',  '$4<z<6$'+'\n'+'Wide', 
        '$6<z<8$'+'\n'+'Deep',  '$6<z<8$'+'\n'+'Medium',  '$6<z<8$'+'\n'+'Wide', 
        '$8<z<10$'+'\n'+'Deep', '$8<z<10$'+'\n'+'Medium', '$8<z<10$'+'\n'+'Wide']
        , fontsize='medium')

    ax.legend()
    x = np.arange(0,8)
    la = []
    for i in range(len(x)):
        la.append(r'$10^%i$'%(x[i]))
    ax.set_yticks(x)
    ax.set_yticklabels(la)

    ax.set_ylabel('Number counts')
    plt.savefig(plt_loc+'number_bar_SD2.pdf')



def plot_hist_for_Asantha_SD2_no_pessimistic():
    z, obj_hi, obj_lo = np.loadtxt('SD2_20180911/number_SD2.dat',unpack=True)
    width = 0.35

    plt.figure(figsize=(10,7))
    ax = plt.subplot(111)
    # deep
    rects1 = plt.bar(np.arange(0,6,3), np.log10(obj_hi[3::3]), width, color='C2') # number_high
    #rects2 = plt.bar(np.arange(0,9,3)+width, np.log10(obj_lo[0::3]), width, color='C1') # number low
    # medium
    rects3 = plt.bar(np.arange(1,7,3), np.log10(obj_hi[4::3]), width, color='C3')#,alpha=0.7)
    #rects4 = plt.bar(np.arange(1,10,3)+width, np.log10(obj_lo[1::3]), width, color='C1', alpha=0.7)
    # wide
    rects3 = plt.bar(np.arange(2,8,3), np.log10(obj_hi[5::3]), width, color='C4')#,alpha=0.3)
    #rects4 = plt.bar(np.arange(2,11,3)+width, np.log10(obj_lo[2::3]), width, color='C1', alpha=0.3)

    '''
    # plot cumulative
    # 4 < z < 6 
    cum = np.array([np.log10(sum(obj_hi[0:3])), np.log10(sum(obj_hi[1:3])), np.log10(sum(obj_hi[2:3]))])
    plt.plot(np.arange(3), cum, marker='s', c='k', drawstyle='steps')

    # 6 < z < 8 
    cum = np.array([np.log10(sum(obj_hi[3:6])), np.log10(sum(obj_hi[4:6])), np.log10(sum(obj_hi[5:6]))])
    plt.plot(np.arange(3,6), cum, marker='s', c='k', drawstyle='steps')

    # 8 < z < 10 
    cum = np.array([np.log10(sum(obj_hi[6:9])), np.log10(sum(obj_hi[7:9])), np.log10(sum(obj_hi[8:9]))])
    plt.plot(np.arange(6,9), cum, marker='s', c='k', drawstyle='steps', label='Cumulative counts')
    '''

    cum = np.zeros(6)
    for i in range(6):
        start = 3+i
        cum[i] = np.log10(sum(obj_hi[start:9]))

    plt.plot(np.arange(6), cum, marker='s', c='k', drawstyle='steps', label='Cumulative counts')


    plt.axhline(4, label='Science Requirement ($6<z<10$)', c='gray', ls='--')

    ax.set_xticks(range(6))
    ax.set_xticklabels([
        '$6<z<8$'+'\n'+'Deep',  '$6<z<8$'+'\n'+'Medium',  '$6<z<8$'+'\n'+'Wide', 
        '$8<z<10$'+'\n'+'Deep', '$8<z<10$'+'\n'+'Medium', '$8<z<10$'+'\n'+'Wide']
        , fontsize='medium')

    ax.legend()
    x = np.arange(0,8)
    la = []
    for i in range(len(x)):
        la.append(r'$10^%i$'%(x[i]))
    ax.set_yticks(x)
    ax.set_yticklabels(la)

    ax.set_ylabel('Number counts')
    plt.savefig(plt_loc+'number_bar_SD2_3.5sig.pdf')


def calc_requirement(factor, opt_or_pes): # factor: fractional of current
    # what is the sensitivity that give 10^5 sources? take 4<z<6, wide
    zmid = 7
    dz = 1
    name='wide'
    survey_area_sq_deg=15.6
    ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
    ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
    if opt_or_pes=='opt': fname = ranga_hi
    if opt_or_pes=='pes': fname = ranga_lo

    n_obj = 0.
    for name in ['wide', 'medium', 'deep']:
        data = np.loadtxt('SD2_20180911/cdim_sbsens_SD2_%s_R300.txt'%(name), delimiter=',')
        lam = data[:,0] # lambda is um
        freq_hz = 3.e+8/(lam*1e-6)
        R = 300. # R = lambda/dlambda = nu / dnu
        dfreq_hz = freq_hz / R
        # divide 5-sigma value by 5 to get 1-sigma
        nsigma = 3.5
        flux_th = data[:,4] * (1.e-6) * (1.e-23) * dfreq_hz * (1.e+18) / 5.* nsigma
        flux_th_interp = interp1d(lam, flux_th)
        lam_obs = lam_rest * (1.+zmid)
        flux_th = flux_th_interp(lam_obs) * (1.e-18) * factor
        print(name,'flux th at ' , lam_obs, 'um is', flux_th)

        zlo = zmid - dz
        zhi = zmid + dz
        chi = cosmo.comoving_distance(zmid).value
        Mpc2cm = 3.086e+24
        d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

        lam_obs = lam_rest * (1+zmid)

        fsky = survey_area_sq_deg/41253.
        dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky
        #print('cosmo.comoving_volume(zhi)', cosmo.comoving_volume(zhi))
        #print('dV', dV)
        
        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        flux = L / (4*np.pi*d_L*d_L)
        n = data[:,1]
        b1 = (flux > flux_th)
        n_obj += np.trapz(n[b1], x = np.log10(flux[b1])) * dV
    print('n_obj = %e'%n_obj)



def calc_requirement_z_5_8(factor, opt_or_pes): # factor: fractional of current
    # what is the sensitivity that give 10^5 sources? take 4<z<6, wide
    name = 'deep'
    survey_area_sq_deg=15.6
    n_obj = 0.
    for zmid in [5,7]:
        ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
        ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
        if opt_or_pes=='opt': fname = ranga_hi
        if opt_or_pes=='pes': fname = ranga_lo        
        data = np.loadtxt('SD2_20180911/cdim_sbsens_SD2_%s_R300.txt'%(name), delimiter=',')
        lam = data[:,0] # lambda is um
        freq_hz = 3.e+8/(lam*1e-6)
        R = 300. # R = lambda/dlambda = nu / dnu
        dfreq_hz = freq_hz / R
        # divide 5-sigma value by 5 to get 1-sigma
        nsigma = 3.5
        flux_th = data[:,4] * (1.e-6) * (1.e-23) * dfreq_hz * (1.e+18) / 5.* nsigma
        flux_th_interp = interp1d(lam, flux_th)
        lam_obs = lam_rest * (1.+zmid)
        flux_th = flux_th_interp(lam_obs) * (1.e-18) * factor
        print(name,'flux th at ' , lam_obs, 'um is', flux_th)

        if zmid == 5:
            zlo = 5#zmid - dz
            zhi = 6#zmid + dz
        if zmid == 7:
            zlo = 6
            zhi = 8
        chi = cosmo.comoving_distance(zmid).value
        Mpc2cm = 3.086e+24
        d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

        lam_obs = lam_rest * (1+zmid)

        fsky = survey_area_sq_deg/41253.
        dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky
        #print('cosmo.comoving_volume(zhi)', cosmo.comoving_volume(zhi))
        #print('dV', dV)
        
        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        flux = L / (4*np.pi*d_L*d_L)
        n = data[:,1]
        b1 = (flux > flux_th)
        n_obj += np.trapz(n[b1], x = np.log10(flux[b1])) * dV
    print('n_obj = %e'%n_obj)

def calc_requirement_z_7_8(factor, opt_or_pes): # factor: fractional of current
    # what is the sensitivity that give 10^5 sources? take 4<z<6, wide
    name = 'deep'
    survey_area_sq_deg=15.6
    n_obj = 0.
    for zmid in [7]:
        ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
        ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
        if opt_or_pes=='opt': fname = ranga_hi
        if opt_or_pes=='pes': fname = ranga_lo        
        data = np.loadtxt('SD2_20180911/cdim_sbsens_SD2_%s_R300.txt'%(name), delimiter=',')
        lam = data[:,0] # lambda is um
        freq_hz = 3.e+8/(lam*1e-6)
        R = 300. # R = lambda/dlambda = nu / dnu
        dfreq_hz = freq_hz / R
        # divide 5-sigma value by 5 to get 1-sigma
        nsigma = 3.5
        flux_th = data[:,4] * (1.e-6) * (1.e-23) * dfreq_hz * (1.e+18) / 5.* nsigma
        flux_th_interp = interp1d(lam, flux_th)
        lam_obs = lam_rest * (1.+zmid)
        flux_th = flux_th_interp(lam_obs) * (1.e-18) * factor
        print(name,'flux th at ' , lam_obs, 'um is', flux_th)


        if zmid == 7:
            zlo = 7
            zhi = 8
        chi = cosmo.comoving_distance(zmid).value
        Mpc2cm = 3.086e+24
        d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

        lam_obs = lam_rest * (1+zmid)

        fsky = survey_area_sq_deg/41253.
        dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky
        #print('cosmo.comoving_volume(zhi)', cosmo.comoving_volume(zhi))
        #print('dV', dV)
        
        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        flux = L / (4*np.pi*d_L*d_L)
        n = data[:,1]
        b1 = (flux > flux_th)
        n_obj += np.trapz(n[b1], x = np.log10(flux[b1])) * dV
    print('n_obj = %e'%n_obj)


if __name__ == "__main__":
    #plot_sensitivity()
    #
    #plot_hist_for_Asantha_SD2()
    #plot_LF_sensitivity()
    #plot_hist_for_Asantha_SD2_no_pessimistic()
    calc_requirement_z_5_8(factor=9, opt_or_pes='opt')
    calc_requirement_z_5_8(factor=0.85, opt_or_pes='pes')
    calc_requirement_z_7_8(factor=2.8, opt_or_pes='opt')
    calc_requirement_z_7_8(factor=0.27, opt_or_pes='pes')
    plt.show()
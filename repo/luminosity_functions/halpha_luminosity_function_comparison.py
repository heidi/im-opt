#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa 


# Note! for zmid = 10, Heidi only has [9,10]

plt.figure(figsize=(21,7))
zmid_list = [5,7,10]
for iz in range(len(zmid_list)):
    plt.subplot(1,3,iz+1)
    zmid = zmid_list[iz]
    heidi = 'heidi/halpha_z_%g.dat'%(zmid)
    marta = 'marta/LF_Ha_z%g_Marta_middle.out'%(zmid)
    madau = 'marta/LF_Ha_z%g_Madau.out'%(zmid)
    ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
    ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
    finkelstein = 'finkelstein/LF_Ha_z%g.out'%(zmid)
    for fname in [heidi, marta, madau, ranga_hi, ranga_lo, finkelstein]:

        if fname == heidi: la = 'Wu'; c='b'; ls='-'
        if fname == marta: la = 'Silva'; c='g'; ls='-'
        if fname == madau: la = 'Madau(from Silva)'; c='g'; ls='--'
        if fname == ranga_hi: la = 'Chary(High)'; c='r'; ls='-'
        if fname == ranga_lo: la = 'Chary(Low)'; c='r'; ls='--'
        if fname == finkelstein: la = 'Finkelstein'; c='k'; ls='-'


        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        Phi = data[:,1]

        plt.plot(L, Phi, label=la, c=c, ls=ls)
    plt.xlim(1.e+39, None)
    plt.ylim(1.e-5, None)
    plt.xlabel(r"$\rm L(H\alpha) [erg/s]$")
    plt.ylabel(r"$\rm \Phi [Mpc^{-3}dex^{-1}]$")

    plt.legend()
    plt.title('z=%g'%(zmid))

    plt.xscale('log')
    plt.yscale('log')
plt.savefig('../../plots/luminosity_functions/halpha_luminosity_function_comparison.pdf')
plt.show()
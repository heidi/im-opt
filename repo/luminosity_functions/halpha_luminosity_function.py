#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import os
import IM_opt.repo.params as pa 
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')

import fitsio
#from fitsio import FITS, FITSHDR



# Note! for zmid = 10, only do [9,10] (I don't have z > 10..)
# for zmin=0, only do [0,1]
class LuminosityFunction(object):
    def __init__(self, zmid=5): # e.g. zmid=5, zmin=4, zmax = 6
        self.zmid = zmid
        self.zmin = max(0, zmid-1)
        self.zmax = zmid + 1 
        if zmid > 0: self.zmin_file_list = [zmid-1, zmid]
        else: self.zmin_file_list = [zmid]

    def calc_luminosity_function(self):

        lgLHalpha = np.array([])
        z = np.array([])
     
        for zmin_file in self.zmin_file_list:
            zmax_file = zmin_file + 1
            fname = pa.data_master_loc+'lines_for_spherex/lines_z_%i_%i.fits'%(zmin_file, zmin_file+1)
            if os.path.exists(fname)==False:
                print('this does not exist: '+fname)
            else:
                data, h = fitsio.read(fname,header=True)
                # print(h)
                lgLHalpha = np.append(lgLHalpha, data['lgHalpha'])
                z = np.append(z, data['z'])



        b = (z > self.zmin)&(z < self.zmax)
        lgLHalpha = lgLHalpha[b]
        #plt.hist(lgLHalpha)
        #plt.show()
        #exit()
        vol = (sim.VC_interp(self.zmax)-sim.VC_interp(self.zmin))*params.fsky
        print(self.zmin, self.zmax, 'vol**(1/3)', vol**(1./3.))

        # halo mass functions 
        Qlist = lgLHalpha
        nbins = 40
        lowest = 39#min(Qlist)
        highest = max(Qlist)
        binsize = (highest-lowest)/(nbins*1.)
        x_list = np.zeros(nbins); y_list = np.zeros(nbins)
        outfile = open(pa.data_loc+'luminosity_functions/heidi/halpha_z_%g.dat'%(self.zmid),'w')
        outfile.write('# zmin=%g, zmax=%g \n'%(self.zmin, self.zmax))
        outfile.write('# L_Halpha[erg/s], Phi[Mpc^-3 dex^-1]\n')
        for jj in np.arange(nbins):
            lower = lowest + binsize*jj
            upper = lowest + binsize*(jj+1)
            hh = len(Qlist[(Qlist>=lower)*(Qlist<upper)])
            x_list[jj] = (lowest+(jj+0.5)*binsize)
            y_list[jj] = hh/binsize/vol
            outfile.write('%e %e \n'%(10**x_list[jj], y_list[jj]))
        outfile.close()


        plt.plot(x_list, y_list, label='z=%g'%(self.zmid))
        plt.legend()
        plt.yscale('log')
        plt.xlabel(r'$\rm log_{10}L_{H\alpha} [erg/s]$')
        plt.ylabel(r'$\rm \Phi [Mpc^{-3} dex^{-1}]$')
        plt.savefig('../../plots/luminosity_functions/halpha_luminosity_functions_heidi.pdf')



if __name__ == "__main__":
    for zmid in [0]:#range(8):#[5,7,10]:#
        lf = LuminosityFunction(zmid=zmid)
        lf.calc_luminosity_function()
    plt.show()


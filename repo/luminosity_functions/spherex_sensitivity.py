#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa 

c = 3e+10 # speed of light in cm/s

#Band 1-3 have R = 41, 4 has R =38, 5 has R=85 and 6 has R=135
nlam = 16
R = 41 + np.zeros(nlam*3)
R = np.append(R, 38+np.zeros(nlam))
R = np.append(R, 85+np.zeros(nlam))
R = np.append(R, 135+np.zeros(nlam))

def convert_to_line_sensitivity():
    plt.figure(figsize=(21,7))
    lam, dF, M_AB_allsky, M_AB_deep = np.loadtxt('spherex/012718_v23_base_cbe.dat', unpack=True)

    plt.subplot(131)
    plt.plot(lam, dF)
    plt.xlabel(r'$\lambda [\mu m]$')
    plt.ylabel(r'$\rm \Delta F (1\sigma)$')

    plt.subplot(132)
    plt.plot(lam, M_AB_allsky, label=r'$5\sigma$,  all sky')
    plt.plot(lam, M_AB_deep, label=r'$5\sigma$,  deep')

    Nsig = 1
    # convert to Mab
    Mab_1sig = -2.5*np.log10(Nsig*1.e-6*dF/3631.)
    plt.plot(lam, Mab_1sig, label=r'$1\sigma$,  all sky')
    plt.legend()
    plt.xlabel(r'$\lambda [\mu m]$')
    plt.ylabel(r'$\rm m_{AB}$')

    plt.subplot(133)

    flux_jy_allsky = 3631*10**(-M_AB_allsky/2.5)
    flux_jy_deep = 3631*10**(-M_AB_deep/2.5)

    line_cgs_allsky = 1e18* (flux_jy_allsky * 1e-23) * c / R / (lam * 1e-4)
    line_cgs_deep = 1e18* (flux_jy_deep * 1e-23) * c / R / (lam * 1e-4)

    plt.plot(lam, line_cgs_allsky, label='all sky')
    plt.plot(lam, line_cgs_deep, label='deep')


    # check with Zemcov's code
    # convert to line sensitivity using Zemcov's code
    # assume some number of sigma
    #Mab_single = -2.5*np.log10(Nsig*1e-6*dF_single/3631.)
    # convert to line flux
    #Sline_mike = 1e18*Nsig * 3e-14 * dF / R / (lam * 1e4) * 1e7 * 1e-4
    #plt.subplot(224)
    # Basically, Sline = dF * c/R/lambda # explicitly checked with Mike's output
    Sline_1sig = (1e18) * Nsig * (dF * 1e-6 * 1e-23) * c / R / (lam * 1e-4)

    Nsig = 5
    Sline_5sig = (1e18) * Nsig * (dF * 1e-6 * 1e-23) * c / R / (lam * 1e-4)
    #plt.scatter(lam, Sline_5sig, label='zemcov') # correct!
    #plt.plot(lam, Sline_1sig, label=r'$1\sigma$')
    #b = (lam > 4.18)
    #plt.plot(lam[b], Sline[b])#, m='o')
    plt.legend()
    plt.xlabel(r'$\lambda [\mu m]$')
    plt.ylabel(r'5$\sigma$ Line Sensitivity'+'\n'+r'$\rm [10^{-18}erg/s/cm^2]$')
    plt.savefig('../../plots/luminosity_functions/spherex_line_sensitivity.pdf')

    outfile = open('spherex/line_sensitivity_allsky.dat','w')
    outfile.write('# lambda[um], 5-sigma line sensitivity [10^{-18}erg/s/cm^2] \n')
    for i in range(len(lam)):
        outfile.write('%g %g \n'%(lam[i], line_cgs_allsky[i]))
    outfile.close()

    outfile = open('spherex/line_sensitivity_deep.dat','w')
    outfile.write('# lambda[um], 5-sigma line sensitivity [10^{-18}erg/s/cm^2] \n')
    for i in range(len(lam)):
        outfile.write('%g %g \n'%(lam[i], line_cgs_deep[i]))
    outfile.close()


if __name__ == "__main__":
    convert_to_line_sensitivity()
    plt.show()

'''
from scipy.interpolate import interp1d
from astropy import units as u
from astropy.cosmology import Planck15 as cosmo
co_list = ['b','g','r','c','m','y','k']
lam_rest = 0.65628 # um!
plt_loc = '../../plots/luminosity_functions/'

# note! with time dependence
# import sys
# sys.path.append('CDIM_APROBE/')
# from cdim_sbsens_fn_heidi import calculate_cdim_sbsems

#out_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/luminosity_functions/sensitivity_zemcov/'
#calculate_cdim_sbsems(Apert=1.0, R=500, t_int = 200, out_loc=out_loc)

def plot_sensitivity():
    # new stuff by mike
    for name in ['deep', 'wide']:
        data = np.loadtxt('20171215/cdim_sbsens_out_R300_%s.txt'%(name), delimiter=',')
        lam = data[:,0]
        flux_th = data[:,3]
        plt.plot(lam, flux_th, c='k', ls='--', label='latest '+name)


    A = 0.83
    R = 300
    for t in [768, 852, 6240, 6780, 7500]:
        data = np.loadtxt(pa.data_loc+'luminosity_functions/sensitivity_zemcov/cdim_sbsens_out_A%g_R%i_t%i.txt'%(A, R, t), delimiter=',')
        lam = data[:,0]
        flux_th = data[:,3]
        b = (lam < 10)
        plt.plot(lam[b], flux_th[b], label='old %g min'%(t/60.))

    plt.xlabel(r'$\lambda [\mu m]$')
    plt.ylabel(r'1 $\sigma$ Line Sensitivity'+'\n'+r'$\rm [10^{-18}erg/s/cm^2]$')
    plt.legend()
    plt.savefig('../../plots/luminosity_functions/sensitivity_old_new.pdf')
    plt.show()


#plot_sensitivity()
#exit()


def calc_nobj(zmid=10, dz=1, name='deep'):
    if name=='deep': survey_area_sq_deg=31.1
    if name=='wide': survey_area_sq_deg=280
    data = np.loadtxt('20171215/cdim_sbsens_out_R300_%s.txt'%(name), delimiter=',')
    lam = data[:,0]
    flux_th = data[:,3]
    flux_th_interp = interp1d(lam, flux_th)
    lam_obs = lam_rest * (1+zmid)
    print('flux th at ' , lam_obs, 'um is', flux_th_interp(lam_obs))
    flux_th = flux_th_interp(lam_obs) * (1.e-18)

    zlo = zmid - dz
    zhi = zmid + dz
    chi = cosmo.comoving_distance(zmid).value
    Mpc2cm = 3.086e+24
    d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

    lam_obs = lam_rest * (1+zmid)

    fsky = survey_area_sq_deg/41253.
    dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky
    print('cosmo.comoving_volume(zhi)', cosmo.comoving_volume(zhi))
    print('dV', dV)
    ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
    ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)


    for fname in [ranga_lo, ranga_hi]:
        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        flux = L / (4*np.pi*d_L*d_L)
        n = data[:,1]
        b1 = (flux > flux_th)
        n_obj = np.trapz(n[b1], x = np.log10(flux[b1])) * dV
        #print(fname, n_obj)
        if fname == ranga_hi: nobj_hi = n_obj; c='r'; ls='-'
        if fname == ranga_lo: nobj_lo = n_obj; c='r'; ls='--'
        b = (n > 1.e-6)
        plt.plot(flux[b], n[b], c=c, ls=ls)
        plt.axvline(x=flux_th, c='k')

    #plt.savefig(plt_loc+'')
    

    # other peoples' model
    if name == 'deep':
        heidi = 'heidi/halpha_z_%g.dat'%(zmid)
        marta = 'marta/LF_Ha_z%g_Marta_middle.out'%(zmid)
        madau = 'marta/LF_Ha_z%g_Madau.out'%(zmid)
        finkelstein = 'finkelstein/LF_Ha_z%g.out'%(zmid)
        ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
        ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)
        for fname in [heidi, marta, madau, finkelstein, ranga_hi, ranga_lo]: # 
            # if fname == heidi: la = 'Wu'; c='b'; ls='-'; alpha=0.5
            # if fname == marta: la = 'Silva'; c='g'; ls='-'; alpha=0.5
            # if fname == madau: la = 'Madau(from Silva)'; c='g'; ls='--'; alpha=0.5
            # if fname == ranga_hi: la = 'Chary(High)'; c='r'; ls='-'; alpha=0.5
            # if fname == ranga_lo: la = 'Chary(Low)'; c='r'; ls='--'
            # if fname == finkelstein: la = 'Finkelstein'; c='k'; ls='-'
            if fname == heidi: la = 'model 1'; c='b'; ls='-'; alpha=0.5
            if fname == marta: la = 'model 2'; c='g'; ls='-'; alpha=0.5
            if fname == madau: la = 'model 3'; c='g'; ls='--'; alpha=0.5
            if fname == ranga_hi: la = 'model 5 (High)'; c='r'; ls='-'; alpha=0.5
            if fname == ranga_lo: la = 'model 5 (Low)'; c='r'; ls='--'
            if fname == finkelstein: la = 'model 4'; c='k'; ls='-'



            data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
            L = data[:,0]
            #Phi = data[:,1]
            flux = L / (4*np.pi*d_L*d_L)
            n = data[:,1]
            b = (n > 1.e-6)&(n < 1e-2)
            if fname == heidi: b = (flux > 1e-18)
            plt.plot(flux[b], n[b], label=la, c=c, ls=ls, alpha=alpha)


    plt.xscale('log')
    plt.yscale('log')
    plt.legend(loc=3)
    plt.title(r'$%g<z<%g$'%(zlo,zhi))
    plt.xlabel(r'Line flux $\rm [erg/s/cm^2]$')
    plt.ylabel(r'$\rm \Phi \ [Mpc^{-3}dex^{-1}]$')


    return int(nobj_hi), int(nobj_lo)


def plot_hist_for_Asantha():

    z, obj_hi, obj_lo = np.loadtxt(pa.data_loc+'luminosity_functions/number_stawman.dat',unpack=True)
    width = 0.35

    plt.figure(figsize=(10,7))
    ax = plt.subplot(111)
    rects1 = plt.bar(np.arange(0,6,2), np.log10(obj_hi[0::2]), width)#, color='b', label='High')
    rects2 = plt.bar(np.arange(0,6,2)+width, np.log10(obj_lo[0::2]), width)#, color='orange', label='Low')

    rects3 = plt.bar(np.arange(1,7,2), np.log10(obj_hi[1::2]), width, color='#1f77b4',alpha=0.5)
    rects4 = plt.bar(np.arange(1,7,2)+width, np.log10(obj_lo[1::2]), width, color='#ff7f0e',alpha=0.5)


    ax.set_xticks(range(6))
    ax.set_xticklabels(['$4<z<6$'+'\n'+'Deep', '$4<z<6$'+'\n'+'Wide', 
        '$6<z<8$'+'\n'+'Deep', '$6<z<8$'+'\n'+'Wide', 
        '$8<z<10$'+'\n'+'Deep', '$8<z<10$'+'\n'+'Wide'], fontsize='medium')

    ax.legend()
    x = np.arange(0,8)
    la = []
    for i in range(len(x)):
        la.append(r'$10^%i$'%(x[i]))
    ax.set_yticks(x)
    ax.set_yticklabels(la)

    ax.set_ylabel('Number counts')
    plt.savefig(plt_loc+'number_stawman_bar.pdf')

    #rects1 = ax.bar(z[0::2], obj_lo[0::2], width, color='b')
    #rects1 = ax.bar(z[1::2]+width, obj_lo[1::2], width, color='b')


    #ax.set_yscale('log')
    plt.show()

plot_hist_for_Asantha()

exit()


if __name__ == "__main__":
    plt.figure(figsize=(21,7))
    zmid_list = [5, 7, 10]
    outfile = open(pa.data_loc+'luminosity_functions/number_stawman.dat','w')
    for i, zmid in enumerate(zmid_list):
        plt.subplot(1,3,i+1)    
        for name in ['deep', 'wide']:
            out = calc_nobj(zmid=zmid, name=name)
            print(name, 'zmid=',zmid)
            print('{0:,} {1:,}'.format(out[0],out[1]))
            print('######')
            outfile.write('%g %g %g \n'%(zmid, out[0], out[1]))
        plt.savefig(plt_loc+'/flux_func.png')
        plt.savefig(plt_loc+'/flux_func.pdf')
    outfile.close()
'''

#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import IM_opt.repo.params as pa 

from scipy.interpolate import interp1d
from astropy import units as u
from astropy.cosmology import Planck15 as cosmo
co_list = ['b','g','r','c','m','y','k']
lam_rest = 0.65628 # um!

# note! with time dependence
import sys
sys.path.append('CDIM_APROBE/')
from cdim_sbsens_fn_heidi import calculate_cdim_sbsems

out_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/luminosity_functions/sensitivity_zemcov/'
#calculate_cdim_sbsems(Apert=1.0, R=500, t_int = 200, out_loc=out_loc)

def calc_nobj(A=1, R=500, t=200, zmid=10, dz=1, survey_area_sq_deg=300):
    calculate_cdim_sbsems(Apert=A, R=R, t_int=t, out_loc=out_loc)

    data = np.loadtxt(pa.data_loc+'luminosity_functions/sensitivity_zemcov/cdim_sbsens_out_A%g_R%i_t%i.txt'%(A, R, t), delimiter=',')
    lam = data[:,0]
    flux_th = data[:,3]
    flux_th_interp = interp1d(lam, flux_th)
    lam_obs = lam_rest * (1+zmid)
    print('flux th at ' , lam_obs, 'um is', flux_th_interp(lam_obs))
    flux_th = flux_th_interp(lam_obs) * (1.e-18)

    zlo = zmid - dz
    zhi = zmid + dz
    chi = cosmo.comoving_distance(zmid).value
    Mpc2cm = 3.086e+24
    d_L = cosmo.luminosity_distance(zmid).value * Mpc2cm

    lam_obs = lam_rest * (1+zmid)

    fsky = survey_area_sq_deg/41253.  # 300 sq deg
    dV = (cosmo.comoving_volume(zhi).value - cosmo.comoving_volume(zlo).value)/(4*np.pi)*fsky

    ranga_hi = 'ranga/LF_Ha_cgs_z%g_high.out'%(zmid)
    ranga_lo = 'ranga/LF_Ha_cgs_z%g_low.out'%(zmid)


    for fname in [ranga_lo, ranga_hi]:
        data = np.loadtxt(pa.data_loc+'luminosity_functions/'+fname)
        L = data[:,0]
        flux = L / (4*np.pi*d_L*d_L)
        n = data[:,1]
        b1 = (flux > flux_th)
        n_obj = np.trapz(n[b1], x = np.log10(L[b1])) * dV
        #print(fname, n_obj)
        if fname == ranga_hi: nobj_hi = n_obj#la = 'Chary(High)'; c='r'; ls='-'
        if fname == ranga_lo: nobj_lo = n_obj#la = 'Chary(Low)'; c='r'; ls='--'

    return int(nobj_hi), int(nobj_lo)

if __name__ == "__main__":
    #out =  calc_nobj(A=1, R=500, t=96*60, zmid=10, dz=1, survey_area_sq_deg=30)

    area_list = [31.1, 31.1, 311, 31.1, 311]
    t_list = [125, 104, 14.2, 113, 12.8]
    for i in range(len(area_list)):

        out =  calc_nobj(A=0.83, R=300, t=t_list[i]*60, zmid=7, dz=1, survey_area_sq_deg=area_list[i])
        print(area_list[i], t_list[i])
        print(out)
        print('######')
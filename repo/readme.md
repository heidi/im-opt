# Intensity Mapping of Optical Lines

	code:
	/home/hywu/work/IM_opt/repo
	data:
	/panfs/ds08/hopkins/hywu/work/IM_opt/data

#### luminosity_functions

`halpha_luminosity_function.py`

* calculating luminosity function using Delta z = 2 bins (to get more objects)

`spherex_sensitivity.py`

* converting from Oliver's m_AB to line sensitivity
* using the 6-band design

`spherex_number_counts.py`

* integrating over the luminosity functions above the line sensitivity
 

#### pk_tools
`pk_3d_mastercode.py`

* cubic box
* regular 3d P(k) or P(k_perp, k_para)


`demo_pk_3d.py`

* using Bolshoi box to demonstrate the usage of the previous one

`pk_non_cubic_mastercode.py`

* non-cubic box, Lx != Ly != Lz
* calculating P(k_perp)

`demo_pk_non_cubic.py`

* demonstrate the previous code using a full BolshoiP box    

`demo_pk_non_cubic_half.py`

* demonstrate the same code using *half* of the BolshoiP box (Lz = Lx/2)


`demo_pk_non_cubic_half_long_voxel.py`

* using *elongated* voxel

`demo_pk_non_cubic_plots.py`

* master code for plotting anisotropic Pk

#### continuum_for_spherex



#### line_for_spherex
(generalized version of halpha/)

`line_lum.py`

* integrating the spectra around the lines
* generate line luminosity as a function of mass and redshift

`line_catalogs.py`

* write a catalog with ra, dec, redshift, line luminosities

`make_cube_lines.py`

* write the data cube from the catalog


#### pk_cube_continuum/

#### pk_cube_halpha/  (was called pk_cube)
* measuring p(k) from data cube

`survey_spec.py`
* for spherex and alpha specs

`spherex_lambda_bins.py`
* write 

`write_cube_halpha.py` 

* write data cube: read in datalogs and map them to (ra, dec, lambda) plane. one slice at a time
* 

`calc_pk_from_cube.py`


#### pk_lightcone/
* measuring p(k) from lightcone

#### pk_3d/

* measuring p(k) from a simulation box
 
`write_halpha_catalog_3d.py`

* write (px,py,pz)

`halpha_mean_S_z_3d`

* calculate the mean intensity from above


`pk_3d_matercode.py`

* 

`halo_pk_3d****.py`

* various ways for measuring p(k) of halos

`halo_pk_3d_plot.py`

* comparing various ways for measuring p(k) of halos


`halpha_pk_3d****.py`

* various ways for measuring p(k) of Halpha


`halpha_pk_3d_plot.py`

* comparing various ways for measuring p(k) of Halpha



#### halpha/ 
(superceeded by lines_for_spherex)

#### bethermin_sfr/


#### fsps/ [running on laptop]

`fsps_sfh.py`

* taking Peter Behroozi's SFH and run FSPS. 

`fsps_sfh_plot.py`


#### lightcone/ [running on zwicky]

(writing RA, dec, z, and tons of bands for the input for Cygrid)
(superceeded by )

`reorganize_sed_lib.py`

* write to sed_m_z_individual/
* make reading faster

`get_spectrum.py`


`create_fits_table.py`

* code from Daniel


`write_catalog_fits.py`

`write_catalog_fits_driver.py`

`test_catalog.py`


#### cygrid/ [running on laptop]

`catalog_to_grid.py`

`download_catalog.py`

`combine_grids.py`   

`cube_to_map.py`      

`grid_catalogue_from_Daniel.py`

`plot_spec.py`

`plot_grid.py`



#### cl/  
* failed attempts.  no long relevant

`oqe.py`

* from Eric. [ref](http://adsabs.harvard.edu/full/2003MNRAS.346..619P)

`power_fast.py`

* also from Eric. accelerated version

`oqe_1d.py`         
`oqe_learning.py` 
`oqe_2d.py`

* my unsuccessful attempts

`1d_ft_example.py`
`2d_ft_example.py`       

`pk_lightcone_halos.py`

* simple fft for Pk from halos

`pk_map.py`

* simple fft for Pk from halos


#### halpha/

`Halpha_spec_sanity.py`

* plotting the shape of the Halpha line, calculate the luminosity of H-alpha

`Halpha_lum.py`

* Halpha luminosity vs. SFR
        
`line_catalogs.py`  

* Halpha luminostiy function

`SFR_M_Behroozi.py`

* [can't remember]

#### masterpy/

* code from Paolo





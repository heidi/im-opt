#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import os, sys
from scipy.integrate import quad
from scipy.interpolate import interp1d
import IM_opt.repo.params as pa

import multiprocessing
nproc = 12
njob = 12

class LuminosityBandpass(object):
    def __init__(self, ia_mod_njob):
        self.ia_mod_njob = ia_mod_njob

        data_loc = pa.data_master_loc+'data/Behroozi_SFH_v2/'
        self.a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
        self.m_list = np.loadtxt(data_loc+'m_list.dat')

        na = len(self.a_list)
        ia_list = self.ia_mod_njob+njob*np.arange((na/njob+1))
        self.ia_list = ia_list.astype(int)


        wave0 = np.loadtxt(pa.data_master_loc+'sed_m_z/wavelength.dat') # rest frame
        freq_rest = (3.e+8)/(wave0*1.e-10)
        self.freq_rest = freq_rest[::-1]


        ids, lam_min_list, lam_max_list, lam_mid_list , delta_lam_list = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/spherex_lambda_bins.dat', unpack=True)

        self.lam_min_list = lam_min_list
        self.lam_max_list = lam_max_list
        self.nlam = len(lam_min_list)


    # def get_lum_band(self, lgM, z, lam_um_min, lam_um_max):
    #     a = 1./(1.+z)
    #     b0 = (abs(self.m_list - lgM) == min(abs(self.m_list - lgM)))
    #     im = np.arange(len(self.m_list))[b0][0]
    #     b0 = (abs(self.a_list - a) == min(abs(self.a_list - a)))
    #     ia = np.arange(len(self.a_list))[b0][0]

    def get_lum_band(self, im, ia, lam_um_min, lam_um_max):

        fname = pa.data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,im)
        #if os.path.exists(fname):
        spec = np.loadtxt(fname)
        a = self.a_list[ia] 
        z = 1./a - 1.
        #spec = spec0[b]
        #spec = spec - min(spec)
        spec = spec[::-1]
        freq_obs = self.freq_rest / (1+z)
        spec_interp = interp1d(freq_obs, spec)

        freq_min = (3.e+8)/(lam_um_max*1.e-6)
        freq_max = (3.e+8)/(lam_um_min*1.e-6)
        L = quad(spec_interp, freq_min, freq_max)[0]*pa.Lsun2ergs # luminosity in erg/s
        '''
        chi = pa.cosmo.comoving_distance(z).value
        DH = 3000. / pa.h # no h for S_halpha
        DA = chi/(1+z)
        DL = chi*(1+z)
        Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)
        lam_rest = np.sqrt(lam_um_min * lam_um_max)/(1+z) # TODO!
        nu = (3.e+8)/(lam_rest*1.e-6)
        dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
        S_nu = lum_band / (4*np.pi*DL**2) * (DA**2) * dchi_dnu * pa.Mpc2cm**-2 * pa.cgs2Jy
        # Jansky
        '''
        return L




    #def plot_sed():

    #import multiprocessing
    #nproc = 12job_id = int(sys.argv[1]) #171
    # need 15 jobs



    def write_lum_spherex(self, im_mod_nproc):
        nm = len(self.m_list)
        na = len(self.a_list)
        im_list = im_mod_nproc+nproc*np.arange((nm/nproc+1))
        im_list = im_list.astype(int)


        for ia in self.ia_list:
            #for im in range(len(self.m_list)):
            #a_list_calculated = np.array([])
            #m_list_calculated = np.array([])
            for im in im_list:

                if ia < na and im < nm:
                    a = self.a_list[ia]
                    z = 1./a - 1.
                    m = self.m_list[im]
                    print('a=%g, m=%g'%(a,m))

                    ifname = pa.data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,im)
                    ofname = pa.data_master_loc+'continuum_for_spherex/lum_a%.6f_m%.2f.dat'%(a,m)
                    if os.path.exists(ofname) == True:
                        print('file exists!')

                    if os.path.exists(ifname) == True and os.path.exists(ofname) == False:
                        #m_list_calculated = np.append(m_list_calculated, m)
                        #a_list_calculated = np.append(a_list_calculated, a)
                        spec = np.loadtxt(ifname)
                        lum_list = np.zeros(self.nlam)
                        for ilam in range(self.nlam):
                            lam_min = self.lam_min_list[ilam]
                            lam_max = self.lam_max_list[ilam]
                            lum = self.get_lum_band(im=im, ia=ia, lam_um_min=lam_min, lam_um_max=lam_max)
                            lum_list[ilam] = lum

                        # plt.figure()
                        # plt.plot(lam_mid_list, np.log10(lum_list))
                        # plt.show()
                        np.savetxt(ofname, np.log10(lum_list), fmt='%g')
                        
    def plot_lum_spherex():
        data = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/lum_a0.961056_m13.00.dat')
        plt.plot(data)
        plt.show()

if __name__ == "__main__":
    #write_lum_spherex()
    #plot_lum_spherex()
    #print(get_flux_band(lgM=12, z=0, lam_um_min=0.6560, lam_um_max=0.6564))
    ia_mod_njob = int(sys.argv[1])

    lb = LuminosityBandpass(ia_mod_njob=ia_mod_njob)
    pool = multiprocessing.Pool(processes=nproc)
    pool.map(lb.write_lum_spherex, range(nproc))
    pool.close()

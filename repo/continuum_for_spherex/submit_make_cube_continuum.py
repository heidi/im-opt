#!/usr/bin/env python
import numpy as np
import os, sys
import IM_opt.repo.params as pa

master_loc = os.getcwd()

# note! using job arrays
out_loc = pa.data_master_loc+'pbs_scripts/'
if os.path.exists(out_loc)==False: os.system('mkdir -p '+out_loc)
os.chdir(out_loc)

fname = out_loc+'make_cube.pbs'

f = open(fname, 'w')
f.write("""#!/bin/bash -l
#PBS -t 0-9
#PBS -l walltime=24:00:00
#PBS -m abe -M hywu@slac.stanford.edu
source /home/hywu/.bashrc
cd %(master_loc)s
%(master_loc)s/make_cube_continuum.py $PBS_ARRAYID > %(out_loc)s/lum_$PBS_ARRAYID.out & \n"""%vars())
f.write("""wait""")        
f.close()

# submit PBS file
cmd = 'qsub '+fname
os.system(cmd)
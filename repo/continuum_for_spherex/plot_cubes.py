#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
import IM_opt.repo.params as pa 


class CombineCubes(object):
    def __init__(self):
        pass

    def combine_cubes(self, ilam=0):
        data = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/spherex_lambda_bins.dat')
        lam_id = data[:,0]
        lam_min_list = data[:,1]
        lam_max_list = data[:,2]

        n_ra = 812
        data = np.zeros((n_ra, n_ra))
        for zmin in range(10):
            zmax = zmin + 1
            fname = pa.data_master_loc+'pk_cube/nouse/continuum_band_%i_zmin%g_zmax%g.dat'%(ilam, zmin, zmax) #TODO
            data += np.loadtxt(fname)*812*812 # TODO

        print('np.median(data)', np.median(data))
        print('np.max(data)', np.max(data))
        print('np.mean(data)', np.mean(data))
        #med = np.median(data)
        top1percent = sorted(data.flatten())[int(n_ra*n_ra*0.99)]
        print('top1percent', top1percent)

        for i in range(n_ra):
            for j in range(n_ra):
                if data[i,j] > top1percent:
                    data[i,j] = 0

        # print('np.shape(mask)', np.shape(mask))
        # print('np.shape(data)', np.shape(data))
        # print('np.shape(data[mask])', np.shape(data[mask]))

        p1 = plt.imshow(np.arctan(data))
        p1.axes.set_xticklabels([])
        p1.axes.set_yticklabels([])
        #plt.title(r"$Continuum,\ band\ %i,\ %.2f - %.2f \mu m$"%(ilam, lam_min_list[ilam], lam_max_list[ilam]))
        plt.title(r"$Continuum,\ band\ %i,\ %.2f - %.2f \mu m,$"%(ilam, lam_min_list[ilam], lam_max_list[ilam])+'\n'+r"$\rm mean=%g \ Jy/sr$"%(np.mean(data)))

        plt.savefig('../../plots/continuum_for_spherex/continuum_band_%i.png'%(ilam))
        #plt.show()

if __name__ == "__main__":
    cc = CombineCubes()
    for ilam in range(0,96,10):
        cc.combine_cubes(ilam=ilam)

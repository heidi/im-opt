#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys

import IM_opt.repo.params as pa 
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')

# import the mass and redshift of the sample
class MakeCubeContinuum(object):
    def __init__(self):
        pass

    def write_cube(self, zmin):
        data_loc = pa.data_master_loc+'data/Behroozi_SFH_v2/'
        a_sed_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
        m_sed_list = np.loadtxt(data_loc+'m_list.dat')


        l_survey = 1.4 # deg
        n_ra = 812 #int(l_ra/l_ra)
        l_ra = l_survey/n_ra #6.2 arcsecond, spherex
        n_dec = n_ra
        l_dec = l_ra
        n_lam = 96

        data_cube = np.zeros((n_ra, n_dec, n_lam))


        zmax = zmin + 1
        data = np.loadtxt('/panfs/ds08/hopkins/hywu/work/lightcone/Bethermin_v4_AbMatch/lightcone/BolshoiP_Bethermin_Mstar_z_%i_%i.dat'%(zmin,zmax))
        #ra[0], dec[1], z[2], M200[3,Msun], Mstar[4,Msun]
        ra_list = data[:,0]
        dec_list = data[:,1]
        z_list = data[:,2]
        lgM_list = np.log10(data[:,3]) # WHICH MASS????
        
        # ra_list = np.linspace(0,1.4, 10)
        # dec_list = np.linspace(0,1.4, 10)
        # z_list = np.linspace(0.01, 1, 10)
        # lgM_list = np.linspace(9, 14, 10)

        nh = len(ra_list)

        for ih in range(nh):
            i = min(n_ra-1, int(ra_list[ih]/l_ra))
            j = min(n_ra-1, int(dec_list[ih]/l_dec))
            z = z_list[ih]
            a = 1./(1.+z)
            lgM = lgM_list[ih]

            b0 = (abs(m_sed_list - lgM) == min(abs(m_sed_list - lgM)))
            im = np.arange(len(m_sed_list))[b0][0]
            b0 = (abs(a_sed_list - a) == min(abs(a_sed_list - a)))
            ia = np.arange(len(a_sed_list))[b0][0]
            a = a_sed_list[ia]
            m = m_sed_list[im]

            lum_band = 10**np.loadtxt(pa.data_master_loc+'continuum_for_spherex/lum_a%.6f_m%.2f.dat'%(a,m))
            data = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/spherex_lambda_bins.dat')
            lam_mid_list = data[:,3]

            chi = pa.cosmo.comoving_distance(z).value
            DH = 3000. / pa.h # no h for S_halpha
            DA = chi/(1+z)
            DL = chi*(1+z)
            Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)
            lam_rest = lam_mid_list/(1+z) # TODO! check!
            nu = (3.e+8)/(lam_rest*1.e-6) # TODO! check!
            dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz # TODO! check!
            S_nu = lum_band / (4*np.pi*DL**2) * (DA**2) * dchi_dnu * pa.Mpc2cm**-2 * pa.cgs2Jy
            # Jansky
            data_cube[i,j,:] += S_nu

        for ilam in range(n_lam):
            voxel_vol = (sim.VC_interp(zmax)-sim.VC_interp(zmin))*params.fsky/(n_ra**2)
            np.savetxt(pa.data_master_loc+'continuum_for_spherex/continuum_band_%i_zmin%g_zmax%g.dat'%(ilam, zmin, zmax), data_cube[:,:,ilam] / voxel_vol , fmt ='%g')

if __name__ == "__main__":
    zmin = int(sys.argv[1])
    mcc = MakeCubeContinuum()
    mcc.write_cube(zmin = zmin)

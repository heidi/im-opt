#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad
from scipy.interpolate import interp1d
import IM_opt.repo.params as pa
#import numpy.random as nr
# from astropy.cosmology import FlatLambdaCDM
# cosmo = FlatLambdaCDM(H0=67.8, Om0=0.307)

# data_master_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/'


data_loc = pa.data_master_loc+'data/Behroozi_SFH_v2/'
a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')

#print('len(a_list)', len(a_list))
#print('len(m_list)', len(m_list))

#wave = np.loadtxt('../../output/wavelength.dat')
#print(wave)






wave0 = np.loadtxt(pa.data_master_loc+'sed_m_z/wavelength.dat') # rest frame
freq_rest = (3.e+8)/(wave0*1.e-10)
freq_rest = freq_rest[::-1]

def get_lum_band(lgM, z, lam_um_min, lam_um_max):
    a = 1./(1.+z)
    b0 = (abs(m_list - lgM) == min(abs(m_list - lgM)))
    iM = np.arange(len(m_list))[b0][0]
    b0 = (abs(a_list - a) == min(abs(a_list - a)))
    ia = np.arange(len(a_list))[b0][0]

    spec = np.loadtxt(pa.data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,iM))
    #spec = spec0[b]
    #spec = spec - min(spec)
    spec = spec[::-1]
    freq_obs = freq_rest / (1+z)
    spec_interp = interp1d(freq_obs, spec)

    freq_min = (3.e+8)/(lam_um_max*1.e-6)
    freq_max = (3.e+8)/(lam_um_min*1.e-6)
    L_Ha = quad(spec_interp, freq_min, freq_max)[0]*pa.Lsun2ergs
    return L_Ha

ids, lam_min_list, lam_max_list, lam_mid_list , delta_lam_list = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/spherex_lambda_bins.dat', unpack=True)
nlam = len(lam_min_list)


#def plot_sed():

#import multiprocessing
#nproc = 12job_id = int(sys.argv[1]) #171
# need 15 jobs



def write_lum_spherex():
    for ia in range(job_id*ncores, (job_id+1)*ncores):#range(len(a_list)):
        a = a_list[ia]
        z = 1./a - 1.

        print('a=%g, z=%g'%(a,z))
        for im in range(len(m_list)):


            m = m_list[im]

            spec = np.loadtxt(pa.data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,im))
            #plt.figure()
            #plt.plot(wave0*1.e-4, np.log10(spec))
            #plt.xlim(None, 5)
            #plt.show()
            #exit()

            lum_list = np.zeros(nlam)
            for ilam in range(nlam):
                lam_min = lam_min_list[ilam]
                lam_max = lam_max_list[ilam]
                lum = get_lum_band(lgM=m, z=z, lam_um_min=lam_min, lam_um_max=lam_max)
                lum_list[ilam] = lum

            # plt.figure()
            # plt.plot(lam_mid_list, np.log10(lum_list))
            # plt.show()
            np.savetxt(pa.data_master_loc+'continuum_for_spherex/lum_a%.6f_m%.2f.dat'%(a,m), np.log10(lum_list), fmt='%g')

def plot_lum_spherex():
    data = np.loadtxt(pa.data_master_loc+'continuum_for_spherex/lum_a0.961056_m13.00.dat')
    plt.plot(data)
    plt.show()

if __name__ == "__main__":
    #write_lum_spherex()
    plot_lum_spherex()
    #print(get_lum_band(lgM=12, z=0, lam_um_min=0.6560, lam_um_max=0.6564))






'''
def plot_Kennicutt():
    from SFR_M_Behroozi import SFR_M_Behroozi
    sfrm = SFR_M_Behroozi()    
    for z in range(4):
        lgM_list = np.arange(9,14)
        nM = len(lgM_list)
        lgSFR_list = np.zeros(nM)
        lgLHa_list = np.zeros(nM)
        for i in range(nM):
            lgM = lgM_list[i]
            lgLHa_list[i] = np.log10(get_L_Halpha(lgM, z))
            lgSFR_list[i] = sfrm.get_lgSFR(lgM=lgM, z=z)
        plt.scatter(lgLHa_list, lgSFR_list)
    K_LHa = 10**41.27 # KennicuttEvans2012
    #K_LHa = 1.26e+41 # Kennicutt1998
    x = np.arange(36,45)
    plt.plot(x, x-np.log10(K_LHa), label=r"$\rm Kennicutt\ Evans 2012$")
    plt.xlabel(r"$log_{10} L_{H_\alpha} [erg\ s^{-1}]$")
    plt.ylabel(r"$log_{10} SFR [M_\odot\ yr^{-1}]$")
    plt.savefig('../../plots/Ha_Kennicutt.pdf')
    plt.show()

plot_Kennicutt()
exit()
def plot_L_Halpha(lgM, z):
    a = 1./(1.+z)
    b0 = (abs(m_list - lgM) == min(abs(m_list - lgM)))
    iM = np.arange(len(m_list))[b0][0]
    b0 = (abs(a_list - a) == min(abs(a_list - a)))
    ia = np.arange(len(a_list))[b0][0]

    spec0 = np.loadtxt(data_master_loc+'sed_m_z_individual/sed_lib_%i_%i.dat'%(ia,iM))
    spec = spec0[b]
    spec = spec - min(spec)
    spec = spec[::-1]
    spec_interp = interp1d(freq,spec)
    L_Ha = quad(spec_interp, min(freq), max(freq))[0]*Lsun2ergs
    
    print('L_Ha=', L_Ha, 'erg/s')
    plt.plot(wave0[b], spec0[b], label=r"$z=%i,\ L_{H_\alpha}= %.2e\ erg/s$"%(z, L_Ha))
    plt.legend()
    plt.xlabel(r"$\lambda [\AA]$")
    plt.ylabel(r"$L_\lambda [L_\odot/Hz]$")
    # note! SFR = L_Ha / 10**41.27 # KennicuttEvans12
    

def plot_Halpha_line():
    # make plots for line shape & strength
    for z in range(1,4):
        lgM = 12.5
        plot_L_Halpha(lgM=lgM, z=z)
    plt.title(r"$\log_{10}M = %g$"%(lgM))
    plt.savefig('../../plots/Halpha_lum_sanity.pdf')
    plt.show()


def save_Lalpha_z_lgM():
    outfile = open(data_master_loc+'lines/Lalpha_z_lgM.dat','w')
    outfile.write('#z, lgM[Msun], L_Ha[erg/s] \n') # TODO! change to lgLHa
    for a in a_list:
        for m in m_list:
            z = 1./a-1
            lgM = m
            La = get_L_Halpha(lgM=lgM, z=z)
            outfile.write('%g %g %g\n'%(z, lgM, La))
    outfile.close()



def get_Lalpha_z_lgM(z_wanted=0, lgM_wanted=12):
    data = np.loadtxt(data_master_loc+'lines/Lalpha_z_lgM.dat')
    z_list = np.unique(data[:,0])
    lgM_list = np.unique(data[:,1])
    nz = len(z_list)
    nlgM = len(lgM_list)
    La_list = data[:,2].reshape(nz,nlgM)
    #z_wanted = 0
    #lgM_wanted = 12
    b = (abs(lgM_list - lgM_wanted) == min(abs(lgM_list - lgM_wanted)))
    iM = np.arange(len(lgM_list))[b][0]
    b = (abs(z_list - z_wanted) == min(abs(z_list - z_wanted)))
    iz = np.arange(len(z_list))[b][0]
    La_wanted = La_list[iz, iM]
    #print('La_wanted', La_wanted)
    return La_wanted


def plot_Lalpha_z_lgM_interp():
    # interpolation
    data = np.loadtxt(data_master_loc+'lines/Lalpha_z_lgM.dat')
    z_list = np.unique(data[:,0])
    lgM_list = np.unique(data[:,1])
    nz = len(z_list)
    nlgM = len(lgM_list)
    La_list = data[:,2].reshape(nz,nlgM)
    from scipy.interpolate import RectBivariateSpline
    La_interp = RectBivariateSpline(z_list, lgM_list, La_list)

    for z_wanted in [0]:
        for lgM in np.arange(9,15):
            plt.scatter(lgM, np.log10(get_Lalpha_z_lgM(z_wanted, lgM)))
            plt.scatter(lgM, np.log10(La_interp(z_wanted, lgM)),c='r')
    plt.show()


if __name__ == "__main__":
    #save_Lalpha_z_lgM()
    #plot_Lalpha_z_lgM()
    pass

'''
#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

# note! these templates are for IR only...
#http://david.elbaz3.free.fr/astro_codes/chary_elbaz.html
filename = '../../chary_elbaz_codes/chary_elbaz.save'

import scipy.io
idl_dict=scipy.io.readsav(filename)
#print(idl_dict.keys())
#dict_keys(['nulnuinlsun', 'nulnu_iras25', 'nulnu_iras100', 'lir_sanders', 'nulnu_lw3', 'nulnu_lw2', 'nulnu_iras60', 'lambda', 'lir', 'nulnu_iras12'])


itemp = 0

lam = idl_dict['lambda'] # in micron
sed = idl_dict['nuLnuinLsun'][:,itemp]
print(max(sed),min(sed))

b = (lam < 1)
print(lam[b])

plt.subplot(111,xscale='log',yscale='log')
plt.plot(lam[b], sed[b])
plt.show()
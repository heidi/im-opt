#!/usr/bin/env python
import numpy as np


#http://www.sedfitting.org/SED08/Models.html
class Continuum(object):
    def __init__(self):
        pass

    def sed(self):
        data = np.loadtxt('sed_test.dat')
        #lam[A], log sed[ERG/SEC/A] 
        lam = data[:,0]
        sed = data[:,1]
        from scipy import interpolate
        sed_lam = interpolate.interp1d(lam,sed)


if __name__ == "__main__":
    con = Continuum()
    con.sed()
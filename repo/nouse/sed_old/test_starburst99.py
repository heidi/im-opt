#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np

fname = '../../starburst99_test/test.spectrum1'

data = np.loadtxt(fname, skiprows=6)
t = data[:,0]
lam = data[:,1]
sed = data[:,2]
t_list = np.unique(t)

plt.subplot(111,xscale='log')#,yscale='log')
for t_wanted in t_list:
    b = (t==t_wanted)
    plt.plot(lam[b], sed[b])

plt.figure()
plt.subplot(111,xscale='log')#,yscale='log')
b = (t==t_list[-1])
plt.plot(lam[b], sed[b])

b = (t==t_list[0]) 
plt.plot(lam[b], sed[b])
lam = lam[b]
sed = sed[b]

outfile = open('sed_test.dat','w')
outfile.write('#lam[A], log sed[ERG/SEC/A] \n')
for i in range(len(lam)):
    outfile.write('%g %g \n'%(lam[i], sed[i]))
outfile.close()

plt.show()
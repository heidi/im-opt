#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

from line_sfr import LineSFR
from continuum import Continuum
from astropy.cosmology import FlatLambdaCDM

from scipy import interpolate

cosmo = FlatLambdaCDM(H0=67.8, Om0=0.307)
san_loc = '/panfs/ds08/hopkins/hywu/work/H0_CV/mw_ds14a/sanity_dragan/'

red_list = np.linspace(0.,10,1000)
DL_list = cosmo.comoving_distance(red_list).value*(1+red_list)
from scipy.interpolate import interp1d
DL_z_interp = interp1d(red_list, DL_list)

Cosmo2Jansky = 4.e+7 # TODO

class SEDObserved(object):
    def __init__(self, z):
        self.z = z
        # luminosity distance
        self.DL = DL_z_interp(z)

    def calc_sed(self):
        # continuum
        data = np.loadtxt('sed_test.dat')
        #lam[A], log sed[ERG/SEC/A] 
        # need /A to /Hz
        lam_rest = data[:,0]
        lam_obs = lam_rest * (1+self.z)
        #nu_rest = (3.e+8)/(lam_rest*3.e-10) # Hz

        #lam_obs = lam_rest*(1+self.z)
        self.Slam = data[:,1]/(4*np.pi*self.DL**2)/(1+self.z)

        #outfile = open('','w')
        #Snu_Jy = Cosmo2Jansky * Slam * lam_rest / nu_rest 
        
        #lam_Jy = interpolate.interp1d(lam_obs,sed)
        #plt.subplot(111,xscale='log',yscale='log')
        #plt.plot(lam_obs, Slam)
        #plt.show()


if __name__ == "__main__":
    con = SEDObserved(z=0.1)
    
#!/usr/bin/env python
import numpy as np

# from pylab import *
# from math import *
# from numpy import *
# from scipy import *
# import os
# from scipy.interpolate import interp1d
# from constants import *
# import numpy.random as nr
# from SFR_Ms import SFR_Ms
# from sim import Sim

class LineSFR(object):
    def __init__(self):
        pass

    def lgL_Halpha(self, lgSFR): # erg/s
        # Kennicutt & Evans 12
        lgC = 41.27
        return lgSFR+lgC

    # many other lines...

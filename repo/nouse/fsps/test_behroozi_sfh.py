#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt


### newer data, all redshifts ###
data_loc = '../../data/Behroozi_SFH_v2/'
'''
# getting the mass list
data = np.loadtxt(data_loc+'sfh_a0.989282.dat')
m = np.unique(data[:,1])
outfile = open(data_loc+'m_list.dat','w')
outfile.write('#Mass in log10 Msun \n')
for i in range(len(m)):
    outfile.write('%.6f \n'%(m[i]))
exit()
'''

a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')

plt.subplot(yscale='log')
for ia in [0]:
    for iM in [40, 60, 80, 100, 120]: # [11, 12, 13, 14, 15]
        a_present = a_list[ia]
        m_present = m_list[iM]
        print('a=', a_present, 'm=', m_present)
        data = np.loadtxt(data_loc+'sfh_a%.6f.dat'%(a_present))
        b = (data[:,1] == m_present)        
        z = -1 + data[b,0]
        sfr = 10**data[b,2]
        plt.plot(z, sfr, label='%.2f'%(m_present)+', new data')

#plt.show()



#### older data, z = 0 only ####
lgM_list = [11.0, 12.0, 13.0, 14.0, 15.0]
for lgM in lgM_list:
    data = np.loadtxt('../../data/Behroozi_SFH/sfh/sfh_z0.1_corrected_%.1f.dat'%(lgM))
    #Scale factor, SFR, Err_Up, Err_Down (all linear units).
    a = data[:,0]
    sfr = data[:,1]
    er1 = data[:,2]
    er2 = data[:,3]
    z = 1./a-1

    plt.plot(z, sfr, label=str(lgM)+', old data')
plt.xlim(0,5)
plt.legend()
plt.show()



# def SFH_t_Behroozi(t):
#     A = 1
#     B = 1
#     C = 1
#     tau = 6
#     return A/((t/tau)**B + (t/tau)**-C)

# t = 10**np.arange(-1, np.log10(13),0.1)
# sfr = SFH_t_Behroozi(t)

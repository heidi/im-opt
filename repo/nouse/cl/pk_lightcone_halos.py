#!/usr/bin/env python
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt

L = 1.4 # size of the box
n = 512 # number of grid
l_cell = L/(n*1.) # size of the grid 

data_loc = '/panfs/ds08/hopkins/hywu/work/IM_opt/data/'
class PowerSpectrum2D(object):
    def __init__(self):
        pass

    def make_density_grid(self):
        fname = data_loc+'Mock_cat_Bethermin2017.fits'
        from astropy.io import fits
        d = fits.getdata(fname)
        ra = d['ra']
        dec = d['dec']
        z = d['redshift']
        b = (z > 5)&(z < 6)
        ra = ra[b]
        dec = dec[b]
        #print('ra', min(ra), max(ra))
        #print('dec', min(dec), max(dec))

        fx = np.zeros([n,n])
        for ip in range(len(ra)):
            i = int(np.floor(ra[ip]/l_cell))
            j = int(np.floor(dec[ip]/l_cell))
            fx[i,j] += 1

        #plt.imshow(fx)
        #plt.show()
        #np.savetxt('data/den_2d.dat', fx)
        self.fx = fx


    def fourier_transform(self):
        data = self.fx#np.loadtxt('data/den_2d.dat')
        mean_den = np.mean(data.flatten())
        den = data - mean_den
        fx = den

        # create the grid based on Python convention
        pi = np.pi        
        k = np.zeros([n,n])
        for i in range(n):
            for j in range(n):
                if i < n/2: ki = i*2*pi/L
                else: ki = (i-n)*2*pi/L
                if j < n/2: kj = j*2*pi/L
                else: kj = (j-n)*2*pi/L
                k[i,j] = np.sqrt(ki**2+kj**2)

        Fk = fft.fft2(fx)/(n**2) # Fourier coefficients (divided by n) 
        Fk = np.absolute(Fk)**2

        # summing over k-modes
        k = k.flatten()
        Fk = Fk.flatten()
        k = k[k>0]
        Fk = Fk[k>0]
        nk = 10
        kmin = 2*pi/L
        kmax = 2*pi/l_cell
        print('kmin, kmax', kmin, 0.5*kmax)
        lgk_list = np.linspace(np.log10(kmin), 0.5*np.log10(kmax), nk+1)
        Pk_list = np.zeros(nk)
        lgPk_list = np.zeros(nk)
        dPk_list = np.zeros(nk)
        for ik in range(nk):
            b = (np.log10(k) > lgk_list[ik])*(np.log10(k) <= lgk_list[ik+1])
            Pk_list[ik] = np.mean(Fk[b])
            print(10**lgk_list[ik+1], 'len(Fk[b])', len(Fk[b]), 'Pk=' , Pk_list[ik])
            #print(lgk_list[ik], Pk_list[ik])
            #lgPk_list[ik] = np.mean(np.log10(Fk[b]))
            #dPk_list[ik] = np.std(np.log10(Fk[b]))/np.sqrt(len(Fk[b])) #TODO
        #print(Pk_list)
        #print(lgk_list)
        plt.subplot(111,xscale='log',yscale='log')
        plt.plot(10**lgk_list[0:-1], Pk_list)#, dPk_list)
        #plt.savefig('')
        plt.show()

if __name__ == "__main__":
    ps2 = PowerSpectrum2D()
    ps2.make_density_grid()
    ps2.fourier_transform()
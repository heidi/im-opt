#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import time
import timeit
start = timeit.default_timer()
#import power_est as p_est
#import power_fast as pfast

'''
def get_dC_2d(kx, ky, x, y, parity = 'even'):
    if parity == 'even':
        dC = np.outer(np.cos(2*np.pi*kx*x+2*np.pi*ky*y).T , np.cos(2*np.pi*kx*x+2*np.pi*ky*y) ) 
    elif parity == 'odd':
        dC = np.outer(np.sin(2*np.pi*kx*x+2*np.pi*ky*y).T , np.sin(2*np.pi*kx*x+2*np.pi*ky*y) )
    return dC
'''

#def get_dC_2d(k_2d, r_2d_list):

def demo_dC():
    k_2d = np.array([1,0.1]) # 1x2
    ngrid = 8
    x_grid = np.arange(ngrid)
    y_grid = np.arange(ngrid)
    for i in range(ngrid):
        for j in range(ngrid):
            if i==j==0: r_2d_list = np.array([[x_grid[0], y_grid[0]]])
            else: r_2d_list = np.append(r_2d_list, np.array([[x_grid[i], y_grid[j]]]), axis=0)
    print(r_2d_list) # 64 x 2
    ndata = np.shape(r_2d_list)[0] 
    dC = np.zeros([ndata, ndata])
    for i in range(ndata):
        for j in range(ndata):
            dC[i,j] = np.exp(1j*2*np.pi*(k_2d[0]*r_2d_list[i,0]+k_2d[1]*r_2d_list[i,1]-k_2d[0]*r_2d_list[j,0]-k_2d[1]*r_2d_list[j,1]))
    print(dC)
    plt.imshow(dC)
    plt.show()


def get_dC_2d(k_2d, r_2d_list):
    ndata = np.shape(r_2d_list)[0]
    dC = np.zeros([ndata, ndata])
    for i in range(ndata):
        for j in range(ndata):
            dC[i,j] = np.exp(1j*2*np.pi*(k_2d[0]*r_2d_list[i,0]+k_2d[1]*r_2d_list[i,1]-k_2d[0]*r_2d_list[j,0]-k_2d[1]*r_2d_list[j,1]))
    return dC

def get_covar_from_power_spec_2d(pk_list, k_2d_list, r_2d_list):
    ndata = np.shape(r_2d_list)[0]
    nk = np.shape(pk_list)[0]
    covar = np.zeros((ndata, ndata))
    for ik in range(nk):
        dC = get_dC_2d(k_2d_list[ik], r_2d_list)
        covar = covar + pk_list[ik] * dC
    return covar

def generate_fake_data_2d():
    # real space
    ngrid = 8
    x_grid = np.arange(ngrid)
    y_grid = np.arange(ngrid)
    for i in range(ngrid):
        for j in range(ngrid):
            if i==j==0: r_2d_list = np.array([[x_grid[0], y_grid[0]]])
            else: r_2d_list = np.append(r_2d_list, np.array([[x_grid[i], y_grid[j]]]), axis=0)

    ngrid = 8
    image = np.random.randn(ngrid, ngrid) # noise in x space
    image_k = np.fft.fft2(image)        # noise in k space
    kx = np.fft.fftfreq(ngrid)
    ky = kx
    kx_grid = 0.1*np.arange(ngrid)
    ky_grid = 0.1*np.arange(ngrid)
    for i in range(ngrid):
        for j in range(ngrid):
            if i==j==0: k_2d_list = np.array([[kx_grid[0], ky_grid[0]]])
            else: k_2d_list = np.append(k_2d_list, np.array([[kx_grid[i], ky_grid[j]]]), axis=0)

    k0 = 0.01
    pk_2d = np.zeros([ngrid, ngrid])

    for i in range(ngrid):
        for j in range(ngrid):
            kk = np.sqrt(kx[i]**2+ky[j]**2)
            pk_2d[i,j] = 1./(2*np.pi*k0) * np.exp(-(kk/0.05)**2/4.)
    amp = np.sqrt(pk_2d)
    image_k_pk = image_k * amp 
    image_2d = np.real(np.fft.ifft2(image_k_pk) ) # S+N in x space




    return r_2d_list, image_2d, k_2d_list, pk_2d

def main(argv):
    # the grid of r

    # 
    # ngrid = 64#256
    # x = np.arange(ngrid)
    # y = x
    #kx = np.fft.fftfreq(ngrid)
    #k = k[k >= 0]
    # r_2d_list, image_2d, k_2d_list, pk_model_2d = generate_fake_data_2d()


    # plt.figure()
    # plt.imshow(image_2d)
    # plt.colorbar()
    # plt.title('imput image')

    # plt.figure()
    # plt.imshow(pk_model_2d)
    # plt.colorbar()
    # plt.title('imput pk')


    #print('np.shape(data)', np.shape(data))
    #print('np.shape(k_2d_list)', np.shape(k_2d_list))
    #print('np.shape(pk_model_2d)', np.shape(pk_model_2d))
    #exit()
    # plt.imshow(pk_model_2d)    
    # plt.colorbar()
    # plt.show()
    # exit()
    # # now, censor the data!
    # lo = ngrid/4
    # hi = ngrid/2
    # keep = (np.arange(ngrid) < lo) | (np.arange(ngrid) > hi) 
    # x = x[keep]
    # data = data[keep]
    # ngrid = np.sum(keep)
    
    # kk_use = kk>0
    # pk_list = pk_model_2d.flatten()
    # nk = len(pk_list)
    # image_list = image_2d.flatten()
    # ndata = len(image_list)
    # print('nk',nk)
    # print('ndata',ndata)
    # exit()
    #S = get_covar_from_power_spec_2d(pk_list, k_2d_list, r_2d_list)
    # print(S)
    # plt.imshow(S)    
    # plt.colorbar()
    # plt.show()
    # exit()    
    #S = getCovarFromPowerSpec(pk_model[kk_use], kk[kk_use], x)#TODO: what if I don't have the model for pk?
    
    #S = get_covar_from_power_spec_2d(pk_model_2d, kx, ky, x, y)
    #noise = 0.0001
    #N = np.diag(noise + np.zeros(ndata))
    #C = S + N
    #data = np.random.multivariate_normal(np.zeros([ndata]), C, size=1)[0,:] # TODO

    pi = np.pi
    def f_2d(x, y):
        #sigma = 0.5#0.01
        #return np.exp(-0.5*(x*x+y*y)/sigma**2)
        k0 = 1000
        r = np.sqrt(x*x+y*y)
        return np.sin(k0*x)

    L = 1.
    ngrid = 8
    lgrid = L/ngrid
    f_data_2d = np.zeros((ngrid, ngrid))
    for i in range(ngrid):
        for j in range(ngrid):
            x = i*lgrid
            y = j*lgrid
            f_data_2d[i,j] = f_2d(x,y)

    '''
    x_grid = np.arange(ngrid)
    y_grid = np.arange(ngrid)
    for i in range(ngrid):
        for j in range(ngrid):
            if i==j==0: r_2d_list = np.array([[x_grid[0], y_grid[0]]])
            else: r_2d_list = np.append(r_2d_list, np.array([[x_grid[i], y_grid[j]]]), axis=0)
    '''
    x_grid = np.arange(ngrid)
    y_grid = np.arange(ngrid)
    ndata = ngrid**2
    r_2d_list = np.zeros((ndata, 2))
    for i in range(ngrid):
        for j in range(ngrid):
            k = i*ngrid + j
            r_2d_list[k,0] = x_grid[i]
            r_2d_list[k,1] = y_grid[j]



    kx_grid = np.append(np.arange(ngrid/2), np.arange(-ngrid/2,0)) * (2*pi)/L
    ky_grid = kx_grid
    '''
    for i in range(ngrid):
        for j in range(ngrid):
            if i==j==0: k_2d_list = np.array([[kx_grid[0], ky_grid[0]]])
            else: k_2d_list = np.append(k_2d_list, np.array([[kx_grid[i], ky_grid[j]]]), axis=0)
    '''
    k_2d_list = np.zeros((ndata, 2))
    for i in range(ngrid):
        for j in range(ngrid):
            k = i*ngrid + j
            k_2d_list[k,0] = kx_grid[i]
            k_2d_list[k,1] = ky_grid[j]

    stop = timeit.default_timer()
    print('took', stop - start, 'seconds to construct r and k')
    restart = stop


    data = f_data_2d.flatten()
    data = data - np.mean(data)
    #print('np.shape(data)',np.shape(data))
    ndata = ngrid*ngrid
    S = np.identity(ndata)
    noise = 0.0001
    N = np.diag(noise + np.zeros(ndata))
    # plt.plot(data)
    # plt.show()
    # exit()

    Cinv = np.linalg.inv(S+N)
    Ninv = np.linalg.inv(N)
    #Cinv = Ninv
    
    nk = ndata
    qk = np.zeros(nk)
    pk = np.zeros(nk)
    Fisher = np.zeros(nk)
    mode_time = 0.
    dC_time = 0.
    Q_time = 0.

    dC_matrix = np.zeros( (nk, ndata, ndata) )
    for i in range(nk):
        #t0 = time.time()
        #dC = get_dC_2d(k[i],x)
        dC = get_dC_2d(k_2d=k_2d_list[i], r_2d_list=r_2d_list)
        dC_matrix[i,:,:] = dC
        #t1 = time.time()
        Q = Cinv.T.dot(dC).dot(Cinv)
        #t2 = time.time()
        mode = (data.T.dot(Q)*data.T).sum() - np.trace(np.dot(Q, N) )
        #t3 = time.time()
        qk[i] = mode
        # stop = timeit.default_timer()
        # print('took', stop - restart, 'seconds to get dC')
        # restart = stop


    # Get the Fisher matrix.
    Fisher = np.zeros( (nk, nk) )
    print("calculating the Fisher matrix...")
    for i in range(nk):
        print("Fisher matrix row:",i," of ",nk)
        for j in range(i,nk):
            dCi = dC_matrix[i,:,:]
            dCj = dC_matrix[j,:,:]
            Fisher[i,j] = 1./2.*np.trace( np.dot( np.dot( Cinv, dCi), np.dot( Cinv, dCj) ) )
            #x1 = Cinv.dot(dCi)
            #x2 = Cinv.dot(dCj)
            #Fisher[i,j] = 0.5*np.einsum( 'ij,ji',x1,x2)
            Fisher[j,i] = Fisher[i,j]


    print('Fisher', Fisher)
    plt.imshow(Fisher)
    plt.colorbar()
    plt.show()
    #exit()
    # Estimate the power spectrum mode amplitudes.
    Finv = np.linalg.inv(Fisher)
    pk = np.dot(Finv, qk)
    print(pk)
    print('np.shape(pk)', np.shape(pk))
    #ngrid = 8
    plt.figure()
    plt.imshow(pk.reshape(ngrid,ngrid))
    plt.colorbar()
    plt.title('recover')
    plt.show()
    exit()
    #print(pk)
    #exit()
    
    pkest = np.abs( np.fft.fft( data ) )**2/ngrid/ngrid
    k_est = np.fft.fftfreq(ngrid)
    k_est_use = k_est > 0
    print("done with normal p(k) estimate, now trying accelerated version.")
    #pkfast, Ffast = pfast.power(t=x,k = kk[k_est_use], data = data, err = noise + np.zeros(data.size) )
    
    fig,(ax1,ax2) = plt.subplots(nrows=1,ncols=2,figsize=(14,7) )
    ax1.plot(k,pk,'s',color='blue')
    ax1.plot(kk[k_est_use], pkest[k_est_use],'.',color='red')
    ax1.plot(kk,pk_model,'--',color='green')
    #ax1.plot(kk[k_est_use],pkfast,'+',color='cyan')
    ax1.set_yscale('log')
    ax1.set_ylim(1e-10,10)
    ax1.set_xlim(0,0.6)
    ax1.set_ylabel('P(k)')
    ax1.set_xlabel('k')
    
    ax2.plot(x,data,'.')
    ax2.set_xlabel('time (days)')
    ax2.set_ylabel('magnitude')
    ax2.axvspan(lo,hi,color='grey',alpha=0.15)
    fig.savefig('power_oqe_window_example.pdf',format='pdf')
    #fig.show()
    fig.clf()
    #stop
    
if __name__ == "__main__":

    
    import sys
    main(sys.argv)

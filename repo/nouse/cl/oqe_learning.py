#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

npix = 256
noise_x = np.random.randn(npix)
noise_k = np.fft.fft(noise_x)
kk = np.fft.fftfreq(npix)

sig = 0.1
signal_k = 1./np.sqrt(2*np.pi*sig*sig) * np.exp(-kk*kk/(2*sig*sig))
signal_k = np.sqrt(signal_k)
signal_x = np.real(np.fft.ifft(signal_k))

signal_noise_k = signal_k * noise_k #why multiply, not sum?
signal_noise_x = np.real(np.fft.ifft(signal_noise_k) )

# plot the data in Fourier space
plt.plot()
b = np.argsort(kk)
plt.plot(kk[b], noise_k[b], label='noise in k') 
plt.plot(kk[b], signal_k[b], label='signal in k')
plt.plot(kk[b], signal_noise_k[b], label='signal+noise in k')
plt.legend()


# plot the data in config space
plt.legend()
plt.figure()
plt.plot(noise_x, label='noise in real')
plt.plot(signal_x, label='signal in real')
plt.plot(signal_noise_x, c='r', label='signal+noise in real')
plt.legend()
plt.show()

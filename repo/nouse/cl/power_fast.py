#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import linalg as slg

def get_dC(k, x):
    dC_even = np.outer(np.cos(2*np.pi*k*x).T , np.cos(2*np.pi*k*x) ) 
    dC_odd  = np.outer(np.sin(2*np.pi*k*x).T , np.sin(2*np.pi*k*x) )
    return dC_even + dC_odd

def getCovarFromPowerSpec(pk, k, x):
    #x = np.arange(npix)
    npix = x.size
    covar = np.zeros( (npix, npix) )
    for ik in xrange(k.size):
        dC = get_dC(k[ik], x)
        covar = covar + pk[ik] * dC
    return covar

def makeOrtho(ngrid):
    # Construct the trivial orthonormal basis:
    I = np.eye(ngrid)
    p = np.random.permutation(np.arange(ngrid))
    return I[p]

def rapidTrace2(A, B, n_iter = 100, tol = 1e-8):
    # Compute rapid trace of product of large matrices:
    # tr (A^-1 dot B)
    # get the bases.
    II = makeOrtho(A.shape[0])
    thisTol = 1.0
    i = 0
    tr = .0
    n_max = np.min((n_iter, A.shape[0]))
    tr_arr = np.zeros(n_max)    
    while (i < n_max) & (np.abs(thisTol) > tol):
        W,status = slg.cg(A,II[:,i])
        X = B.dot(II[:,i])
        tr_arr[i] = W.dot(X)
        if (i>0):
            tr_old = tr
            tr = np.mean(tr_arr[:i])*A.shape[0]
            thisTol = tr/tr_old - 1.
            #print i, thisTol,tr_old, tr
        else:
            thisTol = 1.0
        i=i+1
    return tr

def fastProd4(Ainv, C_1,Binv,C_2, x1,x2):
    u,s1 = slg.cg(Ainv,x1)
    y = C_2.dot(x2)
    v,s2 = slg.cg(Binv,y)
    #print s1,s2
    return u.T.dot(C_1).dot(v)

def rapidTrace4(Ainv, C_1, Binv, C_2, n_iter = 1000, tol = 1e-6):
    # Compute rapid trace of product of large matrices:
    # tr (A^-1 dot B)
    # get the bases.
    II = makeOrtho(Ainv.shape[0])
    thisTol = 1.0
    i = 0
    tr = 0.
    n_max = np.min((n_iter,Ainv.shape[0]))
    tr_arr = np.zeros(n_max)
    while (i < n_max) & (np.abs(thisTol) > tol):
        tr_arr[i] = fastProd4(Ainv, C_1, Binv, C_2, II[:,i], II[:,i])
        if i > 0:
            tr_old = tr
            tr = np.mean(tr_arr[:i])*Ainv.shape[0]
            thisTol = tr/tr_old - 1.
            #print i, thisTol,tr_old, tr
        else:
            thistol = 1.0
        i = i+1
    return tr


def power(t = None,data = None, err = None,k = None, p_init = None, signal_covariance = None):
    # Center the data.
    data = data - np.mean(data)
    
    # If k values are not supplied, make some up.
    if k == None:
        dx = np.min(t[1:] - t[0:-1])
        nx = np.ceil( (np.max(t) -  np.min(t)) * (1./dx) )
        k = np.fft.fftfreq(nx.astype('int'))
        k = k[k > 0]

    print "building model covariance..."
    if p_init == None:
        N = np.diag(err**2)
        if signal_covariance != None:
            S = signal_covariance
            Winv = S + N
            WD,_ = slg.cg(Winv, data)
        else:
            W = np.diag(err**2)
            Winv = np.diag(1./err**2)
            WD = W.dot(data)
    else:
        N = np.diag(err**2)
        S = getCovarFromPowerSpec(p_init, k, t)
        Winv = S + N
        WD,_ = slg.cg(Winv,data)

    # Build the dC covariance deriv. for each bandpower.
    dC_matrix = np.zeros ( (k.size, t.size, t.size) )
    qk = np.zeros( k.size )
    for i in xrange(k.size):
        print "mode ",i," of ",k.size-1
        dC = get_dC(k[i],t)
        dC_matrix[i, :,: ] = dC
        Q = WD.T.dot(dC).dot(WD)
        tr = rapidTrace2(Winv,N)
        qk[i] = Q.sum() - tr
    
    # Measure the Fisher matrix.
    Fisher = np.zeros( ( k.size, k.size) )
    print "calculating the Fisher matrix..."
    for i in xrange(k.size):
        print "row ",i," of ",k.size-1
        for j in xrange(i,k.size):
            dCi = dC_matrix[i,:,:]
            dCj = dC_matrix[j,:,:]
            Fisher[i,j] = 1./2 * rapidTrace4(Winv, dCi, Winv, dCj, n_iter = 20)
            #x1 = Cinv.dot(dCi)
            #x2 = Cinv.dot(dCj)
            #Fisher[i,j] = 0.5*np.einsum( 'ij,ji',x1,x2)
            Fisher[j,i] = Fisher[i,j]


    Finv = np.linalg.inv(Fisher)
    pk = np.dot(Finv, qk)
    return pk, Finv

#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
pi = np.pi

sigma = 0.5#0.01


def f_2d(x, y):
    #return np.exp(-0.5*(x*x+y*y)/sigma**2)
    k0 = 1000
    r = np.sqrt(x*x+y*y)
    return np.sin(k0*x) #+ np.sin(2*k0*x)

L = 1.
ngrid = 8#16#128
lgrid = L/ngrid
f_data_2d = np.zeros((ngrid, ngrid))
for i in range(ngrid):
    for j in range(ngrid):
        x = i*lgrid
        y = j*lgrid
        f_data_2d[i,j] = f_2d(x,y)

plt.imshow(f_data_2d[0:10,0:10])
plt.colorbar()
#plt.show()


plt.figure()

f_k_2d = np.fft.fft2(f_data_2d)
pk_2d = np.dot(f_k_2d, np.conjugate(f_k_2d)).real
print(pk_2d)

plt.imshow(pk_2d)
plt.colorbar()
plt.title('Fourier space')
plt.show()
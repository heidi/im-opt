#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import time
#import power_est as p_est
#import power_fast as pfast

def get_dC(k, x,parity = 'even'): # x is coordinate
    if parity == 'even':
        dC = np.outer(np.cos(2*np.pi*k*x).T , np.cos(2*np.pi*k*x) ) 
    elif parity == 'odd':
        dC = np.outer(np.sin(2*np.pi*k*x).T , np.sin(2*np.pi*k*x) )
    return dC
    
def getCovarFromPowerSpec(pk, k, x): # x is coordinate
    #x = np.arange(npix)
    npix = x.size
    covar = np.zeros( (npix, npix) )
    for ik in range(k.size):
        dC_even = get_dC(k[ik], x, parity = 'even')
        dC_odd = get_dC(k[ik], x, parity= 'odd')
        #print('dC', dC_even+dC_odd)
        covar = covar + pk[ik] * dC_even + pk[ik] * dC_odd
    return covar



def generate_fake_series(npix):
    image = np.random.randn(npix) # noise in x space
    image_k = np.fft.fft(image)   # noise in k space
    kk = np.fft.fftfreq(npix)
    k0 = 0.01
    pk = 1./(2*np.pi*k0) * np.exp(-(kk/0.05)**2/4.)
    amp = np.sqrt(pk)
    image_k_pk = image_k * amp 
    image_pk = np.real(np.fft.ifft(image_k_pk) ) # S+N in x space
    return image_pk, kk, pk

def main(argv):
    noise = 0.0001
    ngrid = 64#256
    x = np.arange(ngrid)
    k = np.fft.fftfreq(ngrid)
    k = k[k >= 0]
    data, kk, pk_model = generate_fake_series(ngrid)
    #plt.plot(kk, pk_model)
    #plt.show()
    #exit()
    '''
    # now, censor the data!
    lo = ngrid/4
    hi = ngrid/2
    keep = (np.arange(ngrid) < lo) | (np.arange(ngrid) > hi) 
    x = x[keep]
    data = data[keep]
    ngrid = np.sum(keep)
    
    kk_use = kk>0
    S = getCovarFromPowerSpec(pk_model[kk_use], kk[kk_use], x)#TODO: what if I don't have the model for pk?
    '''
    S = getCovarFromPowerSpec(pk_model, kk, x)
    N = np.diag(noise + np.zeros(ngrid))

    C = S + N
    data = np.random.multivariate_normal(np.zeros(ngrid), C, size=1)[0,:]

    data = data - np.mean(data)# fake data set from which to estimate pk

    plt.plot(x,data)
    plt.show()
    exit()

    
    Cinv = np.linalg.inv(S+N)
    Ninv = np.linalg.inv(N)
    #Cinv = Ninv
    
    qk = np.zeros(k.size)
    pk = np.zeros(k.size)
    Fisher = np.zeros(k.size)
    mode_time = 0.
    dC_time = 0.
    Q_time = 0.

    dC_matrix = np.zeros( (k.size, x.size, x.size) )
    
    for i in range(k.size):
        t0 = time.time()
        dC = get_dC(k[i],x)
        dC_matrix[i,:,:] = dC
        t1 = time.time()
        Q = Cinv.T.dot(dC).dot(Cinv)
        t2 = time.time()
        mode = (data.T.dot(Q)*data.T).sum() - np.trace(np.dot(Q, N) )
        t3 = time.time()
        qk[i] = mode
    # Get the Fisher matrix.
    Fisher = np.zeros( (k.size, k.size) )
    print("calculating the Fisher matrix...")
    for i in range(k.size):
        print("Fisher matrix row:",i," of ",k.size)
        for j in range(i,k.size):
            dCi = dC_matrix[i,:,:]
            dCj = dC_matrix[j,:,:]
            Fisher[i,j] = 1./2.*np.trace( np.dot( np.dot( Cinv, dCi), np.dot( Cinv, dCj) ) )
            #x1 = Cinv.dot(dCi)
            #x2 = Cinv.dot(dCj)
            #Fisher[i,j] = 0.5*np.einsum( 'ij,ji',x1,x2)
            Fisher[j,i] = Fisher[i,j]

    # Estimate the power spectrum mode amplitudes.
    Finv = np.linalg.inv(Fisher)
    pk = np.dot(Finv, qk)

    
    pkest = np.abs( np.fft.fft( data ) )**2/ngrid/ngrid
    k_est = np.fft.fftfreq(ngrid)
    k_est_use = k_est > 0
    print("done with normal p(k) estimate, now trying accelerated version.")
    #pkfast, Ffast = pfast.power(t=x,k = kk[k_est_use], data = data, err = noise + np.zeros(data.size) )
    
    fig,(ax1,ax2) = plt.subplots(nrows=1,ncols=2,figsize=(14,7) )
    ax1.plot(k,pk,'s',color='blue')
    ax1.plot(kk[k_est_use], pkest[k_est_use],'.',color='red')
    ax1.plot(kk,pk_model,'--',color='green')
    #ax1.plot(kk[k_est_use],pkfast,'+',color='cyan')
    ax1.set_yscale('log')
    ax1.set_ylim(1e-10,10)
    ax1.set_xlim(0,0.6)
    ax1.set_ylabel('P(k)')
    ax1.set_xlabel('k')
    
    ax2.plot(x,data,'.')
    ax2.set_xlabel('time (days)')
    ax2.set_ylabel('magnitude')
    ax2.axvspan(lo,hi,color='grey',alpha=0.15)
    fig.savefig('power_oqe_window_example.pdf',format='pdf')
    #fig.show()
    fig.clf()
    #stop
    
if __name__ == "__main__":
    '''
    import pdb, traceback,sys
    try:
        main(sys.argv)
    except:
        thingtype, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)
    '''
    import sys
    main(sys.argv)

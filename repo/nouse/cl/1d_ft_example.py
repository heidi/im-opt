#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
pi = np.pi

sigma = 0.01

def f_1d(x):
    k0 = 1000
    return np.sin(k0*x) #+ np.sin(2*k0*x)#np.exp(-0.5*(x*x)/sigma**2)

L = 1.
ngrid = 8#128
lgrid = L/ngrid
f_data_1d = np.zeros(ngrid)
x_list = np.zeros(ngrid)
for i in range(ngrid):
    x = i*lgrid
    x_list[i] = x
    f_data_1d[i] = f_1d(x)

plt.figure()
plt.subplot(211)
plt.title('real pace')
plt.plot(x_list, f_data_1d)
#plt.plot(x_list*0 + sigma, f_data_1d)
#plt.xlim(0,0.1)


f_k_1d = np.fft.fft(f_data_1d).real
k = np.append(np.arange(ngrid/2), np.arange(-ngrid/2,0)) * (2*pi)/L
# don't use fft.freq! confusing...
plt.subplot(212)
plt.title('Fourier space')
plt.scatter(k, f_k_1d)
#plt.plot(k*0 + 1./sigma, f_k_1d)
plt.show()
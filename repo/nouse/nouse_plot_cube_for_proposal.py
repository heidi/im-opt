#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import sys
sys.path.append('../')
import params as pa


l_survey = 1.4 # deg
n_ra = 812 #int(l_ra/l_ra)
l_ra = l_survey/n_ra #6.2 arcsecond, spherex
n_dec = n_ra
l_dec = l_ra

n_lam = 96
data = np.loadtxt('data/spherex_lambda_bins.dat')
lam_id = data[:,0]
lam_min_list = data[:,1]
lam_max_list = data[:,2]

'''
# drawing the grid #
for i in range(n_ra):
    for j in range(n_dec):
        for k in range(n_lam):
            ra_min = i*l_ra
            ra_max = (i+1)*l_ra
            dec_min = j*l_dec
            dec_max = (j+1)*l_dec
            lam_min = lam_min_list[k]
            lam_max = lam_max_list[k+1]
'''

zmin = 5
zmax = 6

lam_rest = 0.6563 # halpha

for ilam in range(70,81):#range(n_lam):
    plt.figure()
    data = np.loadtxt(pa.data_master_loc+'pk_cube/halpha_lam_%i.dat'%(ilam))
    p1 = plt.imshow(np.arctan(data))
    #plt.colorbar()
    p1.axes.set_xticklabels([])
    p1.axes.set_yticklabels([])
    plt.title(r"$H\alpha,\ band\ %i,\ %.2f - %.2f \mu m$"%(ilam, lam_min_list[ilam], lam_max_list[ilam]))
    plt.savefig(pa.data_master_loc+'pk_cube_images/halpha_lam_%i.png'%(ilam))

plt.show()

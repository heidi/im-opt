#!/usr/bin/env python
#from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
from astropy.coordinates import cartesian_to_spherical
import sys
sys.path.append('../')
#sys.path.append('../pk_tools/')
import params as pa
from pk_non_cubic_mastercode import PowerSpectrumNonCubic
#import survey_spec as ss

"""
read in Bolshoi "half" box
plotting P(k_perp, k_para)
"""

name = 'halo_non_cubic_half'
cat_name = pa.data_master_loc+'pk_tools/%s.dat'%(name)

Lx = 250
Ly = 250
Lz = 125

nx = 128
ny = 128
nz = 64

lx = Lx/nx
ly = Ly/ny
lz = Lz/nz
cell_vol = lx*ly*lz

cat_name_2 = pa.data_master_loc+'pk_tools/half_ifile.dat'
"""
cat written by demo_pk_non_cubic.py
"""

def calc_halpha_pk():
    ifname = cat_name_2
    ofname = pa.data_master_loc+'pk_tools/%s'%(name)
    psnc = PowerSpectrumNonCubic(ifname=ifname, ofname=ofname, nx=nx, ny=ny, nz=nz, Lx=Lx, Ly=Ly, Lz=Lz, use_weight=True, save_grid=False, divide_by_mean=True, use_window=False)
    psnc.make_halo_density_grid()
    psnc.fourier_transform(anisotropic=False)
    psnc.fourier_transform(anisotropic=True)

""" # moved to demo_pk_non_cubic_plots.py
def plot_halpha_pk():

    plt.figure(figsize=(14,14))
    # P(k)
    plt.subplot(221)
    fname = pa.data_master_loc+'pk_tools/%s_pk_non_cubic.dat'%(name)
    k, pk = np.loadtxt(fname, unpack=True)
    plt.plot(k, pk*k**3/(2.*np.pi**2))

    k, pk = np.loadtxt(pa.data_master_loc+'pk_tools/BolshoiP_matterpower.dat', unpack=True)
    b = (k>0.01)*(k < 10)
    plt.plot(k[b], pk[b]*k[b]**3/(2.*np.pi**2), label='CAMB')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k \ [h/Mpc]$')
    plt.ylabel(r'$\rm k^3P(k)/(2\pi^2)$')  
    
    plt.subplot(222)
    # P(k_perp, k_para)
    fname = pa.data_master_loc+'pk_tools/%s_pk_perp_para_non_cubic.dat'%(name)
    k_perp, k_para, pk = np.loadtxt(fname, unpack=True)
    k_cubed = np.sqrt(k_perp**2 + k_para**2)**3
    DeltaSqr = pk * k_cubed/(2*np.pi**2)

    k_perp = np.unique(k_perp)
    k_para = np.unique(k_para)
    nk_perp = len(k_perp)
    nk_para = len(k_para)
    pk = pk.reshape([nk_perp, nk_para])
    DeltaSqr = DeltaSqr.reshape([nk_perp, nk_para])
    plt.imshow(np.log10(DeltaSqr.transpose()), origin='lower', aspect='equal', extent=(0,1,0,1), interpolation='nearest')
    
    plt.xlabel(r'$\rm k_\perp \ [h/Mpc]$')
    plt.ylabel(r'$\rm k_\parallel \ [h/Mpc]$')
    plt.title(r'$\rm log_{10} k^3 P(k_\perp, k_\parallel)/(2\pi^2)$')

    xtick_lbls = ['{:.2f}'.format(i) for i in k_perp]
    xtick_locs = (0.5+np.arange(nk_perp))/nk_perp
    plt.yticks(xtick_locs[0:-1:3], xtick_lbls[0:-1:3])

    ytick_lbls = ['{:.2f}'.format(i) for i in k_para]
    ytick_locs = (0.5+np.arange(nk_para))/nk_para
    plt.xticks(ytick_locs[0:-1:3], ytick_lbls[0:-1:3])

    from matplotlib.cm import get_cmap
    cmap = get_cmap('Blues_r') # reverse of 'Blues'

    plt.subplot(223)
    c_list = cmap(np.linspace(0,1,nk_para+1))
    for ik_para in range(nk_para):
        plt.plot(np.unique(k_perp), DeltaSqr[:,ik_para], c=c_list[ik_para])
    plt.xlabel(r'$\rm k_\perp\ [h/Mpc]$')
    plt.ylabel(r'$\rm k^3 P(k_\perp)/(2\pi^2)$')
    plt.xscale('log')
    plt.yscale('log')

    plt.subplot(224)
    c_list = cmap(np.linspace(0,1,nk_perp+1))
    for ik_perp in range(0,nk_perp):
        plt.plot(np.unique(k_para), DeltaSqr[ik_perp,:], c=c_list[ik_perp])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k_\parallel\ [h/Mpc]$')
    plt.ylabel(r'$\rm k^3 P(k_\parallel)/(2\pi^2)$')
    
    plt.savefig('../../plots/pk_tools/%s_pk_perp_para.pdf'%(name))
    
    #plt.show()
"""
if __name__ == "__main__":
    calc_halpha_pk()
    #plot_halpha_pk()
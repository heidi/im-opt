#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

cat_name_1 = pa.data_master_loc+'pk_tools/demo_ifile.dat'
cat_name_2 = pa.data_master_loc+'pk_tools/half_ifile.dat'

ngrid = 128
Lbox = 250.

def write_pk_input_file():
    hlist = pa.bolshoip_loc + '/hlist_0.08937.list'
    data = np.loadtxt(hlist)
    px = data[:,17]
    py = data[:,18]
    pz = data[:,19]
    Mvir = data[:,10]
    print('len(px)', len(px))
    #lgMmin = 10
    #exit()
    outfile1 = open(cat_name_1, 'w')
    outfile2 = open(cat_name_2, 'w')
    for i in range(len(px)):
        if True:#np.log10(Mvir[i]) > lgMmin:
            outfile1.write('%g %g %g %g \n'%(px[i],py[i],pz[i],1))
        if pz[i] < 0.5*Lbox: #np.log10(Mvir[i]) > lgMmin and 
            outfile2.write('%g %g %g %g \n'%(px[i],py[i],pz[i],1))
    outfile1.close()
    outfile2.close()




from pk_3d_mastercode import PowerSpectrum3D

def calc_pk():
    ifname = cat_name_1
    ofname = pa.data_master_loc+'pk_tools/demo'
    ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=False, save_grid=False, divide_by_mean=True)#/pa.h
    ps3.make_halo_density_grid()
    ps3.fourier_transform()


from pk_non_cubic import PowerSpectrumNonCubic

def calc_pk_non_cubic():
    ifname = cat_name_2
    ofname = pa.data_master_loc+'pk_tools/demo_non_cubic'
    psnc = PowerSpectrumNonCubic(ifname=ifname, ofname=ofname, nx=ngrid, ny=ngrid, nz=ngrid//2,\
     Lx=Lbox, Ly=Lbox, Lz=Lbox/2, use_weight=False, save_grid=False, divide_by_mean=True)#/pa.h
    psnc.make_halo_density_grid()
    psnc.fourier_transform()


def plot_pk():
    k, pk = np.loadtxt(pa.data_master_loc+'pk_tools/BolshoiP_matterpower.dat', unpack=True)
    b = (k>0.01)*(k < 10)
    plt.plot(k[b], pk[b]*0.6, label='CAMB')

    k, pk = np.loadtxt(pa.data_master_loc+'pk_tools/demo_pk.dat', unpack=True)
    plt.plot(k, pk, label='BolshoiP, cubic')

    k, pk = np.loadtxt(pa.data_master_loc+'pk_tools/demo_non_cubic_pk.dat', unpack=True)
    plt.plot(k, pk, label='BolshoiP, non cubic')

    plt.legend()
    plt.xlabel('k')
    plt.ylabel('P(k)')
    plt.xscale('log')
    plt.yscale('log')
    plt.savefig('../../plots/pk_tools/demo_pk.pdf')
    plt.show()

if __name__ == "__main__":
    # write_pk_input_file()
    # calc_pk()
    # calc_pk_non_cubic()
    plot_pk()
#!/usr/bin/env python
#from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
from astropy.coordinates import cartesian_to_spherical
import sys
sys.path.append('../')
#sys.path.append('../pk_tools/')
import params as pa

"""
master code for plotting P(k_perp, k_para)
"""

def plot_halpha_pk(name = 'halo_non_cubic_half_long_voxel'):

    plt.figure(figsize=(14,14))
    # P(k)
    plt.subplot(221)
    # CAMB first
    k, pk = np.loadtxt(pa.data_master_loc+'pk_tools/BolshoiP_matterpower.dat', unpack=True)
    b = (k>0.01)*(k < 10)
    plt.plot(k[b], pk[b]*k[b]**3/(2.*np.pi**2), label='CAMB')
    # my calculation
    fname = pa.data_master_loc+'pk_tools/%s_pk_non_cubic.dat'%(name)
    k, pk = np.loadtxt(fname, unpack=True)
    plt.plot(k, pk*k**3/(2.*np.pi**2))

    plt.xlim(1e-2,10)
    plt.ylim(1e-3,100)
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k \ [h/Mpc]$')
    plt.ylabel(r'$\rm k^3P(k)/(2\pi^2)$')
    name2 = name.replace('_',' ')
    plt.title('%s'%(name2))
    
    plt.subplot(222)
    # P(k_perp, k_para)
    fname = pa.data_master_loc+'pk_tools/%s_pk_perp_para_non_cubic.dat'%(name)
    k_perp, k_para, pk = np.loadtxt(fname, unpack=True)
    k_cubed = np.sqrt(k_perp**2 + k_para**2)**3
    DeltaSqr = pk * k_cubed/(2*np.pi**2)

    k_perp = np.unique(k_perp)
    k_para = np.unique(k_para)
    nk_perp = len(k_perp)
    nk_para = len(k_para)
    pk = pk.reshape([nk_perp, nk_para])
    DeltaSqr = DeltaSqr.reshape([nk_perp, nk_para])
    plt.imshow(np.log10(DeltaSqr.transpose()), origin='lower', aspect='equal', extent=(0,1,0,1), interpolation='nearest')
    
    plt.xlabel(r'$\rm k_\perp \ [h/Mpc]$')
    plt.ylabel(r'$\rm k_\parallel \ [h/Mpc]$')
    plt.title(r'$\rm log_{10} k^3 P(k_\perp, k_\parallel)/(2\pi^2)$')

    xtick_lbls = ['{:.2f}'.format(i) for i in k_perp]
    xtick_locs = (0.5+np.arange(nk_perp))/nk_perp
    plt.yticks(xtick_locs[0:-1:3], xtick_lbls[0:-1:3])

    ytick_lbls = ['{:.2f}'.format(i) for i in k_para]
    ytick_locs = (0.5+np.arange(nk_para))/nk_para
    plt.xticks(ytick_locs[0:-1:3], ytick_lbls[0:-1:3])

    from matplotlib.cm import get_cmap
    cmap = get_cmap('Blues_r') # reverse of 'Blues'

    plt.subplot(223)
    c_list = cmap(np.linspace(0,1,nk_para+1))
    for ik_para in range(nk_para):
        plt.plot(np.unique(k_perp), DeltaSqr[:,ik_para], c=c_list[ik_para], label=r'$\rm k_\parallel$=%.2g'%k_para[ik_para])
    plt.xlim(1e-2,10)
    plt.ylim(1e-2,100)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k_\perp\ [h/Mpc]$')
    plt.ylabel(r'$\rm k^3 P(k_\perp)/(2\pi^2)$')
    plt.legend()

    plt.subplot(224)
    c_list = cmap(np.linspace(0,1,nk_perp+1))
    for ik_perp in range(0,nk_perp):
        plt.plot(np.unique(k_para), DeltaSqr[ik_perp,:], c=c_list[ik_perp], label=r'$\rm k_\perp$=%.2g'%k_perp[ik_perp])
    plt.xlim(1e-2,10)
    plt.ylim(1e-2,100)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k_\parallel\ [h/Mpc]$')
    plt.ylabel(r'$\rm k^3 P(k_\parallel)/(2\pi^2)$')
    plt.legend()
    
    plt.savefig('../../plots/pk_tools/%s_pk_perp_para.pdf'%(name))
    
    #plt.show()

if __name__ == "__main__":
    name1 = 'halo_non_cubic'
    name2 = 'halo_non_cubic_half'
    name3 = 'halo_non_cubic_half_long_voxel'
    for name in [name1, name2, name3]:
        plot_halpha_pk(name=name)
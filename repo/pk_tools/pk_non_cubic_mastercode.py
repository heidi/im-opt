#!/usr/bin/env python
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

'''
modified from halo_pk_3d_mastercode.py
ifname: x, y, z, weight.  No header
win_name: flattened 3D grid, 0 or 1
'''
class PowerSpectrumNonCubic(object):
    def __init__(self, ifname, ofname, nx, ny, nz, Lx, Ly, Lz, \
        use_weight=True, save_grid=False, divide_by_mean=False, use_window=False, win_name=''):
        self.ifname = ifname
        self.oname = ofname
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.Lx = Lx # size of the box # beware of h!
        self.Ly = Ly # size of the box # beware of h!
        self.Lz = Lz # size of the box # beware of h!
        self.use_weight = use_weight
        self.save_grid = save_grid
        self.divide_by_mean = divide_by_mean # True for matter density, False for intensity mapping
        self.use_window = use_window
        self.win_name = win_name


        self.lx = self.Lx/(nx*1.) # size of the grid 
        self.ly = self.Ly/(ny*1.) # size of the grid 
        self.lz = self.Lz/(nz*1.) # size of the grid 


    def make_halo_density_grid(self):
        # generate the grid for FFT
        # loop through galaxies

        Lx = self.Lx
        Ly = self.Ly
        Lz = self.Lz
        nx = self.nx
        ny = self.ny
        nz = self.nz
        lx = self.lx
        ly = self.ly
        lz = self.lz

        ifile = open(self.ifname, 'r') # no header in this file!
        self.fx = np.zeros([nx,ny,nz])
        for line in ifile:
            halo = line.split()
            px = np.double(halo[0])
            py = np.double(halo[1])
            pz = np.double(halo[2])
            # if px > Lx+1.e-3 or py > Ly+1.e-3 or pz > Lz+1.e-3: 
            #     print('wrong coordinates or box size')
            #     print('px, py, pz  vs Lx, Ly, Lz = %g %g %g %g %g %g'%(px,py,pz,Lx,Ly,Lz))
            if self.use_weight==True:
                weight = np.double(halo[3])
            else:
                weight = 1

            if px <= Lx or py <= Ly or pz <= Lz:
                i = min(int(np.floor(px/lx)),nx-1)
                j = min(int(np.floor(py/ly)),ny-1)
                k = min(int(np.floor(pz/lz)),nz-1)
                self.fx[i,j,k] += weight

        if self.use_window == False:
            print('no window')
            self.vol_frac = 1
            fx_flat = self.fx.flatten()
            mean_fx = np.mean(fx_flat)
            if self.divide_by_mean == True: 
                self.fx = self.fx / mean_fx - 1
            if self.divide_by_mean == False: 
                self.fx = self.fx - mean_fx
        else:
            print('use window')
            self.window = np.loadtxt(self.win_name).reshape((nx,ny,nz))
            fx_flat = self.fx.flatten()
            win_flat = self.window.flatten()
            b = (win_flat == 1)
            mean_fx = np.mean(fx_flat[b])
            print('np.mean(fx_flat)', mean_fx)
            if self.divide_by_mean == True: 
                fx_flat[b] = fx_flat[b]/mean_fx - 1
            if self.divide_by_mean == False: 
                fx_flat[b] = fx_flat[b] - mean_fx
            #win_norm = (1.*len(win_flat))/(1.*sum(win_flat)) # normalize the window function
            fx_flat *= (win_flat)#*win_norm)
            self.fx = fx_flat.reshape((nx,ny,nz))
            self.vol_frac = (1.*sum(win_flat))/(1.*len(win_flat))

        if self.save_grid == True:
            np.savetxt(self.oname+'_grid.dat', fx_flat)


        if True:#save_grid_coord == True:
            outfile = open(self.oname+'_grid_with_coordinate.dat','w')
            outfile.write('# x, y, z, intensity \n')
            for i in range(nx):
                for j in range(ny):
                    for k in range(nz):
                        outfile.write('%12g %12g %12g %12g \n'%(i*lx, j*ly, k*lz, self.fx[i,j,k]+mean_fx))
            outfile.close()

    def fourier_transform(self, anisotropic=False, dlgk=0.2, dlgk_perp=0.2, dlgk_para=0.2):
        pi = np.pi
        Lx = self.Lx
        Ly = self.Ly
        Lz = self.Lz
        nx = self.nx
        ny = self.ny
        nz = self.nz
        lx = self.lx
        ly = self.ly
        lz = self.lz
        if self.save_grid == True:
            fx_flat = np.loadtxt(self.oname+'_grid.dat')
            self.fx = fx_flat.reshape((nx,ny,nz))

        Fk = fft.fftn(self.fx)*(Lx/nx)*(Ly/ny)*(Lz/nz) # Fourier coefficients
        Fk = np.absolute(Fk)**2

        if anisotropic == False:
            # create the grid based on Python convention
            k = np.zeros([nx,ny,nz])
            for i1 in range(nx):
                for i2 in range(ny):
                    for i3 in range(nz):
                        if i1 < nx/2: k1 = i1*2*pi/Lx
                        else: k1 = (i1-nx)*2*pi/Lx
                        if i2 < ny/2: k2 = i2*2*pi/Ly
                        else: k2 = (i2-ny)*2*pi/Ly
                        if i3 < nz/2: k3 = i3*2*pi/Lz
                        else: k3 = (i3-nz)*2*pi/Lz
                        k[i1,i2,i3] = np.sqrt(k1**2+k2**2+k3**2)

            # summing over k-modes
            k = k.flatten()
            Fk = Fk.flatten()

            b = (k > 0)
            k = k[b]
            Fk = Fk[b]

            kmin = 2*np.pi/min(Lx,Ly,Lz)
            kmax = 2*np.pi/max(lx,ly,lz)
            #nk = 50
            #lgk_list = np.linspace(np.log10(kmin), np.log10(kmax), nk+1)
            lgk_list = np.arange(np.log10(kmin), np.log10(kmax)+dlgk, dlgk)
            nk= len(lgk_list)-1
            lgk_mid = 0.5*(lgk_list[0:-1] + lgk_list[1:])
            Pk_list = np.zeros(nk)
            for ik in range(nk):
                b = (np.log10(k) > lgk_list[ik])*(np.log10(k) < lgk_list[ik+1])
                if len(Fk[b]) > 0:
                    Pk_list[ik] = np.mean(Fk[b])/(Lx*Ly*Lz * self.vol_frac) # NOTE! don't forget L^3 

            outfile = open(self.oname+'_pk_non_cubic.dat','w')
            outfile.write('#k [h/Mpc], Pk [(Mpc/h)^3] \n')
            for i in range(nk):
                outfile.write('%g %g\n'%(10**lgk_mid[i], Pk_list[i]))
            outfile.close()

        if anisotropic == True:
            k_perp = np.zeros([nx,ny,nz])
            k_para = np.zeros([nx,ny,nz])
            for i1 in range(nx):
                for i2 in range(ny):
                    for i3 in range(nz):
                        if i1 < nx/2: k1 = i1*2*pi/Lx
                        else: k1 = (i1-nx)*2*pi/Lx
                        if i2 < ny/2: k2 = i2*2*pi/Ly
                        else: k2 = (i2-ny)*2*pi/Ly
                        if i3 < nz/2: k3 = i3*2*pi/Lz
                        else: k3 = (i3-nz)*2*pi/Lz
                        k_perp[i1,i2,i3] = np.sqrt(k1**2+k2**2)
                        k_para[i1,i2,i3] = k3

            # summing over k-modes
            k_perp = k_perp.flatten()
            k_para = k_para.flatten()
            Fk = Fk.flatten()


            print('k_perp and k_para')
            b = (k_perp > 0)&(k_para > 0)
            k_perp = k_perp[b]
            k_para = k_para[b]
            Fk = Fk[b]
            k_perp_min = 2.*np.pi/Lx#/np.sqrt(2)
            k_perp_max = 2.*np.pi/lx
            k_para_min = 2.*np.pi/Lz
            k_para_max = 2.*np.pi/lz

            lgk_perp_list = np.arange(np.log10(k_perp_min), np.log10(k_perp_max)+dlgk_perp, dlgk_perp)
            lgk_para_list = np.arange(np.log10(k_para_min), np.log10(k_para_max)+dlgk_para, dlgk_para)
            nk_perp = len(lgk_perp_list)-1
            nk_para = len(lgk_para_list)-1

            lgk_perp_mid = 0.5*(lgk_perp_list[0:-1] + lgk_perp_list[1:])
            lgk_para_mid = 0.5*(lgk_para_list[0:-1] + lgk_para_list[1:])
            print('nk_perp =', nk_perp, 'nk_para =', nk_para)
            Pk_ani_list = np.zeros([nk_perp,nk_para])
            outfile = open(self.oname+'_pk_perp_para_non_cubic.dat','w')
            outfile.write('#k_perp [1/Mpc], k_para[1/Mpc], Pk \n')
            for ik_perp in range(nk_perp):
                for ik_para in range(nk_para):
                    b = (np.log10(k_perp) >= lgk_perp_list[ik_perp])&(np.log10(k_perp) < lgk_perp_list[ik_perp+1])&\
                    (np.log10(k_para) >= lgk_para_list[ik_para])&(np.log10(k_para) < lgk_para_list[ik_para+1])
                    if len(Fk[b]) > 0:
                        Pk_ani_list[ik_perp, ik_para] = np.mean(Fk[b])/(Lx*Ly*Lz* self.vol_frac) # NOTE! don't forget L^3 
                    outfile.write('%g %g %g\n'%(10**lgk_perp_mid[ik_perp], 10**lgk_para_mid[ik_para], Pk_ani_list[ik_perp,ik_para]))
            outfile.close()







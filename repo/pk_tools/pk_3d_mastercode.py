#!/usr/bin/env python
from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa

'''
see:  halo_pk_3d_mastercode_demo.py for example
ifname: x, y, z, weight.  No header
win_name: flattened 3D grid, 0 or 1
'''
class PowerSpectrum3D(object):
    def __init__(self, ifname, ofname, ngrid, Lbox, use_weight=True, save_grid=False, divide_by_mean=False, use_window=False, win_name=''):
        self.ifname = ifname
        self.oname = ofname
        self.ngrid = ngrid
        self.Lbox = Lbox # size of the box # beware of h!
        self.use_weight = use_weight
        self.save_grid = save_grid
        self.divide_by_mean = divide_by_mean # True for matter density, False for intensity mapping
        self.use_window = use_window
        self.win_name = win_name
        self.l_cell = self.Lbox/(ngrid*1.) # size of the grid 
        self.nk = 50


    def make_halo_density_grid(self):
        # generate the grid for FFT
        # loop through galaxies

        n = self.ngrid

        ifile = open(self.ifname, 'r') # no header in this file!
        self.fx = np.zeros([n,n,n])
        for line in ifile:
            halo = line.split()
            px = np.double(halo[0])
            py = np.double(halo[1])
            pz = np.double(halo[2])
            if px > self.Lbox+1.e-3 or py > self.Lbox+1.e-3 or pz > self.Lbox+1.e-3: 
                print('wrong coordinates or box size')
                print('px, py, pz, Lbox = %g %g %g %g '%(px,py,pz,self.Lbox))
            if self.use_weight==True:
                weight = np.double(halo[3])
            else:
                weight = 1

            i = min(int(np.floor(px/self.l_cell)),n-1)
            j = min(int(np.floor(py/self.l_cell)),n-1)
            k = min(int(np.floor(pz/self.l_cell)),n-1)
            self.fx[i,j,k] += weight

        if self.use_window == False:
            print('no window')
            self.vol_frac = 1
            fx_flat = self.fx.flatten()
            mean_fx = np.mean(fx_flat)
            if self.divide_by_mean == True: 
                self.fx = self.fx / mean_fx - 1
            if self.divide_by_mean == False: 
                self.fx = self.fx - mean_fx
        else:
            print('use window')
            self.window = np.loadtxt(self.win_name).reshape((n,n,n))
            fx_flat = self.fx.flatten()
            win_flat = self.window.flatten()
            b = (win_flat == 1)
            mean_fx = np.mean(fx_flat[b])
            print('np.mean(fx_flat)', mean_fx)
            if self.divide_by_mean == True: 
                fx_flat[b] = fx_flat[b]/mean_fx - 1
            if self.divide_by_mean == False: 
                fx_flat[b] = fx_flat[b] - mean_fx
            #win_norm = (1.*len(win_flat))/(1.*sum(win_flat)) # normalize the window function
            fx_flat *= (win_flat)#*win_norm)
            self.fx = fx_flat.reshape((n,n,n))
            self.vol_frac = (1.*sum(win_flat))/(1.*len(win_flat))

        if self.save_grid == True:
            np.savetxt(self.oname+'_grid.dat', fx_flat)

    def fourier_transform(self, anisotropic=False):
        pi = np.pi
        n = self.ngrid
        L = self.Lbox
        if self.save_grid == True:
            fx_flat = np.loadtxt(self.oname+'_grid.dat')
            self.fx = fx_flat.reshape((n,n,n))

        Fk = fft.fftn(self.fx)*(L/n)**3 # Fourier coefficients
        Fk = np.absolute(Fk)**2

        if anisotropic == False:
            # create the grid based on Python convention
            k = np.zeros([n,n,n])
            for i1 in range(n):
                for i2 in range(n):
                    for i3 in range(n):
                        if i1 < n/2: k1 = i1*2*pi/L
                        else: k1 = (i1-n)*2*pi/L
                        if i2 < n/2: k2 = i2*2*pi/L
                        else: k2 = (i2-n)*2*pi/L
                        if i3 < n/2: k3 = i3*2*pi/L
                        else: k3 = (i3-n)*2*pi/L
                        k[i1,i2,i3] = np.sqrt(k1**2+k2**2+k3**2)

            # summing over k-modes
            k = k.flatten()
            Fk = Fk.flatten()

            b = (k > 0)
            k = k[b]
            Fk = Fk[b]

            kmin = 2*np.pi/self.Lbox
            kmax = 2*np.pi/self.l_cell
            nk = self.nk
            lgk_list = np.linspace(np.log10(kmin), np.log10(kmax), nk+1)
            lgk_mid = 0.5*(lgk_list[0:-1] + lgk_list[1:])
            Pk_list = np.zeros(nk)
            for ik in range(nk):
                b = (np.log10(k) > lgk_list[ik])*(np.log10(k) < lgk_list[ik+1])
                if len(Fk[b]) > 0:
                    Pk_list[ik] = np.mean(Fk[b])/(self.Lbox**3 * self.vol_frac) # NOTE! don't forget L^3 

            outfile = open(self.oname+'_pk.dat','w')
            outfile.write('#k [h/Mpc], Pk [(Mpc/h)^3] \n')
            for i in range(nk):
                outfile.write('%g %g\n'%(10**lgk_mid[i], Pk_list[i]))
            outfile.close()

        if anisotropic == True:
            k_perp = np.zeros([n,n,n])
            k_para = np.zeros([n,n,n])
            for i1 in range(n):
                for i2 in range(n):
                    for i3 in range(n):
                        if i1 < n/2: k1 = i1*2*pi/L
                        else: k1 = (i1-n)*2*pi/L
                        if i2 < n/2: k2 = i2*2*pi/L
                        else: k2 = (i2-n)*2*pi/L
                        if i3 < n/2: k3 = i3*2*pi/L
                        else: k3 = (i3-n)*2*pi/L
                        k_perp[i1,i2,i3] = np.sqrt(k1**2+k2**2)
                        k_para[i1,i2,i3] = k3

            # summing over k-modes
            k_perp = k_perp.flatten()
            k_para = k_para.flatten()
            Fk = Fk.flatten()

            '''
            print('k_perp only')
            # k_perp first
            k = k_perp
            b = (k > 0)
            k = k[b]
            Fk = Fk[b]

            kmin = 2*np.pi/self.Lbox
            kmax = 2*np.pi/self.l_cell
            nk = self.nk
            lgk_list = np.linspace(np.log10(kmin), np.log10(kmax), nk+1)
            lgk_mid = 0.5*(lgk_list[0:-1] + lgk_list[1:])
            Pk_list = np.zeros(nk)
            for ik in range(nk):
                b = (np.log10(k) > lgk_list[ik])*(np.log10(k) < lgk_list[ik+1])
                if len(Fk[b]) > 0:
                    Pk_list[ik] = np.mean(Fk[b])/(self.Lbox**3 * self.vol_frac) # NOTE! don't forget L^3 

            outfile = open(self.oname+'_pk_perp.dat','w')
            outfile.write('#k_perp [h/Mpc], Pk [(Mpc/h)^3] \n')
            for i in range(nk):
                outfile.write('%g %g\n'%(10**lgk_mid[i], Pk_list[i]))
            outfile.close()
            '''
            print('k_perp and k_para')
            b = (k_perp > 0)&(k_para > 0)
            k_perp = k_perp[b]
            k_para = k_para[b]
            Fk = Fk[b]
            k_perp_min = 0.05#2*np.pi/self.Lbox
            k_perp_max = 1#2*np.pi/self.l_cell
            k_para_min = 0.01#2*np.pi/self.Lbox
            k_para_max = 0.1#2*np.pi/self.l_cell
            nk_perp = 20#self.nk
            nk_para = 5
            lgk_perp_list = np.linspace(np.log10(k_perp_min), np.log10(k_perp_max), nk_perp+1)
            lgk_para_list = np.linspace(np.log10(k_para_min), np.log10(k_para_max), nk_para+1)

            lgk_perp_mid = 0.5*(lgk_perp_list[0:-1] + lgk_perp_list[1:])
            lgk_para_mid = 0.5*(lgk_para_list[0:-1] + lgk_para_list[1:])
            Pk_ani_list = np.zeros([nk_perp,nk_para])
            outfile = open(self.oname+'_pk_perp_para.dat','w')
            outfile.write('#k_perp [1/Mpc], k_para[1/Mpc], Pk \n')
            for ik_perp in range(nk_perp):
                for ik_para in range(nk_para):
                    b = (np.log10(k_perp) > lgk_perp_list[ik_perp])&(np.log10(k_perp) < lgk_perp_list[ik_perp+1])&\
                    (np.log10(k_para) > lgk_para_list[ik_para])&(np.log10(k_para) < lgk_para_list[ik_para+1])
                    if len(Fk[b]) > 0:
                        Pk_ani_list[ik_perp, ik_para] = np.mean(Fk[b])/(self.Lbox**3 * self.vol_frac) # NOTE! don't forget L^3 
                    outfile.write('%g %g %g\n'%(10**lgk_perp_mid[ik_perp], 10**lgk_para_mid[ik_para], Pk_ani_list[ik_perp,ik_para]))
            outfile.close()







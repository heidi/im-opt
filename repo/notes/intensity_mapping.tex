\documentclass[letterpaper,11pt]{article}
\usepackage{hyperref,geometry,color,bm}
\usepackage{graphicx}%,stfloats}
\usepackage{sansmath,amsmath,amssymb}
\usepackage[numbers]{natbib}
\usepackage{mathtools}%for dcases
\usepackage{listings}
\geometry{textheight=8.65in, textwidth=6.6in}

\include{mycommand}
\begin{document}

\title{Intensity mapping for optical emission lines}
\author{Hao-Yi Wu}
\maketitle
%\tableofcontents


\section{Summary of the procedure for generating data cubes}

For a given emission line:
\begin{itemize}

\item Generate the 1.4 deg $\times$ 1.4 deg lightcone of dark matter halos (with R.A., Dec., redshift, $M_{200}$).  This is the same lightcone as in Bethermin et al. (2017)

\item Calculate the spectra for each halo mass and redshift using star-formation history and the FSPS model  (Section~\ref{sec:FSPS})

\item Calculate the emission line strength as a function of halo mass and redshift  (Section~\ref{sec:lines})

\item For each halo in the lightcone, calculate the emission line flux at the observed wavelength  (Section~\ref{sec:line_catalog})

\item For a given band of the instrument, sum over the halos that contribute to this band (Section~\ref{sec:cube})

\end{itemize}

For the continuum,

\begin{itemize}

\item For the spectra of each halo mass and redshift, integrate the the spectra over each band.  Each halo contribute to all bands  (Section~\ref{sec:continuum})

\item Sum over all halos' contribution to all bands

\end{itemize}


\section{Generating spectra using FSPS model and star-formation history}\label{sec:FSPS}

I got the star-formation rate history as a function of halo mass and redshift from Peter Behroozi.  I then plug in the star-formation rate history into the FSPS code and generate the spectral energy distribution.
Figure~\ref{fig:fsps} shows examples of star-formation rate history for halos at $z=0$ (left-hand panel) and the spectral energy distribution output for the code (right-hand panel).
Figure~\ref{fig:optical_lines} shows examples of optical lines at $z=0$.
The most important lines are 
\begin{itemize}
\item H$\alpha$: 6563 \AA
\item $\rm [OIII]$: 5007 \AA 
\item $\rm [OII]$: 3727 \AA
\item H$\beta$: 4861 \AA
\end{itemize}

%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=\columnwidth]{../../plots/fsps/fsps_sfh.pdf}
\caption{Examples for star-formation history and outputs of FSPS.}
\label{fig:fsps}
\end{figure}
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=\columnwidth]{../../plots/spectra_example.pdf}
\caption{Example for the optical emission lines at various redshifts.}
\label{fig:optical_lines}
\end{figure}
%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Emission lines}\label{sec:lines}

Here I use H$\alpha$ lines as an example. 
For each spectra, I integrate $\pm$10\AA  around the center of the line to get the line luminosity (erg/s).  Figure~\ref{fig:Ha_line} shows examples of line shape and intensity.
Figure~\ref{fig:Ha_Kennicutt} shows the H$\alpha$ intensity calculated this way vs.\ SFR (from the Behroozi model).  The line corresponds to the Kennicutt \& Evans (2012) relation.

\heidi{TODO: dust attenuation for H$\alpha$?}
\heidi{TODO: enhanced H$\alpha$ intensity at high redshift?}


%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=0.5\columnwidth]{../../plots/Halpha_spec_sanity.pdf}
\includegraphics[width=0.5\columnwidth]{../../plots/Halpha_lum_sanity.pdf}
\caption{Zooming in on H$\alpha$ lines.}
\label{fig:Ha_line}
\end{figure}
%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=0.5\columnwidth]{../../plots/Ha_Kennicutt.pdf}
\caption{H$\alpha$ luminosity vs.\ SFR.  Here H$\alpha$ intensity comes from integrating over the spectrum. }
\label{fig:Ha_Kennicutt}
\end{figure}
%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Emission line catalog}\label{sec:line_catalog}


Here I still use H$\alpha$ as an example. 
For each halo in the lightcone, I assign it a luminosity of H$\alpha$ based on its mass and luminosity.
Figure~\ref{fig:Ha_LM} shows the luminosity--mass relation and the luminosity functions at several redshifts.

For a given redshift slice, I then sum over the volume to calculate the average intensity.
\beq
\bar{S_\nu} = \sum_{{\rm all\ halos}} S_\nu / V_{{\rm redshift\ slice}}
\eeq
This is equivalent to Eq. 2.9 in Visbal10 or Eq. 9 in Gong17.
\beq
{S_\nu} = \int dM \frac{dn}{dM} \frac{L_{\rm line}}{4\pi D_L^2} D_A^2 \frac{d\chi}{d\nu}
\eeq

The left-hand panel of Figure~\ref{fig:S_z_Ha} shows the total intensity of H$\alpha$.  The total intensity is lower compared with the Gong17 model.  

The right-hand panel of Figure~\ref{fig:S_z_Ha} shows  the integrated star-formation rate density.



%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=0.5\columnwidth]{../../plots/Ha_LM.pdf}
\includegraphics[width=0.5\columnwidth]{../../plots/Ha_LF.pdf}
\caption{{\bf Left}: $H_\alpha$ vs.\ halo mass. {\bf Right}: $H_\alpha$ luminosity function.}
\label{fig:Ha_LM}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\includegraphics[width=0.5\columnwidth]{../../plots/S_z_Halpha.pdf}
\includegraphics[width=0.5\columnwidth]{../../plots/rho_SFR.pdf}
\caption{{\bf Left}: Total intensity of H$\alpha$.  {\bf Right}: star-formation rate density from this model. }
\label{fig:S_z_Ha}
\end{figure}
%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Generating data cube with {\tt Cygrid}}  
\section{Generating the data cube}\label{sec:cube}

For a given line, the flux is given by
\beq
S_\nu = \frac{L_{\rm line}}{4\pi D_L^2} \frac{d\chi}{dz} D_A^2
\eeq
where 
\beq
\frac{d\chi}{dz} = \frac{D_H}{E(z)} \frac{(1+z)^2}{\nu_{\rm line}}
\eeq
(see Gong et al.~2017 and Visbal et al.~2011).

Each galaxy is assigned a value of $S_\nu$ at $\lambda_{\rm obs} = \lambda_{\rm rest} (1+z)$.
Then for a given band given by $(\lammin,\ \lammax)$,
I sum over the flux between 
\beqa
\zmin = \lammin/ \lambda_{\rm rest} - 1 \\
\zmax = \lammax/ \lambda_{\rm rest} - 1 \\
\eeqa
and divide the sum by 
\beq
V_{\rm voxel} =  \bigg(V_c(\zmax) - V_c(\zmax) \bigg) \frac{\Omega_{\rm pixel}}{4\pi}
\eeq
where $V_c(z)$ is the comoving volume for full sky out to redshift $z$.

Figure~\ref{fig:maps} shows examples for H$\alpha$ and $H\beta$ at two Spherex bands.

%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\vspace{-2cm}
\includegraphics[width=0.5\columnwidth]{../../plots/lines_for_spherex/Halpha_lam_20.png}
\includegraphics[width=0.5\columnwidth]{../../plots/lines_for_spherex/Halpha_lam_30.png}
\includegraphics[width=0.5\columnwidth]{../../plots/lines_for_spherex/Hbeta_lam_20.png}
\includegraphics[width=0.5\columnwidth]{../../plots/lines_for_spherex/Hbeta_lam_30.png}
\includegraphics[width=0.5\columnwidth]{../../plots/continuum_for_spherex/continuum_band_20.png}
\includegraphics[width=0.5\columnwidth]{../../plots/continuum_for_spherex/continuum_band_30.png}
\caption{Maps for H$\alpha$, H$\beta$, and the continuum.}
\label{fig:maps}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%



\section{Contribution from the continuum}\label{sec:continuum}
For each galaxy spectra, I integrate the flux in a given band
\beq
L_{\rm cont.} = \int_{\numin}^{\numax} L_\nu d\nu
\eeq
and the contribution to flux is given by 
\beq
S_\nu = \frac{L_{\rm cont.}}{4\pi D_L^2} \frac{d\chi}{dz} D_A^2
\eeq
where
\beq
\frac{d\chi}{dz} = \frac{D_H}{E(z)} \frac{(1+z)^2}{\nu_{\rm mid}}
\eeq
where $\nu_{\rm mid}$ is the middle of the bin {\em converted to the rest frame of that redshift}. \heidi{check this!}

I generate the data cube using similar procedures as in Section~\ref{sec:cube}, except for that
each galaxy contributes to all bands.  

The bottom row of Figure~\ref{fig:maps} shows the map of the continuum.

\clearpage
\section{Computing the power spectra}

Figure~\ref{fig:pk_3d} shows the 3D power spectra computed from dark matter halos (left)
and from H$\alpha$ intensity (right).



%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=0.5\columnwidth]{{../../plots/pk_3d/halo_pk_a0.16700_f1}.pdf}
\includegraphics[width=0.5\columnwidth]{{../../plots/pk_3d/halpha_pk_a0.16700}.pdf}
\caption{Power spectra measured from simulation.  Different curves are different validations and sanity checks.  {\bf Left}: halos. {\bf Right}: H$\alpha$ lines.}
\label{fig:pk_3d}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%


\section{Anisotropic power spectrum}

%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=0.5\columnwidth]{../../plots/pk_cube_halpha/delta_chi_halpha.pdf}
\caption{The comoving length along the line-of-sight for each voxel of Spherex.
The verticle magenta lines denote the four bands, and each band has 24 wavelengths of the same R equally spaced in
$\Delta\ln\lambda$. The fourth band has the highest spectral resolution.
}
\label{fig:delta_chi}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=\columnwidth]{{../../plots/pk_cube_halpha/halpha_non_cubic_z_5.4_6.6_pk_perp_para}.pdf}
\caption{Anisotropic power spectra for band 4 of Spherex, which corresoponds to 24 wavelengths between $5.4<z<6.6$ for $H\alpha$.
{\bf (a)} The FFT grid. I use 128 bins in $x$ and $y$ directions, and 24 bins in the $z$ direction.  I also cut the cone shape to a rectangle.
{\bf (b)} $P(k_\perp, k_\parallel)$.
{\bf (c)} $P(k_\perp)$ for various fixed $k_\parallel$.
{\bf (d)} $P(k_\parallel)$ for various fixed $k_\perp$.}
\label{fig:halpha_non_cubic_z_5.4_6.6_pk_perp_para}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=\columnwidth]{../../plots/pk_cube_continuum/continuum_band_72_95_pk_perp_para.pdf}
\caption{For continnuum.  I convert the data cube to comoving coordinate as if it is H$\alpha$. 
This shows very different structure from H$\alpha$.}
\label{fig:continuum_band_72_95_pk_perp_para}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=0.5\columnwidth]{../../plots/pk_tools/halo_non_cubic_pk_perp_para.pdf}
\includegraphics[width=0.5\columnwidth]{../../plots/pk_tools/halo_non_cubic_half_pk_perp_para.pdf}
\includegraphics[width=0.5\columnwidth]{../../plots/pk_tools/halo_non_cubic_half_long_voxel_pk_perp_para.pdf}
\caption{Validation of the non-cubic FFT (differnet $L_x$, $L_y$, $L_z$ and $k_x$, $k_y$, $k_z$)
{\bf (a)} using the full Bolshoi box.
{\bf (b)} using half of the Bolshoi box.  
{\bf (c)} using half of the Bolshoi box and elongated voxel along LOS.
 The LOS is noisier because there are fewer modes.  }
\label{fig:pk_tools}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%


\clearpage
\section{H$\alpha$ number counts}
%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=\columnwidth]{../../plots/luminosity_functions/spherex_line_sensitivity.pdf}
\caption{Line sensitivity based on the new six-band design.}
\label{fig:spherex_line_sensitivity}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%
\begin{figure*}
\includegraphics[width=\columnwidth]{../../plots/luminosity_functions/spherex_number_counts.pdf}
\caption{Number counts.}
\label{fig:spherex_number_couts}
\end{figure*}
%%%%%%%%%%%%%%%%%%%%



\bibliography{/Users/hao-yiwu/Dropbox/master_refs}
\end{document}

\begin{figure}
\hspace{-1in}%for big figure
\includegraphics[width=1.3\columnwidth]{../../plots/xx.pdf}
\caption{An awesome figure}
\label{fig:awesome}
\end{figure}
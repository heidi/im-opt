#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.idl import readsav
fname = '/panfs/ds08/hopkins/hywu/work/Catalogs/SIDES/mock_cat_2017.06.28.save'

data = readsav(fname)
print(data.keys())
#'luv', 'l40_400', 'band_name', 'qflag', 'dec', 'lco', 'ra', 'mu', 'snu_arr', 'lir', 'issb', 'z', 'icii', 'dl', 'mstar', 'deltasfrms', 'l50_300', 'mhalo', 'lcii', 'sfr', 'isigma', 'c70160', 'umean', 'ico'

ra = data['ra']
dec = data['dec']
z = data['z']
lcii = data['lcii']

lcii = lcii[lcii > 0] # solar luminosity

# plt.subplot(111,yscale='log')
# plt.hist(np.log10(lcii))

# mass function
import sys
sys.path.append('/home/hywu/work/CIB_opt/repo/model')
from sim import Sim
sim = Sim(name='BolshoiP')
sys.path.append('/home/hywu/work/lightcone/repo/coordinate/')
import params as pa
zmin = 0
zmax = 10
params = pa.Params(theta_deg = 1.4, zmin_master = zmin, zmax_master = zmax)
vol = (sim.VC_interp(zmax)-sim.VC_interp(zmin))*params.fsky

Qlist = np.log10(lcii)
nbins = 10
binsize = 0.5
lowest = 5
x_list = np.zeros(nbins)
y_list = np.zeros(nbins)
for jj in range(nbins):
    lower = lowest + binsize*jj
    upper = lowest + binsize*(jj+1)
    hh = len(Qlist[(Qlist>=lower)*(Qlist<upper)])
    x_list[jj] = 10**(lowest+(jj+0.5)*binsize)
    y_list[jj] = hh/binsize/vol
plt.figure()
plt.subplot(111, xscale='log', yscale='log')
plt.plot(x_list, y_list, label='%g < z < %g'%(zmin, zmax))


plt.xlabel(r"$L_{[CII]} [L_\odot]$")
plt.ylabel(r"$dn/dlog_{10} L_{[CII]} [Mpc^{-3}dex^{-1}]$")
plt.title(r"$\rm Bethermin\ model$")
plt.savefig('../../plots/cii_for_time/cii_lf.pdf')
# b = (z > 6)&(z < 7)
# plt.scatter(ra[b], dec[b], c = 'y', s = 1, edgecolors = 'none')

plt.show()


#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('/home/hywu/work/')
import IM_opt.repo.params as pa # here
#print(pa.h)
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')

Lsun2cgs = 3.839e+33 

l_survey = 1.4 # deg
n_ra = 280 #int(l_ra/l_ra)
l_ra = l_survey/n_ra #0.3 arcmin, assume const for now
n_dec = n_ra
l_dec = l_ra

lam_id, lam_min_list, lam_max_list, zmin_list, zmax_list = np.loadtxt('data/time_lambda_bins.dat',unpack=True)
b = (zmax_list < 10)
lam_id = lam_id[b]
lam_min_list = lam_min_list[b]
lam_max_list = lam_max_list[b]
zmin_list = zmin_list[b]
zmax_list = zmax_list[b]
n_lam = len(lam_id)

lam_rest = 158  # micron

def write_cube():
    data_cube = np.zeros((n_ra, n_dec, n_lam))
    from scipy.io.idl import readsav
    fname = '/panfs/ds08/hopkins/hywu/work/Catalogs/SIDES/mock_cat_2017.06.28.save'
    data = readsav(fname)
    #print(data.keys())
    #'luv', 'l40_400', 'band_name', 'qflag', 'dec', 'lco', 'ra', 'mu', 'snu_arr', 'lir', 'issb', 'z', 'icii', 'dl', 'mstar', 'deltasfrms', 'l50_300', 'mhalo', 'lcii', 'sfr', 'isigma', 'c70160', 'umean', 'ico'

    z = data['z']
    ra = data['ra']
    dec = data['dec']
    L_cii = data['lcii'] * Lsun2cgs

    chi = pa.cosmo.comoving_distance(z).value
    DH = 3000. / pa.h # no h for S_cii
    DA = chi/(1+z)
    DL = chi*(1+z)
    Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)
    nu = (3.e+8)/(lam_rest*1.e-6)
    dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
    S_cii = L_cii / (4*np.pi*DL**2) * (DA**2) * dchi_dnu # note! divide by cell later

    for igal in range(len(z)):
        lam_obs = lam_rest * (1+z[igal])
        b = (lam_obs > lam_min_list)&(lam_obs < lam_max_list)
        if len(lam_id[b]) > 0:
            k = int(lam_id[b][0]+1.e-3)
            i = int(ra[igal]/l_ra)
            j = int(dec[igal]/l_dec)
            data_cube[i,j,k] += S_cii[igal]
            #print('band',k)
    

    for ilam in range(n_lam):
        zmin = zmin_list[ilam]
        zmax = zmax_list[ilam]
        #print(zmin, zmax)
        voxel_vol = (sim.VC_interp(zmax)-sim.VC_interp(zmin))*params.fsky/(n_ra**2)
        #print('voxel_vol', voxel_vol) 
        np.savetxt(pa.data_master_loc+'cii_cube/band_%i.dat'%(ilam), data_cube[:,:,ilam]/voxel_vol*pa.Mpc2cm**-2 * pa.cgs2Jy, fmt ='%g')

def plot_slice():
    for ilam in [0]:#range(60):#range(n_lam):
        plt.figure()
        data = np.loadtxt(pa.data_master_loc+'cii_cube/band_%i.dat'%(ilam))
        p1 = plt.imshow(np.arctan(data))
        print('mean', np.mean(data))
        #plt.colorbar()
        p1.axes.set_xticklabels([])
        p1.axes.set_yticklabels([])
        plt.title(r"$[CII],\ band\ %i,\ %.2f - %.2f GHz$"%(ilam, 300000./lam_max_list[ilam], 300000./lam_min_list[ilam]))
        plt.savefig(pa.data_master_loc+'cii_cube_images/band_%i.png'%(ilam))


def plot_all_slices():
    data = np.zeros((n_ra, n_ra))
    for ilam in range(60):#range(n_lam):
        data += np.loadtxt(pa.data_master_loc+'cii_cube/band_%i.dat'%(ilam))
    plt.figure()
    p1 = plt.imshow(np.arctan(data))
    print('mean', np.mean(data))
    #plt.colorbar()
    p1.axes.set_xticklabels([])
    p1.axes.set_yticklabels([])
    #plt.title(r"$H\alpha,\ band\ %i,\ %.2f - %.2f \mu m$"%(ilam, lam_min_list[ilam], lam_max_list[ilam]))
    #plt.savefig(pa.data_master_loc+'pk_cube_images/halpha_lam_%i.png'%(ilam))
    #mean 80.8577552776
    plt.show()


if __name__ == "__main__":

    #write_cube()
    plot_slice()
    #plot_all_slices()
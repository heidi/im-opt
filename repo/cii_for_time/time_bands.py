#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

lam_cii = 158

nu_cen, dnu, beam = np.loadtxt('data/time_bands_131220_from_abby.txt',unpack=True, skiprows=4)
nband = len(nu_cen)
#print(nu_cen[0:-1]/nu_cen[1:])


print('nband = ', nband)
outfile = open('data/time_lambda_bins.dat','w')
outfile.write('# i, lam_lower [micron], lam_upper[micron], z_lower, z_upper \n')
for i in range(nband):
    nu_lower = nu_cen[i] - 0.5*dnu[i]
    lam_upper = 300000./nu_lower
    z_upper = lam_upper/lam_cii - 1
    if i==0:
        nu_upper = nu_cen[i] + 0.5*dnu[i]    
        lam_lower = 300000./nu_upper
        z_lower = lam_lower/lam_cii - 1
    else:
        lam_lower = lam_new_lower # taken from the previous bin
        z_lower = z_new_lower
    lam_new_lower = lam_upper # for next bin
    z_new_lower = z_upper # for next bin
    outfile.write('%2i \t %.2f \t %.2f \t %.2f \t %.2f \n'%(i, lam_lower, lam_upper, z_lower, z_upper))

outfile.close()



#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import fsps

import matplotlib.cm
cmap = matplotlib.cm.get_cmap('afmhot')
nlines = 150
color_list = cmap(np.linspace(0,1,nlines))
plt.figure(figsize=(14,7))
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.27) # what Behroozi used



data_loc = '../../data/Behroozi_SFH_v2/'
a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')

for ia in [0]:
    for iM in [0,20,40, 60, 80, 100, 120]:
        co = color_list[iM]
        a_present = a_list[ia]
        m_present = m_list[iM]
        print('a=', a_present, 'm=', m_present)
        data = np.loadtxt(data_loc+'sfh_a%.6f.dat'%(a_present))
        b = (data[:,1] == m_present)
        z = -1 + data[b,0][::-1]
        sfr = 10**data[b,2][::-1]
        t = cosmo.age(z).value # Gyr # NOTE! t needs to be ascending!!
        plt.subplot(121, yscale='log')
        # plt.xlim(0.1,13)
        plt.plot(z, sfr, label='%.0f'%(m_present), c=co)
        #plt.plot(t, sfr, label='%.2f'%(m_present), c=co)
        plt.legend()

        plt.xlabel('z')
        plt.ylabel(r'SFR $\rm \bf [M_\odot/yr]$')

        # FSPS
        sp = fsps.StellarPopulation(sfh=3, dust_type=1, dust2=0.2)#sfh=3: tabulated sfh
        sp.set_tabular_sfh(age=t, sfr=sfr)
        wave, spec = sp.get_spectrum(tage=max(t))

        plt.subplot(122)
        b = (spec>1.e-7)
        plt.loglog(wave[b], spec[b], label='dusttype=1, dust2=0.2', c=co)
        plt.xlabel(r'wavelegnth $\rm \bf [\AA]$')
        plt.ylabel(r'spectrum $\rm \bf [L_\odot/Hz]$')
        
        ### NO DUST ###
        sp2 = fsps.StellarPopulation(sfh=3)
        sp2.set_tabular_sfh(age=t, sfr=sfr)
        wave, spec = sp2.get_spectrum(tage=max(t))
        b = (spec>-3)#1.e-7)
        plt.loglog(wave[b], spec[b], ls='--', label='no dust', c=co)

        if iM==0: plt.legend(frameon=False,loc='best')
        


plt.savefig('../../plots/fsps_sfh.pdf')
plt.show()



# old version
'''
lgM_list = [11.0]#, 12.0, 13.0, 14.0, 15.0]
for iM in [0]:#range(5):
    lgM = lgM_list[iM]
    co = color_list[iM]

    data = np.loadtxt('../../data/Behroozi_SFH/sfh/sfh_z0.1_corrected_%.1f.dat'%(lgM))
    #Scale factor, SFR, Err_Up, Err_Down (all linear units).
    a = data[:,0]
    sfr = data[:,1]
    er1 = data[:,2]
    er2 = data[:,3]
    z = 1./a-1
    t = cosmo.age(z).value # Gyr
    plt.subplot(121, yscale='log')
    # plt.loglog(t, sfr, label='Behroozi13')

    # plt.xlim(0.1,13)
    #plt.plot(z, sfr, label=str(lgM), c=co)
    plt.plot(t, sfr, label=str(lgM), c=co)
    print(t)
    print(sfr)
    plt.legend(frameon=False,loc='best')
    # plt.xlabel('time [Gyr]')
    #plt.xlim(0,10)
    plt.xlabel('z')
    plt.ylabel(r'SFR $\rm \bf [M_\odot/yr]$')
    #print('len(t)', len(t))
    #plt.show()

    
    #exit()
    # FSPS
    sp = fsps.StellarPopulation(sfh=3, dust_type=1, dust2=0.2)#sfh=3: tabulated sfh
    sp.set_tabular_sfh(age=t, sfr=sfr)
    wave, spec = sp.get_spectrum(tage=max(t))
    print('max(spec)', max(spec))

    
    plt.subplot(122)
    b = (spec>1.e-7)
    plt.loglog(wave[b], spec[b], label='dusttype=1, dust2=0.2', c=co)
    plt.xlabel(r'wavelegnth $\rm \bf [\AA]$')
    plt.ylabel(r'spectrum $\rm \bf [L_\odot/Hz]$')
    
    ### NO DUST ###
    sp2 = fsps.StellarPopulation(sfh=3)
    sp2.set_tabular_sfh(age=t, sfr=sfr)
    wave, spec = sp2.get_spectrum(tage=max(t))
    b = (spec>-3)#1.e-7)
    plt.loglog(wave[b], spec[b], ls='--', label='no dust', c=co)

    if iM==0: plt.legend(frameon=False,loc='best')
    
#plt.show()
#exit()
'''



'''
ntfull = np.shape(spec)[0]
for i in range(ntfull):
    b = (spec[i,:] > 1.e-10)
    plt.loglog(wave[b],spec[i,b])
plt.show()
'''
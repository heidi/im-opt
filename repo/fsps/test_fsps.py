#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import fsps
sp = fsps.StellarPopulation(zcontinuous=1, sfh=0, logzsol=0.0, dust_type=2, dust2=0.2)

#zcontinuous=1:  The SSPs are interpolated to the value of logzsol before the spectra and magnitudes are computed, and the value of zmet is ignored. [???]
#logzsol – (default: 0.0) Parameter describing the metallicity, given in units of log(Z/Z⊙)Only used if zcontinuous > 0.

#sfh=0 for SSP (single stellar population?) 

#dust_type=2: Calzetti et al. (2000) attenuation curve. Note that if this value is set then the dust attenuation is applied to all starlight equally (not split by age), and therefore the only relevant parameter is dust2, which sets the overall normalization (you must set dust1=0.0 for this to work correctly).

#print(sp.libraries)


#http://dan.iel.fm/python-fsps/current/installation/

#Let’s get the AB magnitudes in SDSS bands, for an SSP that is 13.7 Gyr old
# sdss_bands = fsps.find_filter('sdss')
# print(sdss_bands)
# print(sp.get_mags(tage=13.7, bands=sdss_bands))

# # lower the metallicity
# sp.params['logzsol'] = -1
# sp.get_mags(tage=13.7, bands=sdss_bands)

# We can also get the spectrum, here in units of L⊙/Hz as well as the total stellar mass formed by 13.7 Gyr and the surviving stellar mass at 13.7 Gyr (both in units of M⊙)

for tage in [1]:#[0.001, 0.01,0.1,1,10]:
    wave, spec = sp.get_spectrum(tage=tage)
    b = (spec>1.e-20)
    #np.savetxt('../../output/wavelength.dat', wave)
    plt.loglog(wave[b],spec[b], label=str(tage))
plt.legend(frameon=False,loc='best')
plt.show()
#print(sp.formed_mass)
#print(sp.stellar_mass)
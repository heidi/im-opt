#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import fsps

import matplotlib.cm
cmap = matplotlib.cm.get_cmap('afmhot')
nlines = 150
color_list = cmap(np.linspace(0,1,nlines))
#plt.figure(figsize=(14,7))
from astropy.cosmology import FlatLambdaCDM
cosmo = FlatLambdaCDM(H0=70, Om0=0.27) # what Behroozi used

data_loc = '../../data/Behroozi_SFH_v2/'
a_list = np.loadtxt(data_loc+'a_list.dat')[:,1]
m_list = np.loadtxt(data_loc+'m_list.dat')

# print('len(a_list)', len(a_list))
# print('len(m_list)', len(m_list))



for ia in [0]:#range(20,len(a_list)):
    a_present = a_list[ia]
    z_present = 1./a_present - 1.
    #outfile = open('../../output/sed_lib_%i.dat'%(ia),'w')
    for iM in range(0,len(m_list)):#[40]:#,20,40, 60, 80, 100, 120]:
        
        co = color_list[iM]
        m_present = m_list[iM]
        print('a=', a_present, 'm=', m_present)
        data = np.loadtxt(data_loc+'sfh_a%.6f.dat'%(a_present))
        b = (data[:,1] == m_present)
        z = -1 + data[b,0][::-1]
        sfr = 10**data[b,2][::-1]
        t = cosmo.age(z).value # Gyr # NOTE! t needs to be ascending!!

        # FSPS
        sp = fsps.StellarPopulation(sfh=3, dust_type=1, dust2=0.2, add_neb_emission=True)#sfh=3: tabulated sfh
        sp.set_tabular_sfh(age=t, sfr=sfr)
        wave, spec = sp.get_spectrum(tage=max(t))
        np.savetxt('../../output/wavelength.dat', wave)

        print('len(spec)', len(spec))
        b = (wave > 1.e+3)&(wave < 1.e+4)
        plt.plot(wave[b], spec[b])
        plt.show()
        # outfile.write('%g '%(m_present))
        # for s in spec: outfile.write('%g '%(s))
        # outfile.write('\n')

#!/usr/bin/env python
#from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
from astropy.coordinates import cartesian_to_spherical
import sys
sys.path.append('../')
sys.path.append('../pk_3d/')
import params as pa
'''
# using the *INCONVENIENT convention* between (ra,dec,z) and (px,py,pz)
# Mpc/h
'''

zmin = 5
zmax = 5.3

name = 'halo_ydir_z_%g_%g'%(zmin,zmax)
cat_name = pa.data_master_loc+'pk_lightcone_cat/%s.dat'%(name)
win_name = pa.data_master_loc+'pk_lightcone_cat/%s_win.dat'%(name)

chi_min = pa.cosmo.comoving_distance(zmin).value * pa.h  # Mpc/h
chi_max = pa.cosmo.comoving_distance(zmax).value * pa.h  # Mpc/h
ra_min = 0
ra_max = 1.4 * np.pi/180.
dec_min = 0
dec_max = 1.4 * np.pi/180.
py_min  = chi_min * np.cos(ra_max) * np.cos(dec_max)
py_max = chi_max
px_min = 0
px_max = chi_max * np.sin(dec_max)
pz_min = 0
pz_max = px_max
print('chi range', chi_min, chi_max)
#print('predicted py range', py_min, py_max)
#print('predicted px range', px_min, px_max)

Lbox = max(py_max-py_min, px_max-px_min)
print('Lbox=', Lbox)
ngrid = 128
l_cell = Lbox/ngrid

def write_cat():
    fname = pa.data_master_loc+'output_catalog/Ha_z_%i_%i.fits'%(int(np.floor(zmin)),int(np.floor(zmin)+1))
    from astropy.io import fits
    d = fits.getdata(fname)
    z = d['z']
    b = (z >= zmin)&(z < zmax)
    z = z[b]
    ra = d['ra'][b]
    dec = d['dec'][b]
    lgM = d['lgM'][b]
    lgLhalpha = d['lgLHa'][b]

    chi = pa.cosmo.comoving_distance(z).value * pa.h  # Mpc/h
    alpha = ra*np.pi/180.
    delta = dec*np.pi/180.
    px = chi*np.sin(delta)
    py = chi*np.cos(delta)*np.cos(alpha)
    pz = chi*np.cos(delta)*np.sin(alpha)

    outfile = open(cat_name,'w')
    for i in range(len(px)):
        outfile.write('%g %g %g %g\n'%(px[i]-px_min, py[i]-py_min, pz[i]-pz_min, 10**lgLhalpha[i]))
    outfile.close()


def write_window():
    n = ngrid
    window = np.zeros([n,n,n])
    for i in range(n):
        for j in range(n):
            for k in range(n):
                px_cell = px_min + (i+0.5)*l_cell
                py_cell = py_min + (j+0.5)*l_cell
                pz_cell = pz_min + (k+0.5)*l_cell
                chi = np.sqrt(px_cell**2 + py_cell**2 + pz_cell**2)
                ra = np.arctan2(px_cell,py_cell)
                dec = np.arcsin(pz_cell/chi)
                #print('myself', chi, ra*180/np.pi, dec*180/np.pi) # note! astropy gives 90-ra
                if ra_min < ra < ra_max and dec_min < dec < dec_max and chi_min < chi < chi_max: 
                    window[i,j,k] = 1
                    #print(i,j,k,'inside window')

    np.savetxt(win_name, window.flatten(), fmt='%i')



from pk_3d_mastercode import PowerSpectrum3D
def calc_pk():
    ifname = cat_name
    ofname = pa.data_master_loc+'pk_lightcone/%s'%(name)
    ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=False, save_grid=False, divide_by_mean=True, use_window=True, win_name=win_name)
    ps3.make_halo_density_grid()
    ps3.fourier_transform()

'''
# superceeded by halo_pk_3d_plot.py
def plot_pk():
    plt.subplot(111, xscale='log', yscale='log')
    data = np.loadtxt(pa.data_master_loc+'pk_lightcone/%s_pk.dat'%(name))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k, (k**3/(2*np.pi**2))*pk, label='from lightcone')#(k**3/(2*np.pi**2))*

    a = 0.16700
    fsample = 1
    data = np.loadtxt(pa.data_master_loc+'pk_3d/halfbox_a%.5f_pk.dat'%(a))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k, (k**3/(2*np.pi**2))*pk, label='half-box')


    data = np.loadtxt(pa.data_master_loc+'pk_3d/halo_pk_a%.5f_f%g_M0_100.dat'%(a,fsample))
    k = data[:,0]
    pk = data[:,1]
    b = (pk > 0)
    k = k[b]
    pk = pk[b]
    plt.plot(k, (k**3/(2*np.pi**2))*pk, label='original box')

    plt.title('halos, %g < z < %g'%(zmin,zmax))
    plt.xlabel(r"$\rm k [h/Mpc]$")
    plt.ylabel(r"$\rm k^3 P(k)/(2\pi^2)$")
    plt.legend()
    plt.savefig('../../plots/pk_3d_lightcone/halo_pk_lightcone.pdf')
    plt.show()
'''

if __name__ == "__main__":
    write_cat()
    write_window()
    calc_pk()
    #plot_pk()


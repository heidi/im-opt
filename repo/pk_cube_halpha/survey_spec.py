#!/usr/bin/env python
import numpy as np


l_survey = 1.4 # deg
n_ra = 812 #int(l_ra/l_ra)
l_ra = l_survey/n_ra #6.2 arcsecond, spherex
n_dec = n_ra
l_dec = l_ra

lam_rest = 0.6563 # halpha # micron



n_lam = 96
data = np.loadtxt('/home/hywu/work/IM_opt/repo/pk_cube_halpha/data/spherex_lambda_bins.dat')
lam_id = data[:,0]
lam_min_list = data[:,1]
lam_max_list = data[:,2]
lam_mid_list = data[:,3]
#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
import params as pa
'''
# from Phil Korngut
# 96 bands total.  24 bands in each.
# For each band make a Gaussian whose FWHM = 1.06*lambda/R

Band 1
R=41.4 
0.75um - 1.33um

Band 2
R=41.4
1.33um - 2.36um

Band 3
R= 41.4
2.36um - 4.18um

Band 4
R=135
4.18um - 5.0um
'''

lam_min_max_1 = [0.75, 1.33]
lam_min_max_2 = [1.33, 2.36]
lam_min_max_3 = [2.36, 4.18]
lam_min_max_4 = [4.18, 5.00]

def save_spherex_lambda_bins():
    outfile = open('data/spherex_lambda_bins.dat','w')
    outfile.write('#id, lam_min(micron), lam_max, lam_mid, delta_lam \n')
    ilam = 0
    for lam_range in [lam_min_max_1, lam_min_max_2, lam_min_max_3, lam_min_max_4]:
        lam_min = lam_range[0]
        lam_max = lam_range[1]
        nlam = 24
        lam_bins = 10**np.linspace(np.log10(lam_min), np.log10(lam_max), nlam+1)
        for i in range(nlam):
            outfile.write('%i \t %.5f \t %.5f \t %.5f \t %.5f\n'%(ilam, lam_bins[i], lam_bins[i+1],  (lam_bins[i]+lam_bins[i+1])/2, (lam_bins[i+1]-lam_bins[i])))
            ilam += 1
    outfile.close()


#plt.plot()



lam_rest = 0.6563

def save_spherex_halpha():
    outfile = open('data/spherex_halpha.dat','w')
    outfile.write('#id, lam_min(micron), lam_max, zmin(halpha), zmax, delta_chi[Mpc] \n')
    ilam = 0
    for lam_range in [lam_min_max_1, lam_min_max_2, lam_min_max_3, lam_min_max_4]:
        lam_min = lam_range[0]
        lam_max = lam_range[1]
        nlam = 24
        lam_bins = 10**np.linspace(np.log10(lam_min), np.log10(lam_max), nlam+1)

        for i in range(nlam):
            lam_min = lam_bins[i]
            lam_max = lam_bins[i+1]
            zmin = lam_bins[i]/lam_rest-1
            zmax = lam_bins[i+1]/lam_rest-1
            delta_chi = pa.cosmo.comoving_distance(zmax).value - pa.cosmo.comoving_distance(zmin).value
            print(delta_chi)

            outfile.write('%i \t %.5f \t %.5f \t %.5f \t %.5f \t %.5f \n'%(ilam, lam_min, lam_max, zmin, zmax, delta_chi))
            ilam += 1
    outfile.close()

def plot_spherex_halpha():
    ids, lam_min, lam_max, zmin, zmax, delta_chi = np.loadtxt('data/spherex_halpha.dat', unpack=True)
    lam = 0.5*(lam_min + lam_max)
    z = 0.5*(zmin + zmax)
    #plt.figure(figsize=(14,14))
    ax1 = plt.subplot(111)
    plt.plot(lam, delta_chi)
    plt.axvline(lam_min_max_1[0], ls='--', c='m')
    plt.axvline(lam_min_max_2[0], ls='--', c='m')
    plt.axvline(lam_min_max_3[0], ls='--', c='m')
    plt.axvline(lam_min_max_4[0], ls='--', c='m')
    plt.axvline(lam_min_max_4[1], ls='--', c='m')
    plt.ylabel(r'$\rm \Delta \chi $ in z-direction [comoving Mpc]')

    ax1.set_xlabel(r"$\lambda \ [\mu m]$")
    ax3 = ax1.twiny() # draw top axis
    x1, x2 = ax1.get_xlim()
    ax3.set_xlim(x1, x2)#note! it won't work without this line!
    tick_lbls = np.arange(8) # z
    tick_locs = (1+tick_lbls)*lam_rest
    plt.xticks(tick_locs, tick_lbls)
    ax3.set_xlabel(r'z for $\rm H_\alpha$ sources')
    plt.savefig('../../plots/pk_cube/delta_chi_halpha.pdf')
    plt.show()


if __name__ == "__main__":
    plot_spherex_halpha()
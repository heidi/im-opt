#!/usr/bin/env python
#from numpy import fft
import numpy as np
import matplotlib.pyplot as plt
from astropy.coordinates import cartesian_to_spherical
import sys
sys.path.append('../')
sys.path.append('../pk_tools/')
import params as pa
from pk_non_cubic_mastercode import PowerSpectrumNonCubic
import survey_spec as ss

# pz along line of sight
# no h !


zmin = 5.36904 # band 72
zmax = 6.61847 # band 95


name = 'halpha_non_cubic_z_%1.1f_%1.1f'%(zmin,zmax)
cat_name = pa.data_master_loc+'pk_cube/%s.dat'%(name)
win_name = pa.data_master_loc+'pk_cube/%s_win.dat'%(name)

chi_min = pa.cosmo.comoving_distance(zmin).value # no h 
chi_max = pa.cosmo.comoving_distance(zmax).value # no h
ra_min = 0
ra_max = 1.4 * np.pi/180.
dec_min = 0
dec_max = 1.4 * np.pi/180.

pz_min = chi_min * np.cos(ra_max) * np.cos(dec_max)
pz_max = chi_max
px_min = 0
px_max = chi_min * np.sin(dec_max) # note! chi_min here.
py_min = 0
py_max = px_max
print('tansverse range [Mpc] = ', px_max-px_min)
print('chi range', chi_min, chi_max)
#print('predicted py range', py_min, py_max)
#print('predicted px range', px_min, px_max)
# Lbox = max(pz_max-pz_min, px_max-px_min)
# print('L=', Lbox)
# ngrid = 128
# l_cell = Lbox/ngrid
# cell_vol = l_cell**3
# print('l_cell ', l_cell)
Lx = px_max-px_min
Ly = py_max-py_min
Lz = pz_max-pz_min
nx = 128
ny = 128
nz = 24
lx = Lx/nx
ly = Ly/ny
lz = Lz/nz
cell_vol = lx*ly*lz

#print('lz=',lz)
#exit()


def write_xyz_from_cube():
    outfile = open(cat_name,'w')
    for ilam in range(72,96):#range(n_lam): # TODO! determine automatically
        lam_mid = ss.lam_mid_list[ilam]
        z = lam_mid/ss.lam_rest - 1
        if z > zmin and z < zmax:
            print('ilam', ilam, 'zmid', z)
            chi = pa.cosmo.comoving_distance(z).value 
            data = np.loadtxt(pa.data_master_loc+'pk_cube/halpha_lam_%i.dat'%(ilam))
            for i in range(ss.n_ra):
                for j in range(ss.n_dec):
                    ra_mid = (i+0.5)*ss.l_ra
                    dec_mid = (j+0.5)*ss.l_dec
                    S = data[i,j] / cell_vol  #Note!
                    alpha = ra_mid * np.pi/180.
                    delta = dec_mid * np.pi/180.
                    px = chi * alpha
                    py = chi * delta
                    pz = chi
                    #for i in range(len(px)):
                    outfile.write('%g %g %g %g\n'%(px-px_min, py-py_min, pz-pz_min, S))
    outfile.close()

'''
def plot_xyz():
    px, py, pz, s = np.loadtxt(cat_name, unpack=True)
        plt.scatter(px,pz,s=1)
    plt.show()


def write_window(): # the same as pk_lightcone
    n = ngrid
    window = np.zeros([n,n,n])
    for i in range(n):
        for j in range(n):
            for k in range(n):
                px_cell = px_min + (i+0.5)*l_cell
                py_cell = py_min + (j+0.5)*l_cell
                pz_cell = pz_min + (k+0.5)*l_cell
                chi = np.sqrt(px_cell**2 + py_cell**2 + pz_cell**2)
                ra = px_cell / chi
                dec = py_cell / chi
                if ra_min < ra < ra_max and dec_min < dec < dec_max and chi_min < chi < chi_max: 
                    window[i,j,k] = 1
    np.savetxt(win_name, window.flatten(), fmt='%i')

def plot_window():
    for j in [0]:
        plt.figure(j)
        win = np.loadtxt(win_name, unpack=True).reshape([ngrid, ngrid, ngrid])
        plt.imshow(win[:,j,:].transpose(), origin='lower')
    plt.xlabel('x')
    plt.ylabel('z')
    plt.show()
'''
def calc_halpha_pk():
    ifname = cat_name
    ofname = pa.data_master_loc+'pk_cube/%s'%(name)
    # ps3 = PowerSpectrum3D(ifname=ifname, ofname=ofname, ngrid=ngrid, Lbox=Lbox, use_weight=True, save_grid=False, divide_by_mean=False, use_window=True, win_name=win_name)
    # ps3.make_halo_density_grid()
    # ps3.fourier_transform(anisotropic=True)
    # ps3.fourier_transform(anisotropic=False)
    psnc = PowerSpectrumNonCubic(ifname=ifname, ofname=ofname, nx=nx, ny=ny, nz=nz, Lx=Lx, Ly=Ly, Lz=Lz, use_weight=True, save_grid=True, divide_by_mean=False, use_window=False)
    psnc.make_halo_density_grid()
    psnc.fourier_transform(anisotropic=True, dlgk_perp=0.2, dlgk_para=0.2)



def plot_halpha_pk():

    plt.figure(figsize=(14,14))
    plt.subplot(221)
    data = np.loadtxt(pa.data_master_loc+'pk_cube/%s_grid.dat'%(name))
    data = data.reshape(nx,ny,nz)
    plt.imshow(data[:,0,:].transpose(), origin='lower', aspect='equal', extent=(0,1,0,1), interpolation='nearest')
    plt.colorbar()
    plt.xlabel('x: %g Mpc'%(Lx))
    plt.ylabel('z: %g Mpc'%(Lz))
    plt.title('FFT grid')
    '''
    # P(k)
    plt.subplot(221)
    fname = pa.data_master_loc+'pk_cube/%s_pk_non_cubic.dat'%(name)
    k, pk = np.loadtxt(fname, unpack=True)
    plt.plot(k, pk)

    fname = pa.data_master_loc+'pk_cube/%s_pk_non_cubic.dat'%(name)
    k, pk = np.loadtxt(fname, unpack=True)
    plt.plot(k, pk)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k \ [1/Mpc]$')
    plt.ylabel(r'$\rm P(k)$')  
    '''

    plt.subplot(222)
    # P(k_perp, k_para)
    fname = pa.data_master_loc+'pk_cube/%s_pk_perp_para_non_cubic.dat'%(name)
    k_perp, k_para, pk = np.loadtxt(fname, unpack=True)
    k_cubed = np.sqrt(k_perp**2 + k_para**2)**3
    DeltaSqr = pk * k_cubed/(2*np.pi**2)

    k_perp = np.unique(k_perp)
    k_para = np.unique(k_para)
    nk_perp = len(k_perp)
    nk_para = len(k_para)
    pk = pk.reshape([nk_perp, nk_para])
    DeltaSqr = DeltaSqr.reshape([nk_perp, nk_para])
    
    plt.imshow(np.log10(DeltaSqr.transpose()), origin='lower', aspect='equal', extent=(0,1,0,1), interpolation='nearest') 
    plt.colorbar()
    plt.xlabel(r'$\rm k_\perp \ [1/Mpc]$')
    plt.ylabel(r'$\rm k_\parallel \ [1/Mpc]$')
    plt.title(r'$\rm log_{10} k^3 P(k_\perp, k_\parallel)/(2\pi^2)$')

    
    xtick_lbls = ['{:.2f}'.format(i) for i in k_perp]
    xtick_locs = (0.5+np.arange(nk_perp))/nk_perp
    plt.yticks(xtick_locs[0:-1:3], xtick_lbls[0:-1:3])

    ytick_lbls = ['{:.2f}'.format(i) for i in k_para]
    ytick_locs = (0.5+np.arange(nk_para))/nk_para
    plt.xticks(ytick_locs[0:-1:3], ytick_lbls[0:-1:3])

    
    from matplotlib.cm import get_cmap
    cmap = get_cmap('Blues_r') # reverse of 'Blues'

    plt.subplot(223)
    c_list = cmap(np.linspace(0,1,nk_para+1))
    for ik_para in range(nk_para):
        plt.plot(np.unique(k_perp), DeltaSqr[:,ik_para], c=c_list[ik_para], label=r'$\rm k_\parallel$=%.2g'%k_para[ik_para])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k_\perp\ [1/Mpc]$')
    plt.ylabel(r'$\rm k^3P(k_\perp)/(2\pi^2)$')
    plt.legend()

    plt.subplot(224)
    c_list = cmap(np.linspace(0,1,nk_perp+1))
    for ik_perp in range(0,nk_perp):
        plt.plot(np.unique(k_para), DeltaSqr[ik_perp,:], c=c_list[ik_perp], label=r'$\rm k_\perp$=%.2g'%k_perp[ik_perp])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$\rm k_\parallel\ [1/Mpc]$')
    plt.ylabel(r'$\rm k^3P(k_\parallel)/(2\pi^2)$')
    plt.legend()

    plt.savefig('../../plots/pk_cube/%s_pk_perp_para.pdf'%(name))
    
    #plt.show()

if __name__ == "__main__":
    #write_xyz_from_cube()
    calc_halpha_pk()
    plot_halpha_pk()
#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

import IM_opt.repo.params as pa 
import lightcone.repo.coordinate.params as pa2
params = pa2.Params(theta_deg = 1.4)
from CIB_opt.repo.model.sim import Sim
sim = Sim(name='BolshoiP')

import survey_spec as ss
lam_rest = ss.lam_rest
l_survey = ss.l_survey
n_ra = ss.n_ra
l_ra = ss.l_ra
n_dec = ss.n_dec
l_dec = ss.l_dec
n_lam = ss.n_lam
lam_id = ss.lam_id
lam_min_list = ss.lam_min_list
lam_max_list = ss.lam_max_list
lam_mid_list = ss.lam_mid_list
'''
# drawing the grid #
for i in range(n_ra):
    for j in range(n_dec):
        for k in range(n_lam):
            ra_min = i*l_ra
            ra_max = (i+1)*l_ra
            dec_min = j*l_dec
            dec_max = (j+1)*l_dec
            lam_min = lam_min_list[k]
            lam_max = lam_max_list[k+1]
'''




def write_cube():
    data_cube = np.zeros((n_ra, n_dec, n_lam))

    for zmin in [5,6]:#range(7):# for group 4
        dz = 1    
        zmax = zmin + dz
        fname = pa.data_master_loc+'output_catalog/Ha_z_%i_%i.fits'%(zmin,zmax)
        from astropy.io import fits
        d = fits.getdata(fname)
        z = d['z']
        #b = (z >= zmin)&(z < zmax)
        #z = z[b]
        ra = d['ra']#[b]
        dec = d['dec']#[b]
        lgM = d['lgM']#[b]
        lgLhalpha = d['lgLHa']#[b]

        chi = pa.cosmo.comoving_distance(z).value
        L_halpha = 10**lgLhalpha
        DH = 3000. / pa.h # no h for S_halpha
        DA = chi/(1+z)
        DL = chi*(1+z)
        Ez = np.sqrt(pa.OmegaM*(1+z)**3+pa.OmegaDE)
        nu = (3.e+8)/(lam_rest*1.e-6)
        dchi_dnu = DH * (1+z)**2 / nu / Ez # Mpc/Hz
        S_halpha = L_halpha / (4*np.pi*DL**2) * (DA**2) * dchi_dnu
        # erg/s Mpc/Hz 1/sr
        # note! divide by cell vol later
        
        for igal in range(len(z)):
            lam_obs = lam_rest * (1+z[igal])
            b = (lam_obs > lam_min_list)&(lam_obs < lam_max_list)
            if len(lam_id[b]) > 0:
                k = int(lam_id[b][0]+1.e-3)
                i = int(ra[igal]/l_ra)
                j = int(dec[igal]/l_dec)
                data_cube[i,j,k] += S_halpha[igal]
        
    data = np.loadtxt('data/spherex_halpha.dat')
    #id, lam_min(micron), lam_max, zmin(halpha), zmax, delta_chi[Mpc] 
    zmin_halpha_list = data[:,3]
    zmax_halpha_list = data[:,4]
    for ilam in range(72,96):#Note! 4th group
        print('writing ilam %i'%(ilam))
        zmin_halpha = zmin_halpha_list[ilam]
        zmax_halpha = zmax_halpha_list[ilam]
        voxel_vol = (sim.VC_interp(zmax_halpha)-sim.VC_interp(zmin_halpha))*params.fsky/(n_ra**2)
        '''
        print(voxel_vol)

        data = np.loadtxt(pa.bolshoip_loc+'cosmo_volume_time.dat')
        z = data[:,1]
        dVdOmegadz = data[:,5]
        dVdOmega = data[:,6]

        from scipy.interpolate import interp1d
        dVdOmegadz_interp = interp1d(z, dVdOmegadz)
        dVdOmega_interp = interp1d(z, dVdOmega)
        zmid = 0.5*(zmin+zmax)
        vol = dVdOmegadz_interp(zmid)
        area = (1.4*np.pi/180.)**2
        dz = zmax - zmin
        zslice_vol = vol * area * dz
        print(zslice_vol)
        '''
        
        np.savetxt(pa.data_master_loc+'pk_cube/halpha_lam_%i.dat'%(ilam), data_cube[:,:,ilam] / voxel_vol * pa.Mpc2cm**-2 * pa.cgs2Jy, fmt ='%g')
        # erg s-1 Mpc-2 Hz-1 to Jy
        

def plot_slices():
    for ilam in [70]:#range(70, 85):
        plt.figure()
        data = np.loadtxt(pa.data_master_loc+'pk_cube/halpha_lam_%i.dat'%(ilam))
        p1 = plt.imshow(np.arctan(data))
        print('mean', np.mean(data))
        #plt.colorbar()
        p1.axes.set_xticklabels([])
        p1.axes.set_yticklabels([])
        plt.title(r"$\rm H_\alpha,\ band\ %i,\ %.2f - %.2f \mu m, mean=%g Jy/sr$"%(ilam, lam_min_list[ilam], lam_max_list[ilam],np.mean(data)))
        plt.savefig(pa.data_master_loc+'pk_cube_images/halpha_lam_%i.png'%(ilam))
    plt.show()


if __name__ == "__main__":

    write_cube()
    #plot_slices()
